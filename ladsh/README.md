﻿# Learning Analytics Dashboard

## Attribution

This dashboard was implemented using the [xAPI-Dashboard](https://github.com/adlnet/xAPI-Dashboard) and [NVD3.js](https://nvd3.org/index.html) library.

### Getting Started

To view the dashboard, simply open `index.html` in your web browser. For a more robust setup, you can deploy the dashboard using Nginx and Docker.

```
cd ..
docker-compose -f docker-compose-ladsh.yml up -d
```
