[<img src="https://git.rwth-aachen.de/medialab/19squared/-/raw/2.0.9/src/app/static/img/brand/banner3.png" alt="19squared" width="700"/>](https://19squared.de)

[![main](https://img.shields.io/website?url=http%3A%2F%2F19squared.de%2Fhome%2F&up_message=online&down_message=offline&label=19squared.de)](https://19squared.de)
[![Tag](https://img.shields.io/gitlab/v/tag/80064?gitlab_url=https%3A%2F%2Fgit.rwth-aachen.de&label=version)](https://git.rwth-aachen.de/medialab/19squared/-/tags)
[![Issues](https://img.shields.io/gitlab/issues/open-raw/80064?gitlab_url=https%3A%2F%2Fgit.rwth-aachen.de)](https://git.rwth-aachen.de/medialab/19squared/-/issues)
[![Merge Requests](https://img.shields.io/gitlab/merge-requests/open-raw/80064?gitlab_url=https%3A%2F%2Fgit.rwth-aachen.de)](https://git.rwth-aachen.de/medialab/19squared/-/merge_requests)
[![Contributors](https://img.shields.io/gitlab/contributors/80064?gitlab_url=https%3A%2F%2Fgit.rwth-aachen.de)](https://git.rwth-aachen.de/medialab/19squared/-/project_members)
[![License](https://img.shields.io/gitlab/license/80064?gitlab_url=https%3A%2F%2Fgit.rwth-aachen.de)](https://git.rwth-aachen.de/medialab/19squared/-/blob/main/LICENSE)

[![Python](https://img.shields.io/badge/Python-FFD43B?style=for-the-badge&logo=python)](https://www.python.org/)
[![Flask](https://img.shields.io/badge/Flask-000000?style=for-the-badge&logo=flask)](https://flask.palletsprojects.com/en/3.0.x/)
[![MariaDB](https://img.shields.io/badge/MariaDB-003545?style=for-the-badge&logo=mariadb)](https://mariadb.org/)
[![JavaScript](https://img.shields.io/badge/JavaScript-323330?style=for-the-badge&logo=javascript)](https://wiki.selfhtml.org/wiki/JavaScript)
[![HTML5](https://img.shields.io/badge/HTML5-E34F26?style=for-the-badge&logo=html5&logoColor=white)](https://wiki.selfhtml.org/wiki/HTML/Tutorials/Entstehung_und_Entwicklung#HTML5)
[![Docker](https://img.shields.io/badge/Docker-2CA5E0?style=for-the-badge&logo=docker&logoColor=white)](https://www.docker.com/)
[![nginx](https://img.shields.io/badge/Nginx-009639?style=for-the-badge&logo=nginx)](https://nginx.org/en/)

# [`19squared`](https://19squared.de) - Just more than 360

19squared is a web application for creating and experiencing interactive 360° tours in extended reality environments.

# Deploy on Linux

The project can be deployed using the following chained command.

```
git clone https://git.rwth-aachen.de/medialab/19squared.git && cd ./19squared && cp .env.template .env && docker compose up -d
```

# Environment Variables

To get started, configure the application using the environment variables.

```
cp .env.template .env
```

## Development Server (for local development)

### Prerequisites

- [`Docker 🐋 >= 20.10`](https://www.docker.com/)

### Run

After configuring the environment variables, open the project folder in VSC and the task will launch automatically. You can also run the following code.

```
(cp .env.template .env)
docker compose -f docker-compose-dev.yml up -d --build
```

## Production Server with Docker and nginx

### Prerequisites

- [`Docker 🐋 ~= 20.10`](https://www.docker.com/)

### Run

Hot-reloading is not supported in this configuration.

```
(cp .env.template .env)
docker compose up -d
```

## Learning Analytics Dashboard [`(ladsh)`](https://ladsh.19squared.de)

View the dashboard locally by opening `ladsh/index.html` or by deploying the configured nginx server.

```
docker compose -f docker-compose-ladsh.yml up -d
```

# Contributors

## Code Owners

<table>
	<tr>
		<td align="center">
			<a href="https://www.lbz.rwth-aachen.de/go/id/mhzn/contact/aaaaaaaaaavchth/gguid/PER-F4SYRNW/ikz/1210">
				<img src="https://www.lbz.rwth-aachen.de/global/show_picture.asp?id=aaaaaaaabhwlwwz&h=230&q=94" width="100px" />
				<br>
				<sub>
					<b>Dr. Matthias Ehlenz</b>
				</sub>
			</a>
		</td>
        <td align="center">
			<a href="https://www.lbz.rwth-aachen.de/go/id/mhzn/contact/aaaaaaaaaavchpe/gguid/PER-U8RXXFL/ikz/1210">
				<img src="https://www.lbz.rwth-aachen.de/global/show_picture.asp?id=aaaaaaaabhrxkfi&h=230&q=94" width="100px" />
				<br>
				<sub>
					<b>Frederic Maquet</b>
				</sub>
			</a>
		</td>
	</tr>
</table>

## Developers

<table>
	<tr>
		<td align="center">
			<a href="https://git.rwth-aachen.de/jonas.rothhardt">
				<img src="https://git.rwth-aachen.de/uploads/-/system/user/avatar/15571/avatar.png?width=400" width="100px" />
				<br>
				<sub>
					<b>Jonas Rothhardt</b>
				</sub>
			</a>
            <br>
            <sub>
                <b>Bachelor (2023)</b>
				<br>
				<br>
				![GitLab Last Commit](https://img.shields.io/gitlab/last-commit/80064?gitlab_url=https%3A%2F%2Fgit.rwth-aachen.de&ref=1.0.0-ba-jonas-rothhardt)
			</sub>
		</td>
        <td align="center">
			<a href="https://git.rwth-aachen.de/OnlyTimm1">
				<img src="https://git.rwth-aachen.de/uploads/-/system/user/avatar/35969/avatar.png?width=400" width="100px" />
				<br>
				<sub>
					<b>Timm Köster</b>
				</sub>
			</a>
            <br>
            <sub>
                <b>Bachelor (2024)</b>
				<br>
				<br>
				![GitLab Last Commit](https://img.shields.io/gitlab/last-commit/80064?gitlab_url=https%3A%2F%2Fgit.rwth-aachen.de&ref=timm-dev)
			</sub>
		</td>
        <td align="center">
			<a href="https://git.rwth-aachen.de/paul.rennecke">
				<img src="https://git.rwth-aachen.de/uploads/-/system/user/avatar/4565/avatar.png?width=400" width="100px" />
				<br>
				<sub>
					<b>Paul Rennecke</b>
				</sub>
			</a>
            <br>
            <sub>
                <b>Master (2024)</b>
				<br>
				<br>
				![GitLab Last Commit](https://img.shields.io/gitlab/last-commit/80064?gitlab_url=https%3A%2F%2Fgit.rwth-aachen.de&ref=paul-dev)
			</sub>
		</td>
        <td align="center">
			<a href="https://git.rwth-aachen.de/larsmeiendresch">
				<img src="https://git.rwth-aachen.de/uploads/-/system/user/avatar/15196/avatar.png?width=400" width="100px" />
				<br>
				<sub>
					<b>Lars Meiendresch</b>
				</sub>
			</a>
            <br>
            <sub>
                <b>Master (2024)</b>
				<br>
				<br>
				![GitLab Last Commit](https://img.shields.io/gitlab/last-commit/80064?gitlab_url=https%3A%2F%2Fgit.rwth-aachen.de&ref=lars-dev)
			</sub>
		</td>
	</tr>
</table>

Brand Design by Alexander Prizkau, Hotspot Icons by Jasmin Hartanto

# Wiki

For in-depth project information, please consult the [📚 Git Wiki](https://git.rwth-aachen.de/medialab/19squared/-/wikis/).
