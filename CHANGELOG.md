# SemVer Changelog

## 2.7.7

- quickfix AnonymousUser' object has no attribute 'id'

## 2.7.4

- add last login to user-managemnet
- bugfixes

## 2.7.3

- Searchlist
- Hotspot Locked
- User progress
- Deadlock Detection

## 2.7.2

- add access to tours of all users in user_management
- bugfixes

## 2.7.1

- optimize share options
- added option to share tour directly in tour
- added currently only users of a tour in lib and tour options

## 2.7.0

- updated group management
  - invite via username, email or token
  - generate new users
  - export group list (with QR-Codes)
- group invites and list on home
- leave group via profil
- add german translation
- reverse mouse rotation vertical rotation on touch screens and toggle for gryo-sensor on view
- optimize design
- bugfixes

## 2.6.18

-optimize responsive design

## 2.6.17

- enable vertical rotation on touch screens and toggle for gryo-sensor on create
- optimize create on small devices

## 2.6.16

- bugfixes

## 2.6.15

- bugfixes

## 2.6.14

- bugfixes

## 2.6.13

- bugfixes

## 2.6.12

- bugfixes
- new messages bubble

## 2.6.11

- add currently online users of a tour
- refactor hotspotlist in mediapage, add icons and warnings

## 2.6.10

- add user-management export and table options like pagination and search
- optimize lib view
- add permission descriptions
- multiple bugfixes

## 2.6.8

- multiple bugfixes

## 2.6.7

- multiple fixes and improvements in CreateTour

## 2.6.6

- add importing assets if they are already on the server (also to the demo tour)
- replace confirms with modals (and confirm with enter)
- short explanations on new empty tour
- add group creator to group
- multiple minor bugfixes

## 2.6.4

- multiple bugfixes

## 2.6.3

- fix scrollbar bug on Create View

## 2.6.2

- drop files direct into scene/mediapages
- add Role-Management settings "Allow to add role to a group" and "Assign to new user"
- fix umlauts in demotour

## 2.6.1

- fix visibility of new hotspot icons
- fix clickability of hotspots in creation page
- opacity of visited hotspots added
- improved usability in creation page when hotspot is clicked

## 2.6.0

- add Quiz Hotspot
- add Combination Hotspot

## 2.5.0

- user quotas
- fix many minor issues
- nginx proxy manager as seperate docker compose
- download tours in full and view them locally
- shorten string id
- allow same tour name multiple times
- new learning analytics dashboards

## 2.4.2

- add asset, mediapage and tour annotations
- add tour chat
- revision asset/mediapage icons, responsive behavior and their actions e.g. renaming in an modal
- rename code to token
- add progressive web app
- improve iOS popups

## 2.4.1

- fix some user/role issue and add explanations
- add account deletion

## 2.4.0

- add hotspot dependencies

## 2.3.2

- fixed a pipeline error

## 2.3.1

- fix of issue where valid email adresses were not recongnised

## 2.3.0

- add user-roles with permissions
- add groups with invite code system
- add profil view, user-management, role-management and group-management
- fix footer and some other styling issue

## 2.2.1

- alembic database migration system
- first alembic migration

## 2.2.0

- learning analytics dashboards
- mail events for registering, sharing tours
- possible to reset password
- style and colour changes
- preloading of assets
- more route tests
- advances authentication methods (Google, GitLab, GitHub)

## 2.1.0

- sync "everything" over all open instances via SocketIO
- add demotour to startpage
- add initial admin-user and demotour, admin has permission to change or edit demotour
- selecting hotspots via click on scene

## 2.0.13

- first learning analytics dashboard
- changes in VSC tasks
- further documentation
- further tests

## 2.0.12

- finish MediaLab Lehramt tour as demo

## 2.0.11

- fix Docker/npm issues
- fix asset export
- fix deleting tours
- remove unwanted headers
- fix CI/CD git cleaning strategy

## 2.0.10

- npm install
- fix vr video controls
- fix footer

## 2.0.10

- MediaLab Lehramt demo tour

## 2.0.9

- fix login/register error
- potential fix 502 Bad Request error
- re-branding with new logos and banners
- minor improvements in readability

## 2.0.8

- tests in docker environment
- fix error handling for login and register
- add mail sending capability

## 2.0.7

- add sharedTours
- redesign project overview

## 2.0.6

- add CI/CD for automatic testing and MLL deployment
- add GitLab runners for CI/CD
- add further unit tests
- rename sample to demo tour
- add error handling messages to HTTP errors
- fix import tour names

## 2.0.5

- skip 10sec controls

## 2.0.4

- add testing
- port changes to allow reverse proxies

## 2.0.3

- js static library fix

## 2.0.2

- nginx fixes

## 2.0.1

- HTML footer fix

## 2.0.0

- BREAKING: switch from SQLite to mariadb
- branding change: Interactive360XR --> 19squared
- fix softlock for initial/home mediapage
- implement xAPI tracking for tour creation

## 1.1.3

- download assets on tour export
- allow download of assets
- allow change of initial/home mediapage

## 1.1.2

- add assets to export
- fix migration of assets to imported tours

## 1.1.1

- fix bug of footer hiding control elements
- fix several minor bugs
- add icon to header
- design of font and colours

## 1.1.0

- many fixes and minor additions
- repository re-organization of complete project
- tour operations
  - import tour
  - export tour
  - create sample tour
  - duplicate tour
  - rename tour
- drag&drop editor
- video controls
- record audio
- zooming
  3-

## 1.0.0

First finished version.
