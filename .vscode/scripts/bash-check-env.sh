# Check if the .env file is available or copy it if not.

# it might be possible, that sometimes this file has not the right permission to be executed by a vsc-task. To resolve this, please run: "chmod +x bash-stop-containers.sh" on this file.

# Check if .env exists, if not, copy it from .env.template
if [ ! -f .env ]; then
  echo -e "\033[31m.env not found! Copying from .env.template...\033[0m"
  cp .env.template .env
fi