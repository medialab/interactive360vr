# Stop any existing containers, working with bash
# reuse me whenever you want to stop all possible running containers (while you using bash)

# it might be possible, that sometimes this file has not the right permission to be executed by a vsc-task. To resolve this, please run: "chmod +x bash-stop-containers.sh" on this file.

docker ps -q --filter 'name=test-19sq-mariadb' | grep -q . && docker stop test-19sq-mariadb || true;
docker ps -q --filter 'name=19sq-assets' | grep -q . && docker stop 19sq-assets || true;
docker ps -q --filter 'name=test-19sq-flask' | grep -q . && docker stop test-19sq-flask || true;
docker ps -q --filter 'name=19sq-mariadb' | grep -q . && docker stop 19sq-mariadb || true;
docker ps -q --filter 'name=19sq-flask' | grep -q . && docker stop 19sq-flask || true;
docker ps -q --filter 'name=19sq-nginx' | grep -q . && docker stop 19sq-nginx || true;