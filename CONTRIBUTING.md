# Contribution Guidelines

Thank you for your interest in contributing to this project. We value your time and effort. Please follow these guidelines to ensure a smooth and efficient contribution process.

## Getting Started

1.  **Request Access:** Before you begin, please request access to this repository from the [RWTH MediaLab](https://www.lbz.rwth-aachen.de/cms/LBZ/Das-Lehrerbildungszentrum/Geschaeftsstelle/~vcacl/MediaLab/).

2.  **Branching:** Once access is granted, create a new branch for your work. Follow this naming convention:
    - `dev-<your_name>` (for general development tasks)
    - `<my_feature_branch>` (for specific features)

Base your branch off the current `stage` branch.

## Issue Management

- **Create an Issue:** Before starting any development, please create an issue in the issue tracker. This helps us track progress and prioritize tasks.
- **Self-Assign and Label:** Assign the issue to yourself and apply appropriate labels.
- **Existing Issues:** If an issue already exists for your proposed change, simply assign yourself to it.

## Code Style and Formatting

We maintain consistent code style using:

- **Prettier:** [https://prettier.io/](https://prettier.io/)
- **autopep8:** [https://pypi.org/project/autopep8/](https://pypi.org/project/autopep8/)

Please format your code before committing any substantial changes using the predefined VSC task.

## Merging Your Changes

To merge your changes, follow these steps:

1.  **Semantic Versioning:** Ensure you've updated the `src/VERSION.py` file according to **[Semantic Versioning 2.0.0](https://semver.org/spec/v2.0.0.html)**.
2.  **Changelog:** Document your changes in the `CHANGELOG.md` file.
3.  **Merge Request:** Create a merge request from your development/feature branch to the `stage` branch.
4.  **Assignee:** Assign your merge request to a [project maintainer](https://git.rwth-aachen.de/medialab/19squared/-/project_members?sort=access_level_desc) or request review if necessary.

_Thank you for helping to make 19squared even better!_
