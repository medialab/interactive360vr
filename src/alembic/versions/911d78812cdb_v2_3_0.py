"""v2.3.0

Revision ID: 911d78812cdb
Revises: 8fb7b71cc6f5
Create Date: 2024-10-16 11:13:16.603416

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision: str = '911d78812cdb'
down_revision: Union[str, None] = '8fb7b71cc6f5'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.alter_column('users', 'is_active',
                    existing_type=mysql.TINYINT, new_column_name='active')


def downgrade() -> None:
    pass
