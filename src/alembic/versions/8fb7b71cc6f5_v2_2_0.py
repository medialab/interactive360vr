"""v2.2.0

Revision ID: 8fb7b71cc6f5
Revises: 
Create Date: 2024-08-29 23:16:23.459829

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision: str = '8fb7b71cc6f5'
down_revision: Union[str, None] = None
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.add_column('users', sa.Column('reset_secret',
                  sa.String(length=64), nullable=True))
    op.add_column('users', sa.Column(
        'la_opt_out', sa.Boolean(), nullable=True))
    op.alter_column('users', 'password_hash',
                    existing_type=mysql.VARCHAR(length=256),
                    nullable=True)


def downgrade() -> None:
    pass
