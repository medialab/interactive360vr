import os
from app import app
cwd = os.getcwd()
print(cwd)

ssl_context = "adhoc" if app.config["SSL"] else None
extra_files = [
    "app/translations/de/LC_MESSAGES/messages.mo"] if app.config["DEBUG"] else None
if (app.config["ENVIRONMENT"]):
    app.run(host="0.0.0.0", port=app.config["PORT"], debug=app.config["DEBUG"],
            ssl_context=ssl_context, extra_files=extra_files)
else:
    app.run(port=app.config["PORT"], debug=app.config["DEBUG"],
            ssl_context=ssl_context, extra_files=extra_files)
