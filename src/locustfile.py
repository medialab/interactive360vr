from locust import HttpUser, task, between
import random
import string
import time


class TestTasks(HttpUser):
    wait_time = between(1, 20)

    @task
    def demo_tour(self):
        self.client.get("/view/demo")

    @task
    def static_pages(self):
        urls = ["/", "/login", "/register", "/impressum", "/datenschutz"]
        for url in urls:
            self.client.get(url)
            time.sleep(random.randint(1, 3))

    @task
    def register_login_create_view(self):
        randomString = ''.join(random.choice(string.ascii_letters)
                               for _ in range(20))

        self.client.get("/register")
        time.sleep(random.randint(1, 3))
        self.client.post("/register", data={"username": randomString,
                         "email": randomString + "@locusttest.de", "password": "leifieee"})
        time.sleep(random.randint(1, 3))
        self.client.get("/login")
        time.sleep(random.randint(1, 3))
        self.client.post(
            "/login", data={"username_or_email": randomString, "password": "leifieee"})
        time.sleep(random.randint(1, 3))
        self.client.get("/home")
        time.sleep(random.randint(1, 3))
        id = self.client.get("/home/new_tour?project_name=" + randomString,
                             name="/home/new_tour").request.path_url.split('=')[-1]
        time.sleep(random.randint(1, 3))
        self.client.get("/home/create?id=" + id, name="/home/create")
        time.sleep(random.randint(1, 3))
        self.client.get("/view_tour/" + id, name="/view_tour")
