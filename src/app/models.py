import datetime
import random
import string
import uuid
import os

from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_security import UserMixin, RoleMixin
from sqlalchemy import Column, Integer, String, Text, Boolean, ForeignKey, DateTime, BigInteger, Float, and_
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.mysql import BIGINT

from werkzeug.security import generate_password_hash, check_password_hash
from dotenv import load_dotenv
load_dotenv()

db = SQLAlchemy()

roles_users = db.Table('roles_users',
                       Column('user_id', Integer(),
                              ForeignKey('users.id')),
                       Column('role_id', Integer(), ForeignKey('role.id')))


class User(db.Model, UserMixin):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True)
    username = Column(String(64), index=True, unique=True, nullable=False)
    password_hash = Column(String(256))
    email = Column(String(80), index=True, unique=True, nullable=True)
    active = Column(Boolean, default=True)
    created = Column(DateTime, default=datetime.datetime.utcnow)
    created_by = Column(Integer, ForeignKey('users.id'), default=None)
    reset_secret = Column(String(64), default=None)
    la_opt_out = Column(Boolean, default=False)
    fs_uniquifier = Column(String(255), unique=True,
                           nullable=False, default=lambda: str(uuid.uuid4()))
    used_quota_gb = Column(Float, default=0)
    individual_quota_gb = Column(Integer)
    language = Column(String(8), default=os.getenv('DEFAULT_LANGUAGE'))
    last_login = Column(DateTime, default=None)

    roles = relationship('Role', secondary=roles_users, backref='roled')
    tour = relationship("Tour", back_populates="creator")

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return "<User {}>".format(self.username)


class Role(db.Model, RoleMixin):
    __tablename__ = 'role'
    id = Column(Integer(), primary_key=True)
    name = Column(String(80), unique=True)
    permissions = Column(String(255), default='')
    default_quota_gb = Column(Integer)
    allow_as_role_in_group = Column(Boolean, default=False)
    assign_to_new_user = Column(Boolean, default=False)

    def get_permissions(self):
        return self.permissions.split(',')

    def set_permissions(self, permissions_list):
        self.permissions = ','.join(permissions_list)


class Asset(db.Model):
    __tablename__ = "assets"
    filename_id = Column(String(64), index=True, primary_key=True)
    name = Column(String(64), index=True)
    format = Column(String(80), index=True)
    created = Column(DateTime, nullable=False)
    media_type = Column(Text)
    annotation = Column(Text, nullable=True)
    tour_id = Column(Integer, ForeignKey('tour.id'),
                     nullable=False, primary_key=True)
    equirectangular = Column(Boolean, default=None, nullable=True)

    def __repr__(self):
        return "<Asset {}>".format(self.filename_id)


class Hotspot(db.Model):
    __tablename__ = "hotspots"
    id = Column(Integer, primary_key=True)
    media_page_id = Column(Integer, ForeignKey(
        "mediapages.id", name="fk_hotspot_media_page_id"))
    linked_media_page_id = Column(Integer)
    media_type_id = Column(Text)
    presented_media_id = Column(String(64))
    name = Column(String(64), nullable=False)
    desc = Column(String(512))
    position = Column(String(64), nullable=False)
    rotation = Column(String(64), nullable=False)
    size = Column(Float, default=1)
    custom_display_icon_id = Column(String(64))
    starttime = Column(Float, default=-1)
    endtime = Column(Float(asdecimal=True), default=-1)
    question = Column(String(512))
    visibleIfLocked = Column(Boolean, default=False)

    # custom_display_icon = relationship(
    #     "Asset", foreign_keys=[custom_display_icon_id])
    media_page = relationship("Mediapage", foreign_keys=[
                              media_page_id], back_populates="hotspots")
    dependencies = relationship(
        "Dependencies", foreign_keys="Dependencies.hotspot_id", back_populates="hotspot")
    EscapeRoomAnswers = relationship(
        "EscapeRoomAnswers", foreign_keys="EscapeRoomAnswers.hotspot_id", back_populates="hotspot")

    # linked_media_page = relationship("Mediapage", foreign_keys=[
    #                                  linked_media_page_id], back_populates="linked_hotspot")
    # presented_media = relationship("Asset")

    def to_dict(self):
        return {
            'id': self.id,
            'media_page_id': self.media_page_id,
            'linked_media_page_id': self.linked_media_page_id,
            'media_type_id': self.media_type_id,
            'presented_media_id': self.presented_media_id,
            'name': self.name,
            'desc': self.desc,
            'position': self.position,
            'rotation': self.rotation,
            'size': self.size,
            'custom_display_icon_id': self.custom_display_icon_id,
            'starttime': self.starttime,
            'endtime': self.endtime,
            'question': self.question,
            'visibleIfLocked': self.visibleIfLocked,
            "dependencies": [dep.to_dict() for dep in self.dependencies],
            "EscapeRoomAnswers": [answ.to_dict() for answ in self.EscapeRoomAnswers]
        }


class Mediapage(db.Model):
    __tablename__ = "mediapages"
    id = Column(Integer, primary_key=True)
    asset_id = Column(String(128))
    tour_id = Column(Integer, ForeignKey("tour.id"))
    is_start_page = Column(Boolean)
    name = Column(String(64), nullable=False)
    annotation = Column(Text, nullable=True)
    media_type = Column(String(32))
    tour = relationship("Tour", back_populates="mediapages")
    hotspots = relationship(
        "Hotspot", foreign_keys="Hotspot.media_page_id", back_populates="media_page")

    def to_dict(self):
        return {
            'id': self.id,
            'asset_id': self.asset_id,
            'tour_id': self.tour_id,
            'is_start_page': self.is_start_page,
            'name': self.name,
            'annotation': self.annotation,
            'media_type': self.media_type,
            "hotspots": [hotspot.to_dict() for hotspot in self.hotspots],
            'thumbnail': self.asset_id
        }


class Tour(db.Model):
    __tablename__ = "tour"
    id = Column(Integer, primary_key=True)
    creator_id = Column(Integer, ForeignKey("users.id"), nullable=False)
    name = Column(String(64), nullable=False)
    creator = relationship("User", back_populates="tour")
    mediapages = relationship("Mediapage", back_populates="tour")
    assets = relationship('Asset', backref='tour',
                          lazy='dynamic', cascade='all, delete-orphan')
    share_string = Column(String(32), unique=True)
    annotation = Column(Text, nullable=True)

    def __init__(self, *args, **kwargs):
        super(Tour, self).__init__(*args, **kwargs)
        self.generate_unique_share_string()

    def generate_unique_share_string(self):
        while True:
            allowed_chars = string.ascii_lowercase.replace(
                'l', '').replace('o', '') + string.digits.replace('0', '')
            share_string = ''.join(random.choice(allowed_chars)
                                   for _ in range(int(os.getenv('SHARE_LINK_LENGTH'))))
            existing_tour = Tour.query.filter_by(
                share_string=share_string).first()
            if not existing_tour:
                self.share_string = share_string
                break

    def set_share_string(self, string):
        self.share_string = string

    def get_start_page(self):
        return Mediapage.query.filter(and_(Mediapage.tour_id == self.id, Mediapage.is_start_page == True)).first()


class SharedTour(db.Model):
    __tablename__ = "sharedtour"
    id = Column(Integer, primary_key=True)
    tour_id = Column(Integer, ForeignKey("tour.id"), nullable=False)
    collaborator_id = Column(Integer, ForeignKey("users.id"), nullable=False)
    edit = Column(Integer)

    def get_start_page(self):
        return Mediapage.query.filter(and_(Mediapage.tour_id == self.tour_id, Mediapage.is_start_page == True)).first()


class SharedGroupTour(db.Model):
    __tablename__ = "sharedgrouptour"
    id = Column(Integer, primary_key=True)
    tour_id = Column(Integer, ForeignKey("tour.id"), nullable=False)
    group_id = Column(Integer, ForeignKey("group.id"), nullable=False)
    edit = Column(Integer)

    def get_start_page(self):
        return Mediapage.query.filter(and_(Mediapage.tour_id == self.tour_id, Mediapage.is_start_page == True)).first()


class Group(db.Model):
    __tablename__ = "group"
    id = Column(Integer, primary_key=True)
    creator_id = Column(Integer, ForeignKey("users.id"), nullable=False)
    name = Column(String(64), nullable=False)
    roles = Column(String(64), nullable=True)


class GroupUsers(db.Model):
    __tablename__ = "group_users"
    id = Column(Integer, primary_key=True)
    group_id = Column(Integer, ForeignKey("group.id"), nullable=False)
    user_id = Column(Integer, ForeignKey("users.id"), nullable=True)
    invite_token = Column(String(32), unique=True)
    invite_email = Column(String(80))
    accepted = Column(Boolean, default=None)

    def __init__(self, *args, **kwargs):
        super(GroupUsers, self).__init__(*args, **kwargs)
        self.generate_unique_invite_token()

    def generate_unique_invite_token(self):
        while True:
            invite_token = ''.join(random.choice(
                string.ascii_uppercase) for _ in range(6))
            existing_invite = GroupUsers.query.filter_by(
                invite_token=invite_token).first()
            if not existing_invite:
                self.invite_token = invite_token
                break


class Chat(db.Model):
    __tablename__ = "chats"
    id = Column(Integer, primary_key=True)
    tour_id = Column(Integer, ForeignKey("tour.id"), nullable=False)
    user_id = Column(Integer, ForeignKey("users.id"), nullable=False)
    created = Column(DateTime, default=datetime.datetime.utcnow)
    message = Column(Text, nullable=False)


class ChatUnread(db.Model):
    __tablename__ = "chats_unread"
    id = Column(Integer, primary_key=True)
    tour_id = Column(Integer, ForeignKey("tour.id"), nullable=False)
    user_id = Column(Integer, ForeignKey("users.id"), nullable=False)
    last_chat_id = Column(Integer, ForeignKey("chats.id"), nullable=False)


class TourAnnotationUnread(db.Model):
    __tablename__ = "tour_annotation_unread"
    id = Column(Integer, primary_key=True)
    tour_id = Column(Integer, ForeignKey("tour.id"), nullable=False)
    user_id = Column(Integer, ForeignKey("users.id"), nullable=False)
    last_annotation = Column(Text, nullable=True)


class Dependencies(db.Model):
    __tablename__ = "dependencies"
    id = Column(Integer, primary_key=True)
    hotspot_id = Column(Integer, ForeignKey("hotspots.id"))
    hotspot = relationship(
        "Hotspot", foreign_keys=[hotspot_id], back_populates="dependencies")
    dependsOn = Column(Integer, ForeignKey("hotspots.id"), nullable=False)

    def to_dict(self):
        return {
            'id': self.id,
            'hotspot_id': self.hotspot_id,
            'dependsOn': self.dependsOn
        }


class EscapeRoomAnswers(db.Model):
    __tablename__ = "EscapeRoomAnswers"
    id = Column(Integer, primary_key=True)
    hotspot_id = Column(Integer, ForeignKey("hotspots.id"))
    hotspot = relationship("Hotspot", foreign_keys=[
                           hotspot_id], back_populates="EscapeRoomAnswers")
    answer = Column(String(512))
    correct = Column(Boolean, default=False)

    def to_dict(self):
        return {
            'id': self.id,
            'hotspot_id': self.hotspot_id,
            'answer': self.answer,
            'correct': self.correct
        }


class HotspotVisited(db.Model):
    __tablename__ = "hotspotVisited"
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey("users.id"), nullable=False)
    tour_id = Column(Integer, ForeignKey("tour.id"), nullable=False)
    hotspot_id = Column(Integer, ForeignKey("hotspots.id"), nullable=False)

    def to_dict(self):
        return {
            'id': self.id,
            'user_id': self.user_id,
            'tour_id': self.tour_id,
            'hotspot_id': self.hotspot_id,
        }
