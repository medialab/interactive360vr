//this is a fork of https://github.com/extraymond/aframe-mouse-dragndrop
if (window.AFRAME == null) {
  console.error("aframe not found, please import it before this component.");
}

AFRAME.registerSystem("track-cursor", {
  init: function () {
    this.el.setAttribute("cursor", { rayOrigin: "mouse" });
  },
});

AFRAME.registerComponent("track-cursor", {
  init: function () {
    this.isDragging = false;
    this.dragTimeout = null;

    this.el.addEventListener("mousedown", (e) => {
      this.dragTimeout = setTimeout(() => {
        if (this.el.is("cursor-hovered")) {
          this.el.sceneEl.camera.el.setAttribute("look-controls", {
            enabled: false,
          });
          this.el.addState("dragging");
          this.isDragging = true;
        }
      }, 200);
    });

    this.el.addEventListener("mouseup", (e) => {
      clearTimeout(this.dragTimeout);
      if (this.isDragging) {
        this.el.sceneEl.camera.el.setAttribute("look-controls", {
          enabled: true,
        });
        this.el.removeState("dragging");
        this.isDragging = false;

        //update hotspot
        const position = this.el.object3D.position;
        const rotation = this.el.object3D.rotation;
        console.log(rotation.x + " " + rotation.y + " " + rotation.z);
        console.log(rotation);
        $.ajax({
          url:
            "{{ url_for('home.update_hotspot', hotspot_id='') }}" +
            this.el.getAttribute("data-hotspot-id"),
          type: "PUT",
          contentType: "application/x-www-form-urlencoded",
          data: $.param({
            position: position.x + " " + position.y + " " + position.z,
            rotation: rotation.x + " " + rotation.y + " " + rotation.z,
          }),
          success: function (data) {
            console.log("Hotspot updated successfully:", data);
          },
          error: function (error) {
            console.error("Error updating hotspot:", error);
          },
        });
      }
    });

    this.el.addEventListener("click", (e) => {
      if (this.isDragging) {
        e.preventDefault();
        e.stopPropagation();
      }
    });
  },
});

AFRAME.registerComponent("dragndrop", {
  dependencies: ["track-cursor"],
  init: function () {
    this.range = 0;
    this.dist = 0;

    this.el.addEventListener("stateadded", (e) => {
      if (e.detail == "dragging") {
        this.range = 0;
        this.dist = this.el.object3D.position
          .clone()
          .sub(this.el.sceneEl.camera.el.object3D.position)
          .length();
      }
    });

    this.direction = new AFRAME.THREE.Vector3();
    this.target = new AFRAME.THREE.Vector3();
    document.addEventListener("wheel", (e) => {
      if (e.deltaY < 0) {
        this.range += 0.1;
      } else {
        this.range -= 0.1;
      }
    });
  },
  updateDirection: function () {
    this.direction.copy(this.el.sceneEl.getAttribute("raycaster").direction);
  },
  updateTarget: function () {
    let camera = this.el.sceneEl.camera.el;
    this.target.copy(
      camera.object3D.position
        .clone()
        .add(this.direction.clone().multiplyScalar(this.dist + this.range)),
    );
  },
  tick: function () {
    if (this.el.is("dragging")) {
      this.updateDirection();
      this.updateTarget();
      this.el.object3D.position.copy(this.target);

      // Rotate the object to face the camera
      this.el.object3D.lookAt(this.el.sceneEl.camera.el.object3D.position);
    }
  },
});
