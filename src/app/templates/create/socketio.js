var socket = io();
socket.on("connect", function () {
  socket.emit("joinCreateTour", {
    tour_id: "{{ tour_id }}",
    user_id: "{{ current_user.id }}",
    username: "{{ current_user.username }}",
  });
});

socket.on("hotspot", function (id) {
  fetchHotspot(id);
});

socket.on("removeHotspot", function (id) {
  removeHotspot(id);
});

socket.on("updateMediaPages", function () {
  var selectedHotspot = document.querySelector(
    ".card-content.hotspot_entry.selected",
  );
  if (selectedHotspot) {
    var id = selectedHotspot.id.split("-").pop();
  }
  updateMediaPages(id);
});

socket.on("updateFileList", function (id) {
  updateFileList();
});

socket.on("removeAsset", function (id) {
  removeAsset(id);
});

socket.on("updateOnlineUsers", function (lists) {
  updateOnlineUsers(lists.tour_id, lists.users, lists.usersView);
});

socket.on("updateTourAnnotation", function (annotation) {
  updateTourAnnotation(annotation);
});

unreadChat = "";
socket.on("message", function (data) {
  if ($(".chat-popover").length > 0) {
    //chat is currently open
    $("#chat").val($("#chat").val() + data.msg + "\n");
    $("#chat").scrollTop($("#chat")[0].scrollHeight);

    $.ajax({
      url: "{{ url_for('home.set_chat_unread') }}",
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify({
        tour_id: "{{ tour_id }}",
      }),
      error: function (xhr, status, error) {
        console.error("Failed to set chat unread status");
      },
    });
  } else {
    //chat is currently closed
    $("#unreadCount").removeClass("d-none");
    $("#unreadCount").text(parseInt($("#unreadCount").text()) + 1);
    unreadChat += data.msg + "\n";
  }
});
