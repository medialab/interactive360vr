/**
 *
 * Here the created dummy parameters file_js are used to create an AssetCard  in Javascript from a Template.
 * this is very important to understand
 */

function createCard(file) {
  let cardHtml = `{% with file=file_js %}
                                {% include "create/pic_card.html" %}
                            {% endwith %}`;
  let card = $(cardHtml);
  if (file.media_type == "Audio") {
    card.find(".overlay-asset-button").remove();
    let img = card.find(".assetcard_pic");
    img.attr(
      "src",
      "{{ url_for('static', filename='img/hotspots/audio.png')}}",
    );
  }
  $(".uploaded-files-list").append(card);
}

/**
 *
 * Here the created dummy parameters media_js are used to create a mediaCard in Javascript from a Template.
 * this is very important to understand
 */
function createMediaCard(file) {
  let cardHtml = `{% with mediapage=media_js %}
                                {% include "create/media_card.html" %}
                            {% endwith %}`;
  let card = $("<div>")
    .append(cardHtml)
    .appendTo(".media-page-container")
    .get(0);

  return card;
}

/**
 * A quiet long function which first fetches all Mediapages from the Server. Then it removes all Mediapage-Cards to create new from the Server Response
 */
function updateMediaPages(markedHotspot = null, selectedMediaPage = null) {
  var scroll = $("#media-pages")[0].scrollTop;
  $.ajax({
    url: "{{ url_for('home.get_media_pages') }}",
    type: "GET",
    data: {
      tour_id: _tour_id,
    },
    success: function (response) {
      _hotspots = [];
      //Remove ALl MediaCards
      $(".mediapage_card").remove();
      //MediaPageNames for Dependencies
      _mediaPageNames = response.reduce((acc, curr) => {
        acc[curr.id] = curr.name;
        return acc;
      }, {});

      //handle start-messages
      if (response.length === 0) {
        if ($(".pic_card").length === 0) {
          // show "Begin by uploading your media on the left"
          $("#start-message-scene").addClass("d-none");
          $("#start-message-upload").removeClass("d-none");
        } else {
          // show "Add your uploaded file as scene by clicking on the +"
          $("#start-message-upload").addClass("d-none");
          $("#start-message-scene").removeClass("d-none");
        }
      } else {
        // remove start messages
        $("#start-message-upload").addClass("d-none");
        $("#start-message-scene").addClass("d-none");
      }
      response.forEach(function (file) {
        var card = document.querySelector(
          `.mediapage_card[data-media-id="${file.id}"]`,
        );
        // If card is null, then this card doesn't exist, so we have to create it
        if (card == null) {
          //This should be the regular case
          card = createMediaCard(file);
          if (card.querySelector(".annotation").title == "null") {
            card.querySelector(".annotation").title = '{{_("Notes")}}';
          } else {
            card.querySelector(".annotation").title = file.annotation.replace(
              /\n/g,
              "<br>",
            );
          }
          new bootstrap.Tooltip(card.querySelector(".annotation")).update();
        } else {
          // for the case a card needs to be updated
          card.querySelector(".mediapage_name").textContent = file.name;
          card.querySelector(".mediapage_pic").src =
            "{{ url_for('base.serve_uploaded_file', filename='') }}" +
            file.asset_id;
        }
        //Update the Click Event
        registerMediaPicClickEvent();
        if (file.is_start_page) {
          card.querySelector(".mediapage_pic").classList.add("is-start-page");
          card.querySelector(".mediapage_pic").click();
          _currentMediapage = file.id;
          let startpage = $(".start-page", card);
          startpage.replaceWith(`
            <button
              class="p-1 card-buttons start-page"
              data-media-id="${file.id}"
              data-bs-toggle="tooltip"
              title="{{_("This is set as Start Scene")}}"
            >
              <i class="bi bi-house-fill"></i>
            </button>
          `);
        }
        // Update hotspots
        const hotspotsContainer = card.querySelector(".hotspot-container");

        _hotspots = _hotspots.concat(file.hotspots);
        file.hotspots.forEach(function (hotspot) {
          let hotspotEntry = hotspotsContainer.querySelector(
            `#hotspot-${hotspot.id}`,
          );
          if (hotspotEntry == null) {
            let linked = "";
            let linkedEmpty = false;
            // Create a new hotspot entry if it doesn't exist
            switch (hotspot.media_type_id) {
              case "Move":
                linked = _mediaPageNames[hotspot.linked_media_page_id]
                  ? '{{_("to")}}: ' +
                    _mediaPageNames[hotspot.linked_media_page_id]
                  : '{{_("Linked Scene is Empty")}}';
                linkedEmpty = !_mediaPageNames[hotspot.linked_media_page_id];
                break;
              case "Text":
                linked = hotspot.desc || '{{_("Hotspot is Empty")}}';
                linkedEmpty = !hotspot.desc;
                break;
              case "Image":
              case "Video":
              case "Audio":
                linked =
                  hotspot.presented_media_id &&
                  ![
                    "-1",
                    "hotspot_card_Image",
                    "hotspot_card_Video",
                    "hotspot_card_Audio",
                  ].includes(hotspot.presented_media_id)
                    ? hotspot.presented_media_id
                    : `{{_("${hotspot.media_type_id}")}} {{_("is Empty")}}`;
                linkedEmpty =
                  !hotspot.presented_media_id ||
                  [
                    "-1",
                    "hotspot_card_Image",
                    "hotspot_card_Video",
                    "hotspot_card_Audio",
                  ].includes(hotspot.presented_media_id);
                break;
              case "Quiz":
              case "Combination":
                linked = hotspot.question
                  ? hotspot.question
                  : '{{_("Question is Empty")}}';
                linkedEmpty = !hotspot.question;
                break;
            }
            const hotspotHtml = `
              <table class="w-100 card-content hotspot_entry" style="text-align: left; word-break: break-word;" id="hotspot-${hotspot.id}"
              data-hotspot-id="${hotspot.id}">
              <tr style="height:1em">
              <td class="align-middle ps-1 " style="width: 1em; position: relative">
              <img
              src="{{ url_for('static', filename='img/hotspots') }}/${hotspot.media_type_id.toLowerCase()}.png"
              style="width: 1em; height: 1em;"
              draggable="false"
              data-bs-toggle="tooltip"
              title="${linked}"
              />
              ${linkedEmpty ? '<div style="position: absolute; top: 0; right: 0; width: 10px; height: 10px;" class="warning" ><i class="bi bi-exclamation"></i></div>' : ""}
              </td>
              <td class="align-middle ps-1 hotspotName text-wrap ">
              ${hotspot.name}
              </td>
              <td class="align-middle ps-1" style="width: 1em;">
              <button type="button" class="btn btn-sm btn-removehotspot" data-hotspot-id="${hotspot.id}"
              onmouseover="this.querySelector('i').classList.replace('bi-x-circle', 'bi-x-circle-fill')"
              onmouseout="this.querySelector('i').classList.replace('bi-x-circle-fill', 'bi-x-circle')">
              <i class="bi bi-x-circle"></i>
              </button>
              </td>
              </tr>
              </table>
            `;
            hotspotsContainer.insertAdjacentHTML("beforeend", hotspotHtml);
            $(".btn-removehotspot").off("click");
            registerHotspotRemoveButtons();
          } else {
            // Update existing hotspot entry data
            hotspotEntry.querySelector(".hotspotName").textContent =
              hotspot.name;
          }
          registerHotSpotEntryClickEvent();
        });
        markedHotspot
          ? $("#hotspot-" + markedHotspot).click()
          : $("#hotspot-message-choose").toggleClass(
              "d-none",
              _hotspots.length == 0,
            );
      });
      if (selectedMediaPage) {
        $("#img-media-" + selectedMediaPage).click();
        scroll =
          $("#img-media-" + selectedMediaPage).offset().top -
          $("#media-pages").offset().top +
          $("#media-pages").scrollTop();
      }
      $("#media-pages")[0].scrollTop = scroll; //restore scrolling position
      $('[data-bs-toggle="tooltip"]').tooltip(); // Update tooltips

      $("#hotspot-message-add").toggleClass(
        "d-none",
        !(
          _hotspots.length == 0 &&
          $("#start-message-upload").hasClass("d-none") &&
          $("#start-message-scene").hasClass("d-none")
        ),
      );
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log("Error fetching file list: " + errorThrown);
    },
  });
}

async function setHotspotReset() {
  userResponse = confirm(
    "Do you really want to reset all Hotspots? All progress will be lost!",
  );
  if (userResponse) {
    console.log("reset");
    const response = await fetch("{{ url_for('base.setHotspotReset') }}", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      body: JSON.stringify({
        tour_id: "{{ tour_id }}",
      }),
    });

    if (response.ok) {
      test = await response.json();
      console.log(test.message);
    } else {
      console.error("Failed to set Hotspot Reset");
    }
  }
}

async function toggleVisibility(hotspotid, e) {
  console.log("Triggered Visibility" + hotspotid);
  console.log(e.checked);
  let count = 0;
  let planesToMark;
  while (!planesToMark || planesToMark.length === 0 || count == 40) {
    count++;
    //console.log("waited " + count)
    //Wait if Plane not loaded
    await waitForPlane();
    planesToMark = $('a-plane[data-hotspot-id="' + hotspotid + '"]');
  }
  if (e.checked) {
    planesToMark.each(function () {
      $(this).attr("visible", true);
      $(this).attr("visible", true);

      console.log($(this));
    });
  } else {
    planesToMark.each(function () {
      $(this).attr("visible", false);
    });
  }
}
async function toggleDependency(
  hotspot_id,
  dependsOnid,
  media_page_id,
  checkbox,
) {
  const response = await fetch("{{ url_for('base.setDependency') }}", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify({ hotspot_id, dependsOnid, media_page_id }),
  });

  if (response.ok) {
    test = await response.json();

    if (test.message == "Deadlock detected") {
      alert(
        "Deadlock detected, please remove the dependency from the other hotspot first",
      );
      checkbox.checked = false;
    } else if (test.message == "Dependency added") {
      _currentDependencies.push(dependsOnid);
      updateLockDependencyVisibility(hotspot_id);
    } else if (test.message == "Dependency removed") {
      _currentDependencies = _currentDependencies.filter(
        (id) => id !== dependsOnid,
      );
      updateLockDependencyVisibility(hotspot_id);
    }
  } else {
    console.error("Failed to add Dependency");
  }
}
function updateLockDependencyVisibility(hotspotid) {
  let div = document.getElementById("lockDependencyDiv");
  let locked = document.getElementById("lockedDependencyCheckbox");
  if (_currentDependencies.length > 0) {
    div.removeAttribute("hidden");
  } else {
    div.setAttribute("hidden", "true");
    locked.checked = false;
    toggleVisibleIfLocked(hotspotid, false);
  }
}
async function toggleVisibleIfLocked(id, inputField) {
  _hotspots.find((obj) => obj.id == id).visibleIfLocked = inputField;
  console.log(inputField);
  const response = await fetch("{{ url_for('base.setVisibleIfLocked') }}", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify({ hotspotid: id, state: inputField }),
  });

  if (response.ok) {
    test = await response.json();
    console.log(test.message);
  } else {
    console.error("Failed to set VisibleIfLocked");
  }
}
//Update Quiz Question
async function toggleQuizQuestion(hotspotid, question) {
  const response = await fetch("{{ url_for('base.setQuizQuestion') }}", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify({ hotspotid, question }),
  });

  if (response.ok) {
    console.log("Question set");
    question ? $(`#hotspot-${hotspotid} .warning`).remove() : "";
  } else {
    console.error("Failed to set Question");
  }
}
//add Answer to Quiz
async function quizAddAnswer(hotspotid) {
  const response = await fetch("{{ url_for('base.addQuizAnswer') }}", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify({ hotspotid }),
  });

  if (response.ok) {
    let answerid = await response.json();
    return answerid.id;
  } else {
    console.error("Failed to set Answer");
  }
}

async function quizDeleteAnswer(answerid) {
  const response = await fetch("{{ url_for('base.deleteQuizAnswer') }}", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify({ answerid }),
  });

  if (response.ok) {
    console.log("Answer Deleted");
  } else {
    console.error("Failed to delete Answer");
  }
}
async function quizUpdateAnswer(inputId, answerText) {
  const response = await fetch("{{ url_for('base.setQuizAnswer') }}", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify({ inputId, answerText }),
  });

  if (response.ok) {
    console.log("Answer set");
  } else {
    console.error("Failed to set Answer");
  }
}
async function quizToggleAnswer(inputId, answerState) {
  const response = await fetch("{{ url_for('base.toggleQuizAnswer') }}", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify({ inputId, answerState }),
  });

  if (response.ok) {
    console.log("Answer state set");
  } else {
    console.error("Failed to set Answer State");
  }
}
async function getDependency(id) {
  const response = await fetch("{{ url_for('base.getDependency') }}", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify({ id }),
  });

  if (response.ok) {
    const currentDepend = await response.json();
    return currentDepend;
  } else {
    console.error("Failed to add Dependency");
  }
}

async function generateVideoThumbs() {
  // Iterate over each image with class mediapage_pic
  $("img.mediapage_pic").each(function () {
    // Check if source ends with a video file extension
    var src = $(this).attr("src");
    if (
      src.endsWith(".mp4") ||
      src.endsWith(".webm") ||
      src.endsWith(".mov") ||
      src.endsWith(".mkv") ||
      src.endsWith(".avi")
    ) {
      // Create a new video element
      var video = document.createElement("video");
      video.src = src;
      video.type = $(this).attr("type");
      var img = this;

      video.addEventListener("loadedmetadata", function () {
        // Create a new canvas element
        var canvas = document.createElement("canvas");
        canvas.width = this.videoWidth;
        canvas.height = this.videoHeight;

        // Draw the video frame to the canvas
        canvas
          .getContext("2d")
          .drawImage(this, 0, 0, canvas.width, canvas.height);

        // Set the image source to the data URL of the canvas
        $(img).attr("src", canvas.toDataURL());
      });
    }
  });
}

/**
 * Update the global Variable Hotspots and creates Planes from it in the A-Frame Scene
 */
async function fetchHotspot(id, markedHotspot = null) {
  const response = await fetch("{{ url_for('base.get_hotspot') }}", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify(id),
  });

  if (response.ok) {
    const hotspot = await response.json();
    // new hotspot
    if (!_hotspots.some((h) => h.id === hotspot.id)) {
      _hotspots.push(hotspot);
      // udpate existing hotspot
    } else {
      _hotspots[_hotspots.findIndex((h) => h.id === hotspot.id)] = hotspot;
      $('a-plane[data-hotspot-id="' + hotspot.id + '"]').remove();

      //remove ! if value is set
      switch (hotspot.media_type_id) {
        case "Move":
          _mediaPageNames[hotspot.linked_media_page_id]
            ? $(`#hotspot-${hotspot.id} .warning`).remove()
            : "";
          break;
        case "Text":
          hotspot.desc ? $(`#hotspot-${hotspot.id} .warning`).remove() : "";
          break;
        case "Image":
        case "Video":
        case "Audio":
          hotspot.presented_media_id
            ? $(`#hotspot-${hotspot.id} .warning`).remove()
            : "";
          break;
        case "Quiz":
        case "Combination":
          hotspot.question ? $(`#hotspot-${hotspot.id} .warning`).remove() : "";
          break;
      }
    }
    if (_currentMediapage == hotspot.media_page_id) {
      createPlanesFromHotspot(hotspot);
    }
    markedHotspot ? $("#hotspot-" + markedHotspot).click() : "";
  } else {
    console.error("Failed to fetch hotspot");
  }
}

/**
 * Sets the global Variable Hotspots and creates Planes from it in the A-Frame Scene
 */
async function fetchHotspots(tour_id, mediapage_id) {
  const response = await fetch("{{ url_for('base.get_hotspots') }}", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify({ tour_id, mediapage_id }),
  });

  if (response.ok) {
    const hotspots = await response.json();
    //console.log("Hotspots:", hotspots);
    createPlanesFromHotspots(hotspots);
  } else {
    console.error("Failed to fetch hotspots");
  }
}

/***
 * Function for marking selected Hotspot Entrys
 */
document.addEventListener("DOMContentLoaded", () => {
  registerHotSpotEntryClickEvent();
});

/**
 *  Very Important files.off(); to unregister all click event and then reregister it this avoids multiple click events on the Hotspot Entrys
 *  Hotspot Entrys are the items you can click in the right column
 */
function registerHotSpotEntryClickEvent() {
  const files = $(".hotspot_entry");
  files.off();
  files.on("click", function (e) {
    let id = $(this).data("hotspot-id");
    const hot = _hotspots.find((obj) => obj.id == id);
    if (hot.media_page_id != _currentMediapage) {
      let mediapagePic =
        this.closest(".mediapage_card").querySelector(".mediapage_pic");

      // Trigger the click event on the nearest mediapage_pic element
      if (mediapagePic) {
        mediapagePic.click();
      }
    }
    $("#media-page-container").children().children().removeClass("border-dark");
    $(this).closest(".card").toggleClass("border-dark");

    unmarkAllHotspots(e);

    // Toggle the "selected" class for the clicked element
    this.closest(".hotspot_entry").classList.add("selected");

    //Make the selected Blue
    directCam(id);

    getDependency(id).then((currDep) => {
      _currentDependencies = currDep;
      displayProperties(e.target.closest(".hotspot_entry"));
    });

    //Set Properties
    createVisibleTimeline(id);

    //scroll down to to current scene
    const $container = $("#media-pages");
    const $targetDiv = $(this).closest(".card");
    $container.scrollTop(
      $targetDiv.offset().top -
        $container.offset().top +
        $container.scrollTop(),
    );
  });
}

//unmarks all other Hotspots
function unmarkAllHotspots(e) {
  const files = $(".hotspot_entry");
  files.each((index, otherFile) => {
    if (otherFile !== e.target) {
      $(otherFile).removeClass("selected");
      // Unmark all other planes
      PlaneId = $(otherFile).data("hotspot-id");
      planesToMark = $('a-plane[data-hotspot-id="' + PlaneId + '"]');
      planesToMark.each(function () {
        $(this).attr("material", "transparent: true");
        $(this).attr("material", "color: white");
      });
    }
  });
}

/**
 * This Function directs the Camera to the plane which belongs to a hotspot which is clicked.
 */
async function directCam(clickedId) {
  let count = 0;
  let planesToMark;

  while (!planesToMark || planesToMark.length === 0) {
    if (count >= 40) break;
    count++;
    await new Promise((resolve) => setTimeout(resolve, 10));
    planesToMark = $(`a-plane[data-hotspot-id="${clickedId}"]`);
  }

  if (planesToMark && planesToMark.length > 0) {
    panCameraToPlane(planesToMark);
    const primaryColor = getComputedStyle(document.body).getPropertyValue(
      "--bs-primary",
    );
    planesToMark.each(function () {
      $(this).attr("material", `color:${primaryColor}`);
    });
  }
}

/**
 *Creates a Timeline which can determin when a hotspot is visible on a plane this Function adds also all functionality to the timeline slider
 */
async function createVisibleTimeline(id) {
  $("#visible-timeline").remove();
  let visibleTimelineEl = $('<div id="visible-timeline" ></div>');
  $("#aframe-container").append(visibleTimelineEl);
  let visibleTimeline = document.querySelector("#visible-timeline");
  let videoEl = document.querySelector("#my-video");
  const hot = _hotspots.find((obj) => obj.id == id);

  //It is very important to get the duration of the video, so the slider can have the right length
  function waitForMetadata(videoEl) {
    return new Promise((resolve) => {
      videoEl.addEventListener("loadedmetadata", function () {
        resolve(videoEl.duration);
      });
    });
  }

  let duration;
  if (_metadataloaded) {
    duration = videoEl.duration;
  } else {
    duration = await waitForMetadata(videoEl);
  }
  if (videoEl.duration) {
    let slider = noUiSlider.create(visibleTimeline, {
      start: [hot.starttime, hot.endtime],
      connect: true,
      range: {
        min: 0,
        max: duration, // this should be set to video duration once it's loaded
      },
    });
    slider.on("set", function (values) {
      //Updates the Data in the Database according to the user Input
      const url =
        "{{ url_for('home.update_hotspot', hotspot_id='') }}" + hot.id;

      const data = new FormData();
      data.append("starttime", values[0]);
      data.append("endtime", values[1]);

      $.ajax({
        type: "PUT",
        url: url,
        data: data,
        contentType: false,
        processData: false,
        success: function (response) {
          console.log(response);
          const index = _hotspots.findIndex((hotspot) => hotspot.id == id);
          _hotspots[index] = response; // Update the global _hotspots array
          //closeProperties();
          socket.emit("updateMediaPages", {
            tour: "{{ tour_id }}",
          });
          updateMediaPages();
        },
        error: function (response) {
          console.log(response);
        },
      });

      const selector = `.hotspot-plane[data-hotspot-id="${hot.id}"]`;
      var plane = document.querySelector(selector);

      if (videoEl.currentTime > values[0] && videoEl.currentTime < values[1]) {
        plane.setAttribute("visible", true);
      } else {
        plane.setAttribute("visible", false);
      }
    });
  }
}

/**
 * Create the HTML Propertie entrys in the HTML
 */
function displayProperties(hotspot) {
  $("#hotspot-message-choose").addClass("d-none");
  _selectedHotspot = hotspot;
  let container = $("#propertiesContainer");
  container.html("");
  let id = hotspot.id.split("-").pop();
  const hot = _hotspots.find((obj) => obj.id == id);
  const move_hot = _hotspots.find((obj) => obj.type == "Move");

  // Create a form element to contain the inputs and dropdowns
  let form = $('<form id="hotspot-form" class="p-2"></form>');

  // Create an input for the hotspot's name and set its current value
  let name = $("<input>")
    .attr("type", "text")
    .attr("name", "name")
    .attr("placeholder", '{{_("Name")}}')
    .attr("id", "InputText")
    .attr("maxlength", "50")
    .addClass("form-control")
    .val(hot.name)
    .change(function () {
      updateHotspot(hot.id, form.serializeArray());
    });
  form.append('<label for="InputText">{{_("Name")}}</label>');
  // Append the name input to the form
  form.append(name);

  // If the hotspot is a "Move" type
  if (hot.media_type_id === "Move") {
    // Create a dropdown for linked media pages
    let mediaPagesDropdown = $("<select></select>")
      .attr("name", "linked_media_page_id")
      .addClass("form-select")
      .change(function () {
        updateHotspot(hot.id, form.serializeArray());
      });

    mediaPagesDropdown.append(
      $("<option></option>")
        .attr("value", -1)
        .attr("id", "propertiesLinkedMediapage")
        .text('{{_("Empty")}}'),
    );
    form.append(
      `<label for="propertiesLinkedMediapage">
      {{_("Linked Scene")}}               
      <i class="bi bi-info-circle"
        data-bs-toggle="tooltip"
        data-bs-placement="top"
        title="{{_("Set the scene to move to")}}">
      </i>
      </label>`,
    );
    // Iterate through all media page cards
    $(".mediapage_card").each(function () {
      let mediaPageId = $(this).data("media-id");
      let mediaPageName = $(this).data("media-name");

      // Only add the option if this is not the current mediapage
      if (mediaPageId !== hot.media_page_id) {
        // Create an option for each media page with its ID and name
        let option = $("<option></option>")
          .attr("value", mediaPageId)
          .text(mediaPageName);

        // Set the current linked media page as selected
        if (hot.linked_media_page_id === mediaPageId) {
          option.attr("selected", "selected");
        }

        // Append the option to the media pages dropdown
        mediaPagesDropdown.append(option);
      }
    });

    // Append the media pages dropdown to the form
    form.append(mediaPagesDropdown);
  }
  // If the hotspot is a "Image" type
  else if (hot.media_type_id === "Image") {
    let mediaDropdown = getAllAssetsByType(
      hot.presented_media_id,
      "presented_media_id",
      "Image",
    );
    mediaDropdown.change(function () {
      updateHotspot(hot.id, form.serializeArray());
    });
    // Append the media dropdown to the form
    form.append(
      '<label for="propertiesLinkedImage">{{_("Set Image")}}</label>',
    );
    form.append(mediaDropdown);
  }
  // If the hotspot is a "Text" type
  else if (hot.media_type_id === "Text") {
    let desc = hot.desc ? hot.desc : "";
    let textarea = $(`<textarea>${desc}</textarea>`)
      .attr("name", "desc")
      .attr("id", "PropertiesTextArea")
      .attr("style", "field-sizing: content;")
      .addClass("form-control")
      .text(hot.text_content)
      .change(function () {
        updateHotspot(hot.id, form.serializeArray());
      });
    form.append(
      '<label for="PropertiesTextArea">{{_("Hotspot Text")}}</label>',
    );
    form.append(textarea);
  }
  // If the hotspot is a "Video" type
  else if (hot.media_type_id === "Video") {
    let mediaDropdown = getAllAssetsByType(
      hot.presented_media_id,
      "presented_media_id",
      "Video",
    );
    mediaDropdown.change(function () {
      updateHotspot(hot.id, form.serializeArray());
    });
    // Append the media dropdown to the form
    form.append(
      '<label for="propertiesLinkedImage">{{_("Set Video")}}</label>',
    );
    form.append(mediaDropdown);
  } else if (hot.media_type_id === "Audio") {
    let mediaDropdown = getAllAssetsByType(
      hot.presented_media_id,
      "presented_media_id",
      "Audio",
    );
    mediaDropdown.change(function () {
      updateHotspot(hot.id, form.serializeArray());
    });
    // Append the media dropdown to the form
    form.append(
      '<label for="propertiesLinkedImage">{{_("Set Audio")}}</label>',
    );
    form.append(mediaDropdown);
  } else if (hot.media_type_id === "Quiz") {
    // Create a quiz wrapper
    let quizWrapper = document.createElement("div");
    quizWrapper.classList.add("quizWrapper");
    quizWrapper.append('{{_("Question")}}:');

    let questionWrapper = document.createElement("div");
    questionWrapper.classList.add("questionWrapper");
    quizWrapper.append(questionWrapper);
    let answerWrapper = document.createElement("div");
    answerWrapper.classList.add("answerWrapper");
    quizWrapper.append(answerWrapper);
    let question = document.createElement("input");
    question.type = "text";
    question.placeholder = '{{_("Question")}}';
    //get question text from _hotspots
    questionText = _hotspots.find((obj) => obj.id == id).question;
    console.log(questionText);
    question.value = questionText;
    question.classList.add("quizQuestionInput");
    questionWrapper.append(question);

    let addAnswer = document.createElement("button");
    addAnswer.classList.add("quizAnswer");
    addAnswer.classList.add("quizButtonAddAnswer");
    addAnswer.innerHTML = '{{_("Add Answer")}}';
    answerWrapper.append(addAnswer);

    const buildAnswer = ({ id, answer, correct }) => {
      console.log("buildAnswer", id, answer, correct);
      let answerDiv = document.createElement("div");
      answerDiv.classList.add("quizAnswer");
      let answerInput = document.createElement("input");
      answerInput.placeholder = '{{_("Answer")}}';
      answerInput.classList.add("quizAnswerInput");
      answerInput.type = "text";
      answerInput.value = answer;
      answerInput.id = "answerInput_" + id;
      let correctInput = document.createElement("input");
      correctInput.classList.add("quizRadioCorrect");
      correctInput.type = "checkbox";
      correctInput.checked = correct;
      correctInput.addEventListener("change", (event) => {
        //backendlogic to set answer to true or false
        quizToggleAnswer(answerInput.id.split("_")[1], event.target.checked);
        _hotspots
          .find((obj) => obj.id == hot.id)
          .EscapeRoomAnswers.find(
            (obj) => obj.id == answerInput.id.split("_")[1],
          ).correct = event.target.checked;
      });
      answerInput.addEventListener("change", (event) => {
        quizUpdateAnswer(answerInput.id.split("_")[1], event.target.value);
        _hotspots
          .find((obj) => obj.id == hot.id)
          .EscapeRoomAnswers.find(
            (obj) => obj.id == answerInput.id.split("_")[1],
          ).answer = event.target.value;

        //backendlogic to update answer
      });
      let deletebutton = document.createElement("button");
      deletebutton.classList.add("quizButtonRemoveAnswer");
      deletebutton.addEventListener("click", (event) => {
        event.preventDefault();
        quizDeleteAnswer(answerInput.id.split("_")[1]);
        answerDiv.remove();
        //backendlogic to delete answer from database
      });
      deletebutton.innerHTML = "&#x2716;";
      answerDiv.append(answerInput);
      answerDiv.append(deletebutton);
      answerDiv.append(correctInput);
      answerWrapper.insertBefore(answerDiv, addAnswer);
    };

    //get answers from _hotspots
    _hotspots
      .find((obj) => obj.id == hot.id)
      .EscapeRoomAnswers.map(buildAnswer);

    //add answers
    addAnswer.addEventListener("click", (event) => {
      event.preventDefault();
      //backendlogic to add answer and get id
      quizAddAnswer(hot.id).then((response) => {
        buildAnswer({ id: response, answer: "", correct: false });
        //add new answer to _hotspots
        _hotspots
          .find((obj) => obj.id == hot.id)
          .EscapeRoomAnswers.push({ id: response, answer: "", correct: false });
      });
    });
    question.addEventListener("change", (event) => {
      //backendlogic to update question
      toggleQuizQuestion(hot.id, event.target.value);
      _hotspots.find((obj) => obj.id == hot.id).question = event.target.value;
    });

    form.append('<label for="propertiesLinkedImage">{{_("Set Quiz")}}</label>');
    form.append(quizWrapper);
  } else if (hot.media_type_id === "Combination") {
    let CombinationWrapper = document.createElement("div");
    CombinationWrapper.classList.add("quizWrapper");
    CombinationWrapper.append('{{_("Instruction")}}:');

    let questionWrapper = document.createElement("div");
    questionWrapper.classList.add("questionWrapper");
    CombinationWrapper.append(questionWrapper);
    let answerWrapper = document.createElement("div");
    answerWrapper.classList.add("answerWrapper");
    CombinationWrapper.append(answerWrapper);
    let question = document.createElement("input");
    question.type = "text";
    question.placeholder = '{{_("Instruction")}}';
    //get question text from _hotspots
    questionText = _hotspots.find((obj) => obj.id == id).question;
    console.log(questionText);
    question.value = questionText;
    question.classList.add("quizQuestionInput");
    questionWrapper.append(question);

    let addOption = document.createElement("button");
    addOption.classList.add("quizAnswer");
    addOption.classList.add("quizButtonAddAnswer");
    addOption.innerHTML = "Add Option";
    answerWrapper.append(addOption);

    const buildAnswer = ({ id, answer, correct }) => {
      console.log("buildAnswer", id, answer, correct);
      let answerDiv = document.createElement("div");
      answerDiv.classList.add("quizAnswer");
      let answerInput = document.createElement("input");
      answerInput.placeholder = "Option";
      answerInput.classList.add("quizAnswerInput");
      answerInput.type = "text";
      answerInput.value = answer;
      answerInput.id = "answerInput_" + id;
      answerInput.addEventListener("change", (event) => {
        quizUpdateAnswer(answerInput.id.split("_")[1], event.target.value);
        _hotspots
          .find((obj) => obj.id == hot.id)
          .EscapeRoomAnswers.find(
            (obj) => obj.id == answerInput.id.split("_")[1],
          ).answer = event.target.value;

        //backendlogic to update answer
      });
      let deletebutton = document.createElement("button");
      deletebutton.classList.add("quizButtonRemoveAnswer");
      deletebutton.addEventListener("click", (event) => {
        event.preventDefault();
        quizDeleteAnswer(answerInput.id.split("_")[1]);
        answerDiv.remove();
        //backendlogic to delete answer from database
      });
      deletebutton.innerHTML = "&#x2716;";
      answerDiv.append(answerInput);
      answerDiv.append(deletebutton);
      answerWrapper.insertBefore(answerDiv, addOption);
    };

    //get answers from _hotspots
    _hotspots
      .find((obj) => obj.id == hot.id)
      .EscapeRoomAnswers.map(buildAnswer);

    //add answers
    addOption.addEventListener("click", (event) => {
      event.preventDefault();
      //backendlogic to add answer and get id
      quizAddAnswer(hot.id).then((response) => {
        buildAnswer({ id: response, answer: "", correct: false });
        //add new answer to _hotspots
        _hotspots
          .find((obj) => obj.id == hot.id)
          .EscapeRoomAnswers.push({ id: response, answer: "", correct: false });
      });
    });
    question.addEventListener("change", (event) => {
      //backendlogic to update question
      toggleQuizQuestion(hot.id, event.target.value);
      _hotspots.find((obj) => obj.id == hot.id).question = event.target.value;
    });

    form.append(
      '<label for="propertiesLinkedImage">{{_("Set Combination")}}</label>',
    );
    form.append(CombinationWrapper);
  }
  // Append the Update button to the form
  form.append('<label for="propertiesRotation">{{_("Rotation")}}</label>');

  // Hotspot Icon Rotation
  let rotationInput = $("<input>")
    .attr("id", "propertiesRotation")
    .attr("name", "z-rotation")
    .attr("type", "range")
    .attr("min", -180)
    .attr("max", 180)
    .attr("dir", "rtl")
    .attr("value", parseInt(hot.rotation.split(" ").slice(2).join(" ")))
    .addClass("form-range ps-3 pe-3")
    .change(function () {
      updateHotspot(hot.id, form.serializeArray());
    });

  form.append(rotationInput);
  let rotationValue = $("<span>")
    .attr("id", "rotationValue")
    .text(rotationInput.val() * -1 + "°")
    .addClass("ms-2");

  rotationInput.on("input", function () {
    rotationValue.text($(this).val() * -1 + "°");
  });
  rotationValue.insertBefore(rotationInput);

  //Create an Selection for the hotspots dependencies
  let dependency = $("<div>").addClass("dependencyWrapper");
  let options = _hotspots;
  refreshDependencyList("", false);

  let checkVisibleLock = hot.visibleIfLocked ? "checked" : "unchecked";
  form.append(`
       <div style="margin-top: 10px">
         <label for="InputDependency">{{_("Dependencies")}} <i class="bi bi-info-circle"
      data-bs-toggle="tooltip"
      data-bs-placement="top"
      title="{{_("The hotspot is only shown when already interacted with the selected dependencies")}}">
    </i></label>
       </div>
     `);
  //searchbar for dependencies
  let searchbar = $("<input>")
    .attr("type", "text")
    .attr("id", "searchbar")
    .attr("placeholder", "{{_('Search for Hotspot')}}")
    .attr("maxlength", "50")
    .addClass("form-control");

  let searchforChecked = $("<input>")
    .attr("type", "checkbox")
    .attr("id", "checkFilterInput")
    .attr("unchecked", "unchecked")
    .attr("style", "margin-left:10px");

  searchforChecked.on("change", function () {
    refreshDependencyList(searchbar.val().toLowerCase(), this.checked);
  });
  searchbar.on("input", function () {
    refreshDependencyList(
      $(this).val().toLowerCase(),
      $("#checkFilterInput").is(":checked"),
    );
  });

  function refreshDependencyList(searchString, filterChecked) {
    dependency.empty();
    console.log(filterChecked);
    //form.remove(".dependencyOptionWrapper");
    let filteredOptions = options.filter((option) =>
      option.name.toLowerCase().includes(searchString),
    );
    console.log(filteredOptions);
    if (filterChecked) {
      filteredOptions = filteredOptions.filter((option) =>
        _currentDependencies.includes(option.id),
      );
    }

    filteredOptions.forEach(function (option) {
      if (id != option.id) {
        let isChecked = _currentDependencies.includes(option.id)
          ? "checked"
          : "unchecked";
        let opt = $("<div>").addClass(
          "dependencyOptionWrapper d-flex align-items-center justify-content-between",
        );
        opt.attr("id", "optionDiv_" + option.id);
        opt.append(
          `<div>${_mediaPageNames[option.media_page_id]} | ${option.name}</div>`,
        );
        opt.append(
          `<input type="checkbox" ${isChecked} style="margin-right:10px" onclick="toggleDependency(${id},${option.id},${option.media_page_id}, this)"></input>`,
        );
        dependency.append(opt);
      }
    });
  }
  form.append(searchbar);
  form.append(searchforChecked);

  // Append the name input to the form
  form.append(dependency);
  let checkIfLocked = _currentDependencies.length > 0;
  let isLocked = _hotspots.find((obj) => obj.id == id).visibleIfLocked
    ? "checked"
    : "unchecked";
  console.log("checkifLocked" + checkIfLocked);
  form.append(`<div id="lockDependencyDiv" ${checkIfLocked ? "" : "hidden"} style="display: flex, justify_content: center">
             <label for="lockedDependency">{{_("Locked Hotspot Visible")}} </label>
       <input
       id="lockedDependencyCheckbox"
       type="checkbox"
       ${isLocked}
       onclick="toggleVisibleIfLocked(${hot.id}, this.checked)"
     /> </div>`);

  /*   // Create an Selection for the hotspots dependencies
  form.append(`<label for="dependencies">
    {{_("Dependencies")}}
    <i class="bi bi-info-circle"
      data-bs-toggle="tooltip"
      data-bs-placement="top"
      title="{{_("The hotspot is only shown when already interacted with the selected dependencies")}}">
    </i>
    </label>`);
  let modular =
    $(`<modular-behaviour name="bootstrap5-tags" src="{{ url_for('static', filename='../node_modules/bootstrap5-tags/tags.js') }}" type="module" class="text-start" lazy>
       </modular-behaviour>`);
  let dependencies = $(
    "<select class='form-select d-none' id='dependencies' name='dependencies[]' multiple data-allow-clear='1' required></select>",
  );
  let groupedOptions = _hotspots.reduce((acc, option) => {
    if (option.id !== hot.id) {
      const mediaPageName = _mediaPageNames[option.media_page_id] || "Unknown";
      acc[mediaPageName] = acc[mediaPageName] || [];
      acc[mediaPageName].push(
        `<option value="${option.id}" ${_currentDependencies.includes(option.id) ? "selected" : ""}>${option.name} (${_mediaPageNames[option.media_page_id]})</option>`,
      );
    }
    return acc;
  }, {});

  Object.entries(groupedOptions).forEach(([mediaPageName, options]) => {
    dependencies.append(
      `<optgroup label="${mediaPageName}">${options.join("")}</optgroup>`,
    );
  });
  dependencies.change(function (e) {
    if (e.originalEvent) {
      toggleDependency(hot.id, $(this).val());
    }
  });
  modular.append(dependencies);
  form.append(modular); */

  // Set hotspot Icon
  form.append('<label for="propertiesLinkedImage">{{_("Set Icon")}}</label>');

  let hotspotImage = getAllAssetsByType(
    hot.custom_display_icon_id,
    "custom_display_icon_id",
    "Image",
  );
  hotspotImage.change(function () {
    updateHotspot(hot.id, form.serializeArray());
  });

  form.append(hotspotImage);

  // Append the form to the properties element
  container.append(form);
  $('[data-bs-toggle="tooltip"]').tooltip(); // Update tooltips
}

/**
 * is called from displayProperties() to create the dropdown Menus for the Properties
 */
function getAllAssetsByType(selectedID, name, type) {
  // Create a dropdown for media assets
  let mediaDropdown = $("<select></select>")
    .attr("name", name)
    .attr("id", "propertiesLinkedImage")
    .addClass("form-select");
  mediaDropdown.append(
    $("<option></option>")
      .attr("value", -1)
      .attr("id", "EmptyOption")
      .text('{{_("Default")}}'),
  );
  // Iterate through all asset cards
  $(".pic_card").each(function () {
    let mediaId = $(this).data("filename");
    let mediaType = $(this).data("format");
    //console.log(mediaId, mediaType)
    // If the asset is an image
    if (mediaType.startsWith(type)) {
      let mediaName = $(this).find(".pic-card-text").text();

      // Create an option for each image asset with its ID and name
      let option = $("<option></option>")
        .attr("value", mediaId)
        .text(mediaName);

      // Set the current media asset as selected
      if (selectedID === mediaId) {
        option.attr("selected", "selected");
      }

      // Append the option to the media dropdown
      mediaDropdown.append(option);
    }
  });
  return mediaDropdown;
}

/***
 * Update hotspot properties
 */
function updateHotspot(hotspotId, f) {
  const form = $("#hotspot-form");
  const url = "{{ url_for('home.update_hotspot', hotspot_id='') }}" + hotspotId;

  const data = new FormData(form[0]);
  data.append("media_type", _selectedHotspot.getAttribute("data-media-type"));
  $("#hotspot-" + hotspotId)
    .find(".hotspotName")
    .html(data.get("name"));
  $.ajax({
    type: "PUT",
    url: url,
    data: data,
    contentType: false,
    processData: false,
    success: function (response) {
      socket.emit("hotspot", {
        tour: "{{ tour_id }}",
        hotspot_id: hotspotId,
      });
      fetchHotspot(hotspotId, hotspotId);
    },
    error: function (response) {
      console.log(response);
    },
  });
}

/***
 * Remove Hotspot Button Click
 */
$(document).ready(function () {
  registerHotspotRemoveButtons();
});

/**
 * In This function is defined what happens when the X is clicked to remove a Hotspot
 */
function registerHotspotRemoveButtons() {
  $(".btn-removehotspot").click(function () {
    const hotspotId = $(this).data("hotspot-id");
    //delete hotspot in _hotspots
    _hotspots = _hotspots.filter((hotspot) => hotspot.id !== hotspotId);
    // Make an AJAX call to remove the hotspot
    $.ajax({
      url: "{{ url_for('home.remove_hotspot') }}",
      method: "POST",
      data: {
        hotspot_id: hotspotId,
      },
      success: function (response) {
        socket.emit("removeHotspot", {
          tour: "{{ tour_id }}",
          hotspot_id: hotspotId,
        });
      },
      error: function (error) {
        console.error("Error removing hotspot:", error);
      },
    });
  });
}

/**
 * remove hotspot from the scene and mediapage
 */
function removeHotspot(hotspotId) {
  if ($('table[data-hotspot-id="' + hotspotId + '"]').hasClass("selected")) {
    $("#propertiesContainer").html("");
  }
  $('table[data-hotspot-id="' + hotspotId + '"]').remove();
  $('a-plane[data-hotspot-id="' + hotspotId + '"]').remove();

  _hotspots = _hotspots.filter((hotspot) => hotspot.id !== hotspotId);
  $("#hotspot-message-add").toggleClass("d-none", _hotspots.length != 0);
}

function annotationMediaPage(id, name, title) {
  $("#annotation-id").val(id);
  $("#annotation-name").html(name);
  if (title !== "{{_('Notes')}}" && title !== undefined) {
    $("#annotation-text").val(title.replace(/<br>/g, "\n"));
  } else {
    $("#annotation-text").val("");
  }
  $("#annotation-model").on("hidden.bs.modal", function () {
    $("#annotation-submit").off("click");
  });
  $("#annotation-model").modal("show");

  $("#annotation-submit").click(function () {
    $.ajax({
      url: "{{ url_for('home.update_mediapage') }}",
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify({
        mediaId: id,
        tour_id: _tour_id,
        annotation: $("#annotation-text").val(),
      }),
      contentType: "application/json",
      success: function (response) {
        $("#annotation-id").val("");
        $("#annotation-name").html("");
        $("#annotation-submit").off("click");
        $("#annotation-model").modal("hide");
        socket.emit("updateMediaPages", {
          tour: "{{ tour_id }}",
        });
        updateMediaPages();
      },
      error: function (error) {
        console.log("Error updating annotation: " + error);
      },
    });
  });
}

function editMediaName(presented_media_id, name) {
  $("#newFileName").val(name);
  $("#renameModal")
    .on("shown.bs.modal", function () {
      $("#newFileName").focus();
    })
    .modal("show");

  $("#renameModal").submit(function (e) {
    e.preventDefault();
    var newName = $("#newFileName").val();
    if (newName) {
      $.ajax({
        url: "{{ url_for('home.update_mediapage') }}",
        type: "POST",
        data: JSON.stringify({
          mediaId: presented_media_id,
          name: newName,
          tour_id: _tour_id,
        }),
        contentType: "application/json",
        success: function (response) {
          socket.emit("updateMediaPages", {
            tour: "{{ tour_id }}",
          });
          updateMediaPages();
        },
        error: function (error) {
          console.log("Error updating annotation: " + error);
        },
        complete: function () {
          $("#renameModal").modal("hide");
          $("#renameModal").off("submit");
        },
      });
    }
  });
}

function setHome(id, asset_id) {
  $.ajax({
    url: "{{ url_for('home.home_tour') }}",
    type: "PUT",
    data: JSON.stringify({ id: id, asset_id: asset_id }),
    contentType: "application/json",
    success: function (response) {
      location.reload();
    },
    error: function (e) {
      console.error(e);
    },
  });
}

function deleteMedia(mediaId) {
  const modalHtml = `
    <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="deleteModalLabel">{{_("Confirm Delete")}}</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            {{_("Are you sure you want to delete this scene?")}}
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{_("Cancel")}}</button>
            <button type="button" class="btn btn-danger" id="confirmDelete">{{_("Delete")}}</button>
          </div>
        </div>
      </div>
    </div>
  `;
  $("body").append(modalHtml);
  $("#deleteModal").on("shown.bs.modal", function () {
    $("#deleteModal").on("keydown", (e) => {
      if (e.key === "Enter" && !$(e.target).is("input"))
        $("#confirmDelete").click();
    });
  });
  $("#deleteModal").on("hidden.bs.modal", () => {
    $("#deleteModal").off("keydown");
    $("#deleteModal").remove();
  });
  $("#deleteModal").modal("show");
  $("#confirmDelete").on("click", function () {
    $.ajax({
      url: "{{ url_for('home.delete_mediapage') }}",
      type: "POST",
      data: JSON.stringify({ mediapage_id: mediaId }),
      contentType: "application/json",
      success: function (response) {
        socket.emit("updateMediaPages", {
          tour: "{{ tour_id }}",
        });
        updateMediaPages();
        $("#propertiesContainer").html("");
        if (_currentMediapage == mediaId) {
          _currentMediapage = null;
          $("#visible-timeline").remove();
          $("#current-position").remove();
          $("#mute-button").remove();
          $(".hotspot-plane").remove();
          let vSphere = document.querySelector("#videoSphere");
          vSphere.setAttribute("src", "");
          $("#videoSphere").attr("visible", false).get(0).pause();
          $("#my-video").get(0).pause();
          $("#mute-button").remove();
          $("a-sky").attr("color", "#ECECEC").attr("src", "");
        }
      },
      error: function (jqXHR, textStatus, errorThrown) {
        alert("Error deleting media: " + errorThrown);
      },
      complete: function () {
        $("#deleteModal").modal("hide");
        $("#deleteModal").remove();
      },
    });
  });
}

function registerMediaPicClickEvent() {
  const scenePictures = $(".scene_picture");
  scenePictures.off();
  var sky = document.getElementById("sky");
  scenePictures.on("click", function (event) {
    $(".hotspot_entry").removeClass("selected");
    $("#propertiesContainer").html("");
    _currentMediapage = event.target.id.split("-").pop();
    _metadataloaded = false;

    let vSphere = document.querySelector("#videoSphere");
    let videoEl = document.querySelector("#my-video");
    var assetId = $(this).data("media-source");
    $("#sky").remove();
    $("#visible-timeline").remove();
    $("#current-position").remove();
    if ($(this).closest(".mediapage_card").attr("data-media-type") == "Video") {
      videoEl.setAttribute("src", assetId);
      let slider = $('<div id="current-position"></div>');
      videoEl.addEventListener("loadedmetadata", function () {
        let positionEl = document.querySelector("#current-position");
        positionEl.noUiSlider ? positionEl.noUiSlider.destroy() : "";
        let slider = noUiSlider.create(positionEl, {
          start: 0,
          range: {
            min: 0,
            max: videoEl.duration,
          },
        });

        slider.on("set", function (value) {
          if (!_blockVideoUpdate) videoEl.currentTime = Number(value);
        });
      });

      $("#aframe-container").append(slider);
      vSphere.setAttribute("visible", true);
      //linkTimelineWithVideo(positionEl, sky)

      //add mute Button
      videoEl.muted = true;
      let mute =
        '<i class="bi bi-volume-mute-fill" style="font-size: 2rem;"></i>';
      let unmute =
        '<i class="bi bi-volume-up-fill" style="font-size: 2rem;"></i>';
      let muteButton = $(
        '<div id="mute-button" class="btn position-absolute top-0 end-0">' +
          mute +
          "</div>",
      );
      muteButton.on("click", function () {
        videoEl.muted = !videoEl.muted;
        if (videoEl.muted) {
          muteButton.html(mute);
        } else {
          muteButton.html(unmute);
        }
      });
      $("#aframe-container").append(muteButton);
    } else {
      $("#mute-button").remove();
      videoEl.pause();
      vSphere.setAttribute("visible", false);
      vSphere.currentTime = 0;

      var scene = document.querySelector("a-scene");
      sky = document.createElement("a-sky");
      sky.id = "sky";
      sky.setAttribute("src", assetId);
      scene.appendChild(sky);
    }
    fetchHotspots(_tour_id, _currentMediapage);
    $("#media-page-container").children().children().removeClass("border-dark");

    $(this).closest(".card").toggleClass("border-dark");
  });
}

function linkTimelineWithVideo(positionEl, videoEl) {
  AFRAME.registerComponent("video-handler", {
    init: function () {
      // When the video time updates, check if it's within the slider range
      videoEl.addEventListener("timeupdate", function () {
        // Show or hide the plane based on whether the time is within the range
        var planeEl = document.querySelector("a-plane");
        if (videoEl.currentTime >= start && videoEl.currentTime <= end) {
          //planeEl.setAttribute('visible', true);
        } else {
          //planeEl.setAttribute('visible', false);
        }

        // Update the position slider
        positionEl.noUiSlider.set(videoEl.currentTime);
      });

      // When the position slider value changes, seek the video to that time
      positionEl.noUiSlider.on("change", function (value) {
        videoEl.currentTime = Number(value);
      });

      this.el.addEventListener("click", function () {
        if (videoEl.paused) videoEl.play();
        else videoEl.pause();
      });
    },
  });
}

/***
 * Simple AJAX Call to set the Startpage
 */
function setStartpage(mediaId) {
  $.ajax({
    url: "{{ url_for('home.update_mediapage') }}",
    type: "POST",
    contentType: "application/json",
    data: JSON.stringify({
      mediaId: mediaId,
      is_start_page: 1,
      tour_id: _tour_id,
    }),
    success: function (response) {
      socket.emit("updateMediaPages", {
        tour: "{{ tour_id }}",
      });
      updateMediaPages();
    },
    error: function (xhr, ajaxOptions, thrownError) {
      alert("Error while Setting HomeMedia: " + thrownError);
      console.log(thrownError);
    },
  });
}

function waitForVariable(myVariable) {
  if (myVariable) {
  } else {
    setTimeout(waitForVariable, 100); // Wait for 1 second
  }
}

/*
function setOrientation(mediaId){
    let cam = $('#cam');
    let rotation = cam.get(0).getAttribute('rotation');
        $.ajax({
                url: "{{ url_for('home.update_mediapage') }}",
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify({
                    mediaId: mediaId,
                    tour_id: tour_id,
                    orientation: `${rotation.x}, ${rotation.y}, ${rotation.z}`
                }),
                success: function(response) {
                    console.log(response);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error while Setting HomeMedia: " + thrownError)
                    console.log(thrownError);
                }
            });

}*/
