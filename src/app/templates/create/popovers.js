//enable popovers
new bootstrap.Popover($("#tour-tour-collaboratour")[0], {
  html: true,
  content: $('[data-name="tour-collaboratour-popover-content"]'),
});
new bootstrap.Popover($("#tour-annotation")[0], {
  html: true,
  content: $('[data-name="annotation-popover-content"]'),
});
new bootstrap.Popover($("#tour-chat")[0], {
  html: true,
  content: $('[data-name="chat-popover-content"]'),
});

//on annotation-opening
$("#tour-annotation").on("shown.bs.popover", function () {
  $.ajax({
    url: "{{ url_for('home.set_tour_annotation_unread') }}",
    type: "POST",
    contentType: "application/json",
    data: JSON.stringify({
      tour_id: "{{ tour_id }}",
      annotation: $("#annotation").val(),
    }),
    error: function (xhr, status, error) {
      console.error("Failed to set tour annotation unread status");
    },
  });
  $("#annotationNotification").addClass("d-none");
});

//on collaborator-opening
$("#tour-tour-collaboratour").on("shown.bs.popover", function () {
  //scroll to the bottom
  $("#collaborator").scrollTop($("#collaborator")[0].scrollHeight);
});

//on chat-opening
$("#tour-chat").on("shown.bs.popover", function () {
  $.ajax({
    url: "{{ url_for('home.set_chat_unread') }}",
    type: "POST",
    contentType: "application/json",
    data: JSON.stringify({
      tour_id: "{{ tour_id }}",
    }),
    error: function (xhr, status, error) {
      console.error("Failed to set chat unread status");
    },
  });
  //show unread messages
  $("#chat").val($("#chat").val() + unreadChat);
  //scroll chat to the bottom
  $("#chat").scrollTop($("#chat")[0].scrollHeight);
  //reset unread messages and reset counter
  unreadChat = "";
  $("#unreadCount").addClass("d-none");
  $("#unreadCount").html("0");
});

//hide popover when click somewhere
$("body").on("click", function (e) {
  if ($(e.target).parents(".popover").length === 0) {
    $(".popover-toggle").not($(e.target)).popover("hide");
  }
});

//submit chat-input on enter
$("#chat-input").keypress(function (e) {
  const code = e.keyCode || e.which;
  if (code == 13 && !e.shiftKey) {
    e.preventDefault();
    msg = $("#chat-input").val();
    $("#chat-input").val("");
    socket.emit("text", {
      tour_id: "{{ tour_id }}",
      user_id: "{{ current_user.id }}",
      msg: msg,
    });
  }
});

// Update tour annotation on change
$("#annotation").on("change", function () {
  annotation = this.value;
  $.ajax({
    url: "{{ url_for('home.update_tour_annotation') }}",
    type: "PUT",
    contentType: "application/json",
    data: JSON.stringify({
      annotation: annotation,
      tour_id: "{{ tour_id }}",
    }),
    success: function (response) {
      socket.emit("updateTourAnnotation", {
        tour_id: "{{ tour_id }}",
        annotation: annotation,
      });
    },
    error: function (xhr, status, error) {
      console.error("Failed to update annotation");
    },
  });
});

function updateTourAnnotation(annotation) {
  $("#annotation").val(annotation);
  if ($(".annotation-popover").length <= 0) {
    //annotation is currently closed
    $("#annotationNotification").toggleClass("d-none", annotation == "");
  }
}
