//This file is for Handling all Assets in the left column
$(document).ready(function () {
  registerAssetAddButtonClick();

  $("#upload-files, #media-pages, #aframe-scene").on(
    "dragend dragover dragenter dragleave drop",
    function (e) {
      e.preventDefault();
      e.stopPropagation();
    },
  );

  $("#upload-files").on("dragover", function (e) {
    if (window.draggedElement == null) {
      $("#dropzone-overlay").removeClass("invisible");
    }
  });

  $("#upload-files").on("dragleave", function (e) {
    elementMouseIsOver = document.elementFromPoint(e.clientX, e.clientY);
    if (!$(elementMouseIsOver).closest("#upload-files").length) {
      $("#dropzone-overlay").addClass("invisible");
    }
  });

  $("#upload-files").on("dragend", function (e) {
    window.draggedElement = null;
    $("#dropzone-overlay").addClass("invisible");
  });

  $("#upload-files").on("drop", function (e) {
    if (window.draggedElement == null) {
      $("#dropzone-overlay").addClass("invisible");
      uploadDroppedFiles(e, "asset");
    }
  });

  $("#aframe-scene").on("dragover", function (e) {
    if (
      window.draggedElement == null &&
      $("#start-message-scene").hasClass("d-none") &&
      $("#start-message-upload").hasClass("d-none")
    ) {
      $("#dropzone-overlay2")
        .children()
        .html("{{_('Drop here to upload and add as hotspot')}}");
      $("#dropzone-overlay2").removeClass("invisible");
    } else if (
      !window.draggedElement.className.includes("hotspot_card") &&
      $("#start-message-upload").hasClass("d-none") &&
      $("#start-message-scene").hasClass("d-none")
    ) {
      //maybe also hotspot_cards?
      $("#dropzone-overlay2")
        .children()
        .html("{{_('Drop here to add as hotspot')}}");
      $("#dropzone-overlay2").removeClass("invisible");
    }
  });

  $("#aframe-scene").on("dragend dragleave drop", function (e) {
    $("#dropzone-overlay2").addClass("invisible");
  });

  $("#media-pages").on("dragover", function (e) {
    if (window.draggedElement == null) {
      $("#dropzone-overlay3")
        .children()
        .html("{{_('Drop here to upload and add as new scene')}}");
      $("#dropzone-overlay3").removeClass("invisible");
    } else if (!window.draggedElement.className.includes("hotspot_card")) {
      $("#dropzone-overlay3")
        .children()
        .html("{{_('Drop here to add as new scene')}}");
      $("#dropzone-overlay3").removeClass("invisible");
    }
  });

  $("#media-pages").on("dragend dragleave", function (e) {
    elementMouseIsOver = document.elementFromPoint(e.clientX, e.clientY);
    if (!$(elementMouseIsOver).closest("#media-pages").length) {
      $("#dropzone-overlay3").addClass("invisible");
    }
  });

  $("#media-pages").on("drop", function (e) {
    $("#dropzone-overlay3").addClass("invisible");
    if (window.draggedElement == null) {
      uploadDroppedFiles(e, "mediapage");
    } else {
      var filename = e.originalEvent.dataTransfer.getData("text/plain");
      var name = document
        .getElementById(filename)
        .querySelector(".card-text").innerText;

      addDroppedMediaPage(filename, name);
    }
  });

  //AJAX call to Upload files.
  $("#upload").on("change", function () {
    var filesCount = this.files.length;
    // Show the spinner
    $("#spinner").show();
    Array.prototype.forEach.call(this.files, (file, index) => {
      var formData = new FormData();
      formData.append("image", file);
      formData.append("tour_id", $("#tour_id").val());

      $.ajax({
        url: "{{ url_for('home.upload_route') }}",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function (response) {
          filesCount--;
          filesCount <= 0 ? $("#spinner").hide() : "";
          $("#alerts").prepend(
            '<div id="alert' +
              index +
              '" class="alert alert-success" role="alert">' +
              file.name +
              " {{_('successfully uploaded!')}}</div>",
          );

          socket.emit("updateFileList", {
            tour: "{{ tour_id }}",
          });

          // Hide the alert after two seconds
          setTimeout(function () {
            $("#alert" + index).hide();
          }, 2000);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          filesCount--;
          filesCount <= 0 ? $("#spinner").hide() : "";
          $("#alerts").prepend(
            '<div id="alert' +
              index +
              '" class="alert alert-danger" role="alert">Error uploading: ' +
              errorThrown +
              "</div>",
          );

          socket.emit("updateFileList", {
            tour: "{{ tour_id }}",
          });

          // Hide the alert after two seconds
          setTimeout(function () {
            $("#alert" + index).hide();
          }, 2000);
        },
      });
    });
    // Reset the form
    $("#upload").val("");
  });
});

function uploadDroppedFiles(e, type) {
  $("#spinner").show();
  var files = e.originalEvent
    ? e.originalEvent.dataTransfer.files
    : e.dataTransfer.files;
  var filesCount = files.length;
  Array.prototype.forEach.call(files, (file, index) => {
    var formData = new FormData();
    formData.append("image", file);
    formData.append("tour_id", $("#tour_id").val());
    $.ajax({
      url: "{{ url_for('home.upload_route') }}",
      type: "POST",
      data: formData,
      processData: false,
      contentType: false,
      success: function (response) {
        filesCount--;
        filesCount <= 0 ? $("#spinner").hide() : "";
        $("#alerts").prepend(
          '<div id="alert' +
            index +
            '" class="alert alert-success" role="alert">' +
            file.name +
            " {{_('successfully uploaded!')}}</div>",
        );
        if (type == "mediapage") {
          updateFileList();
          addDroppedMediaPage(response.filename, response.name);
        } else if (
          type == "hotspot" &&
          typeof _currentMediapage !== "undefined" &&
          index < 1
        ) {
          let params = calcHotspotPosition(e);
          params.media_type_id = response.media_type;
          params.id = response.filename;
          params.name = response.name;

          saveHotspotToDatabase(params).done(function () {
            socket.emit("updateMediaPages", {
              tour: "{{ tour_id }}",
            });
            updateMediaPages(params.id);
          });
        } else {
          socket.emit("updateFileList", {
            tour: "{{ tour_id }}",
          });
        }

        // Hide the alert after two seconds
        setTimeout(function () {
          $("#alert" + index).hide();
        }, 2000);
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.error("Error uploading files: " + errorThrown);
      },
    });
  });
}

function addDroppedMediaPage(filename, name) {
  var formData = new FormData();
  formData.append("tour_id", _tour_id);
  formData.append("asset_id", filename);
  formData.append("name", name);

  $.ajax({
    url: "{{ url_for('home.create_mediapage') }}",
    method: "POST",
    data: formData,
    processData: false,
    contentType: false,
    success: function (response) {
      if (response.error) {
        console.error(response.error);
      } else {
        socket.emit("updateMediaPages", {
          tour: "{{ tour_id }}",
        });
        updateMediaPages(null, response.mediapage_id);
      }
      if (response.equirectangular === false) {
        alert(
          "This image may not be a 360° equirectangular projection, potentially resulting in a distorted and unnatural appearance.",
        );
      }
    },
    error: function (err) {
      console.error("Error:", err);
    },
  });
}

function registerAssetAddButtonClick() {
  //Very important to set the click event off, to avoid the button gets over the time multiple Click events
  $(".overlay-asset-button").off();
  $(".overlay-asset-button").on("click", function () {
    // Get the 'p' element with the class 'pic-card-text' within the current card
    var pElement = $(this).closest(".card").find(".pic-card-text");

    // Get the filename from the 'p' element's id attribute
    var filename = pElement.attr("id").replace("card-text-", "");

    // Get the name from the 'p' element's text content
    var name = pElement.text();

    var formData = new FormData();
    formData.append("tour_id", _tour_id);
    formData.append("asset_id", filename);
    formData.append("name", name);

    $.ajax({
      url: "{{ url_for('home.create_mediapage') }}",
      method: "POST",
      data: formData,
      processData: false,
      contentType: false,
      success: function (response) {
        if (response.error) {
          console.error(response.error);
        } else {
          socket.emit("updateMediaPages", {
            tour: "{{ tour_id }}",
          });
          updateMediaPages(null, response.mediapage_id);
        }
        if (response.equirectangular === false) {
          alert(
            "This image may not be a 360° equirectangular projection, potentially resulting in a distorted and unnatural appearance.",
          );
        }
      },
      error: function (err) {
        console.error("Error:", err);
      },
    });
  });
}

function updateFileList() {
  //After an Upload this method is called to display all Assets belonging to the Tour. It gets the data through a simple AJAX call
  $.ajax({
    url: "{{ url_for('home.get_uploaded_files') }}",
    type: "GET",
    data: {
      tour_id: _tour_id,
    },
    success: function (response) {
      response.forEach(function (file) {
        var card = document.querySelector(
          `.pic_card[data-filename="${file.filename}"]`,
        );
        //If card null then this card don't exist so we have to create it
        if (card == null) {
          createCard(file);
          var card = document.querySelector(
            `.pic_card[data-filename="${file.filename}"]`,
          );
          card.querySelector(".pic-card-text").innerHTML = file.equirectangular
            ? "<i class='bi bi-globe'></i>" + file.name
            : file.name;
        } else {
          // Update card data
          card.querySelector(".pic-card-text").innerHTML = file.equirectangular
            ? "<i class='bi bi-globe'></i>" + file.name
            : file.name;
          card
            .querySelector(".annotation")
            .setAttribute(
              "title",
              file.annotation
                ? file.annotation.replace(/\n/g, "<br>")
                : "{{_('Notes')}}",
            );
          new bootstrap.Tooltip(card.querySelector(".annotation")).update();
        }
        registerAssetAddButtonClick();
      });
      socket.emit("updateMediaPages", {
        tour: "{{ tour_id }}",
      });
      updateMediaPages();
      addDragAndDropFunctionality();
      $('[data-bs-toggle="tooltip"]').tooltip();
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log("Error fetching file list: " + errorThrown);
    },
  });
}

function editFileName(filename_id, name) {
  $("#newFileName").val(name);
  $("#renameModal")
    .on("shown.bs.modal", function () {
      $("#newFileName").focus();
    })
    .modal("show");

  $("#renameModal").submit(function (e) {
    e.preventDefault();
    var newName = $("#newFileName").val();
    if (newName) {
      $.ajax({
        url: "{{ url_for('home.edit_asset_name') }}",
        type: "POST",
        data: JSON.stringify({
          filename_id: filename_id,
          name: newName,
          tour_id: _tour_id,
        }),
        contentType: "application/json",
        success: function (response) {
          socket.emit("updateFileList", {
            tour: "{{ tour_id }}",
          });
        },
        error: function (error) {
          console.log(error);
        },
        complete: function () {
          $("#renameModal").modal("hide");
          $("#renameModal").off("submit");
        },
      });
    }
  });
}

function deleteFile(filename, cardElement) {
  //After Confirm the Asset and all Mediapages who depends on it will be deleted
  const modalHtml = `
    <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="deleteModalLabel">{{_("Confirm Delete")}}</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            {{_("Are you sure you want to delete this file?")}}
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{_("Cancel")}}</button>
            <button type="button" class="btn btn-danger" id="confirmDelete">{{_("Delete")}}</button>
          </div>
        </div>
      </div>
    </div>
  `;
  $("body").append(modalHtml);
  $("#deleteModal").on("shown.bs.modal", function () {
    $("#deleteModal").on("keydown", (e) => {
      if (e.key === "Enter" && !$(e.target).is("input"))
        $("#confirmDelete").click();
    });
  });
  $("#deleteModal").on("hidden.bs.modal", () => {
    $("#deleteModal").off("keydown");
    $("#deleteModal").remove();
  });
  $("#deleteModal").modal("show");
  $("#confirmDelete").on("click", function () {
    $.ajax({
      url: "{{ url_for('home.delete_file') }}",
      type: "POST",
      data: JSON.stringify({ tour_id: _tour_id, filename: filename }),
      contentType: "application/json",
      success: function (response) {
        cardElement.remove();
        if ($(".pic_card").length === 0 && $(".mediapage_card").length === 0) {
          $("#start-message-scene").addClass("d-none");
          $("#start-message-upload").removeClass("d-none");
        }
        socket.emit("removeAsset", {
          tour: "{{ tour_id }}",
          id: filename,
        });
      },
      error: function (jqXHR, textStatus, errorThrown) {
        alert("Error deleting file: " + errorThrown);
      },
      complete: function () {
        $("#deleteModal").modal("hide");
        $("#deleteModal").remove();
      },
    });
  });
}

function removeAsset(id) {
  var card = document.querySelector(`.pic_card[data-filename="${id}"]`);
  if (card) {
    card.remove();
  }
}

function downloadAsset(filename, cardElement) {
  const asset = document.createElement("a");
  asset.setAttribute("href", `../../uploaded_files/${filename}`);
  asset.setAttribute("download", `${filename}`);
  asset.click();
}

function annotationAsset(id, name, title) {
  $("#annotation-id").val(id);
  $("#annotation-name").html(name);
  if (title !== "{{_('Notes')}}" && title !== undefined) {
    $("#annotation-text").val(title.replace(/<br>/g, "\n"));
  } else {
    $("#annotation-text").val("");
  }
  $("#annotation-model").on("hidden.bs.modal", function () {
    $("#annotation-submit").off("click");
  });
  $("#annotation-model").modal("show");

  $("#annotation-submit").click(function () {
    $.ajax({
      url: "{{ url_for('home.update_asset_annotation') }}",
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify({
        asset_id: id,
        tour_id: _tour_id,
        annotation: $("#annotation-text").val(),
      }),
      contentType: "application/json",
      success: function (response) {
        $("#annotation-id").val("");
        $("#annotation-name").html("");
        $("#annotation-submit").off("click");
        $("#annotation-model").modal("hide");
        socket.emit("updateFileList", {
          tour: "{{ tour_id }}",
        });
      },
      error: function (error) {
        console.log("Error updating annotation: " + error);
      },
    });
  });
}
