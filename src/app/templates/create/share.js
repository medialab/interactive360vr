var changedUserRights = false;

$(".editCollaboratorRights, .addCollaboratorRights").click(function () {
  $(this).val($(this).val() == "1" ? "0" : "1");
  bootstrap.Tooltip.getInstance($(this)).setContent({
    ".tooltip-inner":
      $(this).val() == "1"
        ? "{{_('change to<br />view permission')}}"
        : "{{_('change to<br />edit permission')}}",
  });
  $(this).find(".editCollaboratorEdit").toggleClass("d-none");
  $(this).find(".editCollaboratorView").toggleClass("d-none");
  $(this).trigger("mouseenter");
  changedUserRights = true;
});

$(".editCollaboratorRights:not(.addCollaboratorRights)").click(function () {
  let sharedTour_id = $(this).attr("id").split("-")[1];
  let edit = $(this).val();

  $("#confirmCollaborator_modal .modal-title").text(
    '{{_("Collaborator permission")}}',
  );
  $("#confirmCollaborator_modal .modal-body").html(
    '{{_("Change permission to")}} <strong>' +
      (edit == "1" ? '{{_("edit")}}' : '{{_("view")}}') +
      "</strong>?",
  );
  $("#confirmCollaboratorBtn").removeClass("btn-danger");
  $("#confirmCollaborator_modal").modal("show");

  $("#confirmCollaborator_modal").submit(function (e) {
    $.ajax({
      url:
        '{{ url_for("home.edit_collaborator_rights", sharedTour_id="") }}' +
        sharedTour_id,
      type: "PUT",
      data: { edit: edit },
      success: function (result) {
        socket.emit("editCollaboratorRights", {
          tour_id: result["tour_id"],
        });
      },
      error: function (error) {
        console.log(error);
      },
      complete: function () {
        $("#confirmCollaborator_modal").modal("hide");
        $("#confirmCollaborator_modal").off("submit");
      },
    });
  });
});

$(".editCollaboratorRights").on("mouseenter mouseleave", function () {
  if (!changedUserRights) {
    $(this).find(".editCollaboratorEdit").toggleClass("d-none");
    $(this).find(".editCollaboratorView").toggleClass("d-none");
  } else {
    changedUserRights = false;
  }
});

$(".addCollaborator").click(function () {
  let tour_id = $(this).attr("id").split("-")[1];
  let collaborator = $("#addCollaboratorInput-" + tour_id).val();
  let edit = $("#addCollaboratorRights-" + tour_id).val();

  $("#confirmCollaborator_modal .modal-title").text(
    "{{_('Add Collaborator')}}",
  );
  $("#confirmCollaborator_modal .modal-body").html(
    "{{_('Add ')}}<b>" +
      collaborator +
      "</b> {{_('with')}} " +
      (edit == "1"
        ? "{{_('permission to<br /><b>edit</b>')}}"
        : "{{_('permission to<br />(only) <b>view</b>')}}") +
      " {{_('the tour?')}}",
  );
  $("#confirmCollaboratorBtn").removeClass("btn-danger");
  $("#confirmCollaborator_modal").modal("show");

  $("#confirmCollaborator_modal").submit(function (e) {
    e.preventDefault();
    $.ajax({
      url: '{{ url_for("home.add_collaborator", tour_id="") }}' + tour_id,
      type: "POST",
      data: { collaborator: collaborator, edit: edit },
      success: function (result) {
        socket.emit("addCollaborator", {
          tour_id: tour_id,
          collaborator_id: result.collaborator_id,
        });
        location.reload();
      },
      error: function (error) {
        if (error.status === 400) {
          addAlert("danger", "{{_('User already added as collaborator.')}}");
        } else if (error.status === 404) {
          addAlert("danger", "{{_('User not found.')}}");
        } else {
          addAlert("danger", "{{_('An unexpected error occurred.')}}");
        }
        console.log(error);
      },
      complete: function () {
        $("#confirmCollaborator_modal").modal("hide");
        $("#confirmCollaborator_modal").off("submit");
      },
    });
  });
});

$(".removeCollaborator").click(function () {
  let sharedTour_id = $(this).attr("id").split("-")[1];
  let collaborator = $(this).parent().siblings(".username").text();

  $("#confirmCollaborator_modal .modal-title").text(
    "{{_('Remove Collaborator')}}",
  );
  $("#confirmCollaborator_modal .modal-body").html(
    "{{_('remove ')}}<strong>" +
      collaborator +
      "</strong> {{_('from this project?')}}",
  );
  $("#confirmCollaboratorBtn").addClass("btn-danger");
  $("#confirmCollaborator_modal").modal("show");

  $("#confirmCollaborator_modal").submit(function (e) {
    e.preventDefault();
    $.ajax({
      url:
        '{{ url_for("home.remove_collaborator", sharedTour_id="") }}' +
        sharedTour_id,
      type: "DELETE",
      success: function (result) {
        socket.emit("removeCollaborator", {
          tour_id: result["tour_id"],
        });
        location.reload();
      },
      error: function (error) {
        console.log(error);
      },
      complete: function () {
        $("#confirmCollaborator_modal").modal("hide");
        $("#confirmCollaborator_modal").off("submit");
      },
    });
  });
});

var changedGroupRights = false;

$(".editGroupRights, .addGroupRights").click(function () {
  $(this).val($(this).val() == "1" ? "0" : "1");
  bootstrap.Tooltip.getInstance($(this)).setContent({
    ".tooltip-inner":
      $(this).val() == "1"
        ? "{{_('change to<br />view permission')}}"
        : "{{_('change to<br />edit permission')}}",
  });
  $(this).find(".editGroupEdit").toggleClass("d-none");
  $(this).find(".editGroupView").toggleClass("d-none");
  $(this).trigger("mouseenter");
  changedGroupRights = true;
});

$(".editGroupRights:not(.addGroupRights)").click(function () {
  let sharedGroupTour_id = $(this).attr("id").split("-")[1];
  let edit = $(this).val();

  $("#confirmCollaborator_modal .modal-title").text(
    "{{_('Collaborator permission')}}",
  );
  $("#confirmCollaborator_modal .modal-body").html(
    "{{_('Change permission to')}} <strong>" +
      (edit == "1" ? "{{_('edit')}}" : "{{_('view')}}") +
      "</strong>?",
  );
  $("#confirmCollaboratorBtn").removeClass("btn-danger");
  $("#confirmCollaborator_modal").modal("show");

  $("#confirmCollaborator_modal").submit(function (e) {
    e.preventDefault();
    $.ajax({
      url:
        '{{ url_for("home.edit_group_rights", sharedGroupTour_id="") }}' +
        sharedGroupTour_id,
      type: "PUT",
      data: { edit: edit },
      success: function (result) {
        console.log(result);
        socket.emit("editGroupRights", {
          tour_id: result["tour_id"],
        });
      },
      error: function (error) {
        console.log(error);
      },
      complete: function () {
        $("#confirmCollaborator_modal").modal("hide");
        $("#confirmCollaborator_modal").off("submit");
      },
    });
  });
});

$(".editGroupRights").on("mouseenter mouseleave", function () {
  if (!changedGroupRights) {
    $(this).find(".editGroupEdit").toggleClass("d-none");
    $(this).find(".editGroupView").toggleClass("d-none");
  } else {
    changedGroupRights = false;
  }
});

$(".addCollaboratorInput").on("input", function () {
  let tour_id = $(this).attr("id").split("-")[1];
  let input = $(this).val();
  if (input.length > 0) {
    $("#addCollaborator-" + tour_id).removeClass("disabled");

    bootstrap.Tooltip.getInstance($("#addCollaborator-" + tour_id)).setContent({
      ".tooltip-inner": "{{ _('add group') }}",
    });

    $(this)
      .off("keydown")
      .on("keydown", function (e) {
        if (e.key === "Enter") {
          $(this).trigger("blur");
          $("#addCollaborator-" + tour_id).click();
        }
      });
  } else {
    $("#addCollaborator-" + tour_id).addClass("disabled");
    bootstrap.Tooltip.getInstance($("#addCollaborator-" + tour_id)).setContent({
      ".tooltip-inner": "{{ _('enter a username first') }}",
    });
  }
});

$(".addGroupInput").on("input", function () {
  let tour_id = $(this).attr("id").split("-")[1];
  let input = $(this).val();
  let options = $(this).siblings("datalist").children("option");
  for (let i = 0; i < options.length; i++) {
    if (options[i].value == input) {
      $("#addGroup-" + tour_id).removeClass("disabled");
      bootstrap.Tooltip.getInstance($("#addGroup-" + tour_id)).setContent({
        ".tooltip-inner": "{{ _('add group') }}",
      });
      $(this)
        .off("keydown")
        .on("keydown", function (e) {
          if (e.key === "Enter") {
            $(this).trigger("blur");
            $("#addGroup-" + tour_id).click();
          }
        });
      return;
    }
  }
  $("#addGroup-" + tour_id).addClass("disabled");
  bootstrap.Tooltip.getInstance($("#addGroup-" + tour_id)).setContent({
    ".tooltip-inner": "{{ _('enter one of your groups first') }}",
  });
});

$(".addGroup").click(function () {
  let tour_id = $(this).attr("id").split("-")[1];
  let group = $("#addGroupInput-" + tour_id).val();
  let edit = $("#addGroupRights-" + tour_id).val();

  $("#confirmCollaborator_modal .modal-title").text("{{_('Add Group')}}");
  $("#confirmCollaborator_modal .modal-body").html(
    "{{_('Add ')}}<b>" +
      group +
      "</b> {{_('with')}} " +
      (edit == "1"
        ? "{{_('permission to<br /><b>edit</b>')}}"
        : "{{_('permission to<br />(only) <b>view</b>')}}") +
      " {{_('the tour?')}}",
  );
  $("#confirmCollaboratorBtn").removeClass("btn-danger");
  $("#confirmCollaborator_modal").modal("show");

  $("#confirmCollaborator_modal").submit(function (e) {
    e.preventDefault();
    $.ajax({
      url: '{{ url_for("home.add_group", tour_id="") }}' + tour_id,
      type: "POST",
      data: { group: group, edit: edit },
      success: function (result) {
        socket.emit("addGroup", {
          tour_id: tour_id,
          group_id: result["group_id"],
        });
        location.reload();
      },
      error: function (error) {
        addAlert(
          "danger",
          "{{_('Group not found or already added as collaborator.')}}",
        );
        console.log(error);
      },
      complete: function () {
        $("#confirmCollaborator_modal").modal("hide");
        $("#confirmCollaborator_modal").off("submit");
      },
    });
  });
});

$(".removeGroup").click(function () {
  let sharedGroupTour_id = $(this).data("id");
  let collaborator = $(this).data("username");

  $("#confirmCollaborator_modal .modal-title").text(
    "{{_('Remove Collaborator')}}",
  );
  $("#confirmCollaborator_modal .modal-body").html(
    "{{_('remove ')}}<strong>" +
      collaborator +
      "</strong> {{_('from this project?')}}",
  );
  $("#confirmCollaboratorBtn").addClass("btn-danger");
  $("#confirmCollaborator_modal").modal("show");

  $("#confirmCollaborator_modal").submit(function (e) {
    e.preventDefault();
    $.ajax({
      url:
        '{{ url_for("home.remove_group", sharedGroupTour_id="") }}' +
        sharedGroupTour_id,
      type: "DELETE",
      success: function (result) {
        socket.emit("removeGroup", {
          tour_id: result["tour_id"],
          group_id: result["group_id"],
        });
        location.reload();
      },
      error: function (error) {
        console.log(error);
      },
      complete: function () {
        $("#confirmCollaborator_modal").modal("hide");
        $("#confirmCollaborator_modal").off("submit");
      },
    });
  });
});

$(".shareBtn").click(function () {
  var inputObj = $("<input>").val($(this).val()).appendTo("body").select();
  inputObj.remove();

  var clipboardText = inputObj.val();
  $("#qrcodeContainer").empty();
  var qrcode = new QRCode(document.getElementById("qrcodeContainer"), {
    text: clipboardText,
    width: 190,
    height: 190,
    colorDark: "#000000",
    colorLight: "#ffffff",
    correctLevel: QRCode.CorrectLevel.L,
  });
  $("#shareLink").text(clipboardText);
  $("#shareLink").click(function () {
    window.open(clipboardText, "_blank");
  });
  $("#shareLinkCopy").click(function () {
    navigator.clipboard.writeText(clipboardText);
    $("#shareLinkCopy").html('<i class="bi bi-clipboard-check"></i>');
    $("#shareLinkCopy").tooltip("hide");
    $("#shareLinkCopy").attr("data-bs-original-title", "{{_('Copied!')}}");
    $("#shareLinkCopy").tooltip("show");
  });
  $("#qrcodeModal").modal("show");
  $("#qrcodeModal").on("hidden.bs.modal", function () {
    $("#shareLinkCopy").html('<i class="bi bi-clipboard"></i>');
    $("#shareLinkCopy").attr(
      "data-bs-original-title",
      "{{ _('Copy to clipboard') }}",
    );
  });
});

function updateOnlineUsers(tour_id, users, usersView) {
  $('.editing[data-tour-id="' + tour_id + '"]').each(function () {
    $(this).toggleClass(
      "invisible",
      !users.some((u) => u.username === $(this).data("username")),
    );
  });

  $('.viewing[data-tour-id="' + tour_id + '"]').each(function () {
    $(this).toggleClass(
      "d-none",
      !usersView.some((u) => u.username === $(this).data("username")),
    );
  });
  let uniqueUsers = new Set(users.map((user) => user.username));
  let counter = uniqueUsers.size;
  $("#onlineCount").text(counter);
  $("#onlineCount").toggleClass("d-none", counter < 2);

  let uniqueUsersView = new Set(
    usersView
      .filter((user) => user.username !== "")
      .map((user) => user.username),
  );
  let counterViewUser = uniqueUsersView.size;
  let counterViewGuest = usersView.filter((user) => user.username == "").length;
  let counterView = counterViewUser + counterViewGuest;
  $("#counterViewGuest-" + tour_id).text(
    counterViewGuest +
      " " +
      (counterViewGuest == 1 ? "{{_('Guest')}}" : "{{_('Guests')}}"),
  );
  $("#guests-" + tour_id).toggleClass("d-none", counterViewGuest == 0);
  $("#onlineCountView").text(counterView);
  $("#onlineCountViewPopover").text(counterView);
  $("#onlineCountView").toggleClass("d-none", counterView < 1);
}
