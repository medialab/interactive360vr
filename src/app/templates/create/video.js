/**
 * The Video Handler Handles the 360° Video
 */
AFRAME.registerComponent("video-handler", {
  init: function () {
    let vSphere = document.querySelector("#videoSphere");
    let videoEl = document.querySelector("#my-video");
    let startX, startY;
    var threshold = 5;
    var timeUpdateUnlocked = false;
    let block = false;

    this.el.addEventListener("mousedown", function (event) {
      startX = event.detail.mouseEvent.clientX;
      startY = event.detail.mouseEvent.clientY;
    });
    videoEl.addEventListener("loadedmetadata", function () {
      _metadataloaded = true;
    });
    // When the video time updates, update the range slider and position slider (if it exists)
    videoEl.addEventListener("timeupdate", function () {
      var positionEl = document.querySelector("#current-position");
      if (positionEl && positionEl.noUiSlider) {
        _blockVideoUpdate = true;
        positionEl.noUiSlider.set(videoEl.currentTime);
        _blockVideoUpdate = false;

        const hots = _hotspots.filter(
          (obj) => obj.media_page_id == _currentMediapage,
        );
        hots.forEach(function (hot) {
          const selector = `.hotspot-plane[data-hotspot-id="${hot.id}"]`;
          var plane = document.querySelector(selector);

          let start = hot.starttime;
          let end = hot.endtime;

          if (videoEl.currentTime > start) {
            plane.setAttribute("visible", true);
          } else if (videoEl.currentTime < end) {
            plane.setAttribute("visible", false);
          }
        });
      }
    });

    // When the range slider values change, seek the video if necessary
    // ...
    //Add Play Pause functionality
    this.el.addEventListener("click", function () {
      var endX = event.detail.mouseEvent.clientX;
      var endY = event.detail.mouseEvent.clientY;

      if (
        Math.abs(startX - endX) < threshold &&
        Math.abs(startY - endY) < threshold
      ) {
        // it's a click
        if (videoEl.paused) {
          videoEl.play();
        } else {
          videoEl.pause();
        }
      }
    });

    // Listen for the currentPositionSliderAdded event
    document.body.addEventListener("currentPositionSliderAdded", function () {
      var positionEl = document.querySelector("#current-position");

      // When the position slider value changes, seek the video to that time
      // positionEl.noUiSlider.on('change', function (value) {
      //         //videoEl.currentTime = Number(value);
      //
      //
      // });
    });

    //vSphere.setAttribute('visible', false);
  },
});
