$("meta[name='viewport']").remove();
$(document).ready(function () {
  updateMediaPages();

  //fix the height of the page on resize
  $(window)
    .resize(function () {
      const footerHeight = $("footer").outerHeight() + $("nav").outerHeight();
      $(".page-wrapper").css("height", `calc(100% - ${footerHeight}px)`);
      $("header, footer").css(
        "width",
        `calc(${$("#create-row")[0].scrollWidth + $("#create-row")[0].getBoundingClientRect().left * 2}px)`,
      );
    })
    .resize();

  $(window).on("load", function () {
    // add tooltips
    $('[data-bs-toggle="tooltip"]').tooltip();
  });

  addDragAndDropFunctionality();
  addScrollWheelZoom();
});

//Main File for Handling most of the A-Frame Stuff
const scene = document.querySelector("a-scene");
const camera = document.querySelector("[camera]");

//important Function which Place a plane in the Scene
function calcHotspotPosition(e) {
  params = {
    position: null,
    rotation: null,
  };
  // Set up the raycaster
  const raycaster = new THREE.Raycaster();
  const mouse = new THREE.Vector2();
  const radius = 20;

  // Get the canvas and camera from the A-Frame scene
  const aframeScene = document.querySelector("a-scene");
  const camera = aframeScene.camera;
  const canvas = aframeScene.renderer.domElement;
  const canvasBounds = canvas.getBoundingClientRect();

  // Calculate normalized device coordinates (NDC) for the mouse position
  mouse.x = ((e.clientX - canvasBounds.left) / canvasBounds.width) * 2 - 1;
  mouse.y = -((e.clientY - canvasBounds.top) / canvasBounds.height) * 2 + 1;

  // Update the raycaster based on the camera and mouse position
  raycaster.setFromCamera(mouse, camera);

  // Find the intersection point with a sphere
  const sphere = new THREE.Sphere(new THREE.Vector3(0, 0, 0), radius);
  const intersectionPoint = new THREE.Vector3();
  if (raycaster.ray.intersectSphere(sphere, intersectionPoint)) {
    params.position = `${intersectionPoint.x} ${intersectionPoint.y} ${intersectionPoint.z}`;

    // Set the rotation of the plane to face the origin (0, 0, 0)
    const planeObject3D = new THREE.Object3D();
    planeObject3D.position.copy(intersectionPoint);
    planeObject3D.lookAt(new THREE.Vector3(0, 0, 0));
    const planeRotation = planeObject3D.rotation;
    const aframeRotation = eulerToAframe(planeRotation);
    params.rotation = `${aframeRotation.x} ${aframeRotation.y} ${aframeRotation.z}`;
  }
  return params;
}

function handleDragStart(e) {
  window.draggedElement = e.target;
  e.dataTransfer.setData("text/plain", e.target.id);
  e.dataTransfer.setData("datatype", e.target.getAttribute("data-type"));

  if (window.draggedElement.className.includes("pic_card")) {
    var dragImg = e.target.querySelector(".card-img-left").cloneNode(true); //need a clone in invible are to remove green overlay
    dragImg.style.position = "absolute";
    dragImg.style.top = "-1000px";
    dragImg.width = dragImg.height =
      e.target.querySelector(".card-img-left").width;
    document.body.appendChild(dragImg);
    e.dataTransfer.setDragImage(dragImg, 32, 32);
  }
}

function handleDrop(e) {
  //This function is called when a user Drops a New Hotspot.
  //From this point, the hotspot is registered, displayed in A-Frame and then the media pages are updated
  e.preventDefault();
  if (_currentMediapage == null) {
    $("#alerts").prepend(
      '<div id="alert-mediapage" class="alert alert-danger" role="alert">{{_("Please select a media page first.")}}</div>',
    );
    setTimeout(function () {
      $("#alert-mediapage").remove();
    }, 2000);
    return;
  }

  let id = e.dataTransfer.getData("text/plain");
  let card = document.getElementById(id);

  if (!card) {
    //If the card is not found, it is a (new) file
    uploadDroppedFiles(e, "hotspot");
  } else {
    //If the card is found, add it as hotspot
    let img = window.draggedElement.className.includes("hotspot_card")
      ? card.querySelector(".card-img-left")
      : card.querySelector(".hotspot-media-type");
    let imgSrc = img.getAttribute("src");
    let datatype = e.dataTransfer.getData("datatype");
    e.imgSrc = imgSrc;

    let params = calcHotspotPosition(e);
    params.media_type_id = datatype;
    params.id = id;
    params.name = card.querySelector(".card-text")
      ? card.querySelector(".card-text").innerText
      : card.querySelector(".card-content p").innerText;

    saveHotspotToDatabase(params).done(function () {
      socket.emit("updateMediaPages", {
        tour: "{{ tour_id }}",
      });
      updateMediaPages(params.id);
    });
  }
}

function handleDragOver(e) {
  e.preventDefault();
}

function addDragAndDropFunctionality() {
  // Add dragstart event listener to all hotspot cards
  const hotspotCards = document.querySelectorAll(".hotspot_card");
  hotspotCards.forEach((card) => {
    card.addEventListener("dragstart", handleDragStart);
  });

  // Add dragstart event listener to all pic cards
  const picCards = document.querySelectorAll(".pic_card");
  picCards.forEach((card) => {
    card.addEventListener("dragstart", handleDragStart);
  });

  // Add dragover and drop event listeners to the A-Frame scene
  const scene = document.getElementById("aframe-scene");
  scene.addEventListener("dragover", handleDragOver);
  scene.addEventListener("drop", handleDrop);
}

function addScrollWheelZoom() {
  scene.addEventListener("mousewheel", (event) => {
    const delta = Math.sign(event.wheelDelta);
    const camera = document.getElementById("cam").getAttribute("camera");
    const finalZoom = camera.zoom + delta;
    const minZoom = 1;
    const maxZoom = 5;

    camera.zoom = Math.min(Math.max(finalZoom, minZoom), maxZoom);
    document.getElementById("cam").setAttribute("camera", camera);
  });
}

function saveHotspotToDatabase(params) {
  let formData = new FormData();
  formData.append("name", params.name);
  formData.append("position", params.position);
  formData.append("rotation", params.rotation);
  formData.append("media_page_id", _currentMediapage);
  formData.append("media_type", params.media_type_id);
  formData.append("starttime", "0");
  formData.append("endtime", document.querySelector("#my-video").duration);
  formData.append("presented_media_id", params.id);

  return $.ajax({
    url: "/home/save_hotspot",
    type: "POST",
    data: formData,
    processData: false,
    contentType: false,
    success: function (response) {
      params.id = response.hotspot_id;
      createPlanesFromHotspot(params);
      socket.emit("hotspot", {
        tour: "{{ tour_id }}",
        hotspot_id: response.hotspot_id,
      });
      $("#hotspot-" + params.id).click();
    },
    error: function (xhr, status, error) {
      console.error("Error saving hotspot:", error);
    },
  });
}
/**
 * This function is used to calclulate the right rotation of a plane
 */
function eulerToAframe(euler) {
  const radToDeg = 180 / Math.PI;
  const aframeEuler = new THREE.Euler().setFromQuaternion(
    new THREE.Quaternion().setFromEuler(euler),
    "YXZ",
  );

  return {
    x: aframeEuler.x * radToDeg,
    y: aframeEuler.y * radToDeg,
    z: aframeEuler.z * radToDeg,
  };
}

/**
 * This function gets the hotspot object and places a plane from it in the A-Frame Scene
 */
function createPlanesFromHotspot(hotspot) {
  const plane = document.createElement("a-plane");

  // Set the position and rotation
  plane.setAttribute("position", `${hotspot.position}`);
  plane.setAttribute("rotation", `${hotspot.rotation} `);
  plane.setAttribute("material", "transparent: true");
  plane.setAttribute("data-hotspot-id", `${hotspot.id}`);
  plane.setAttribute("id", "hotspot-plane-" + `${hotspot.id}`);
  plane.setAttribute("width", "1.7");
  plane.setAttribute("height", "1.7");
  plane.setAttribute("dragndrop", "");
  // Set the image source
  if (hotspot.custom_display_icon_id && hotspot.custom_display_icon_id != -1) {
    plane.setAttribute(
      "src",
      `/uploaded_files/${hotspot.custom_display_icon_id}`,
    );
  } else {
    plane.setAttribute(
      "src",
      `/static/img/hotspots/${hotspot.media_type_id.toLowerCase()}.png`,
    );
  }

  // Add a class for easier selection and removal
  plane.classList.add("hotspot-plane");

  // Add a click event listener to select properties
  plane.addEventListener("click", function (e) {
    $("#hotspot-" + $(e.target).data("hotspot-id")).click();
  });

  // Append the plane to the scene or a specific entity
  const scene = document.querySelector("a-scene");
  scene.appendChild(plane);
}

/**
 * This function updates all Hotspots in the A-Frame Scene
 */
function createPlanesFromHotspots(hotspots) {
  // Remove all existing planes
  const existingPlanes = document.querySelectorAll(".hotspot-plane");
  existingPlanes.forEach((plane) => plane.parentNode.removeChild(plane));

  // Create planes for each hotspot
  hotspots.forEach((hotspot) => {
    createPlanesFromHotspot(hotspot);
  });
}
/**
 * This function is used to pan the camera to the clicked Hotspot.
 * It is important to set camera.attr('look-controls', {enabled: false}); else this is not working
 */
function panCameraToPlane(planeElement) {
  let camera = $("#cam");
  if (!planeElement || planeElement.length === 0) {
    console.error("Plane element not found.");
    return;
  }

  if (!camera || camera.length === 0) {
    console.error("Camera element not found.");
    return;
  }

  // Retrieve the Three.js object3D from the A-Frame camera and plane elements
  const cameraObject = camera.get(0);
  const planeMesh = planeElement.get(0);

  if (!cameraObject || !planeMesh) {
    console.error("Unable to retrieve the Three.js objects from the elements.");
    return;
  }
  camera.attr("look-controls", { enabled: false });
  let pointTarget = getVector(cameraObject, planeMesh);
  centerCamera(cameraObject, pointTarget);

  //cameraObject.lookAt(planeMesh);
  camera.attr("look-controls", { enabled: true });
}

function getVector(camera, targetObject) {
  var entityPosition = new THREE.Vector3();
  targetObject.object3D.getWorldPosition(entityPosition);

  var cameraPosition = new THREE.Vector3();
  camera.object3D.getWorldPosition(cameraPosition);

  //Based on:  https://github.com/supermedium/superframe/blob/master/components/look-at/index.js
  var vector = new THREE.Vector3(
    entityPosition.x,
    entityPosition.y,
    entityPosition.z,
  );
  vector.subVectors(cameraPosition, vector).add(cameraPosition);
  return vector;
}

function centerCamera(camera, vector) {
  //Based on: https://codepen.io/wosevision/pen/JWRMyK
  camera.object3D.lookAt(vector);
  camera.object3D.updateMatrix();

  //Based on: https://stackoverflow.com/questions/36809207/aframe-threejs-camera-manual-rotation
  var rotation = camera.getAttribute("rotation");
  camera.components["look-controls"].pitchObject.rotation.x =
    THREE.MathUtils.degToRad(rotation.x);
  camera.components["look-controls"].yawObject.rotation.y =
    THREE.MathUtils.degToRad(rotation.y);
}
