import smtplib
import os
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from string import Template
from dotenv import load_dotenv
from flask import request
load_dotenv()

subject_dict = {
    "registration": "Welcome to 19squared",
    "password": "Your new password for 19squared",
    "share": "Access to a tour on 19squared was granted",
    "invite_group": "You have been invited to a group on 19squared"
}


def send_email(event, to_mail, username="", tour_id=0, secret="", invite_token=""):
    if not int(os.getenv("EMAIL_NOTIFICATIONS")) or not to_mail:
        return

    from_mail = os.getenv("EMAIL_ADDRESS")
    password = os.getenv("EMAIL_PASSWORD")
    reply_mail = os.getenv("EMAIL_REPLY_TO")

    msg = MIMEMultipart('alternative')
    msg['From'] = f'19squared <{from_mail}>'
    msg['To'] = to_mail
    msg['Reply-To'] = reply_mail
    msg['Subject'] = subject_dict[event]

    html_file = os.path.join(
        "app", "utils", "mail_" + event + ".html")
    with open(html_file, "r", encoding="utf-8") as f:
        html_template = Template(f.read())

    home = request.host_url + "home"
    shared_tour = request.host_url + "home/create?id=" + str(tour_id)
    reset_secret = request.host_url + "reset?secret=" + secret
    group_token = request.host_url + "home?token=" + \
        str(invite_token) + "&email=" + to_mail
    register = request.host_url + "register?token=" + \
        str(invite_token) + "&email=" + to_mail

    html_content = html_template.substitute(
        to_mail=to_mail, username=username, home=home, shared_tour=shared_tour, reset_secret=reset_secret, invite_token=invite_token, group_token=group_token, register=register, url=request.host_url)
    msg.attach(MIMEText(html_content, 'html'))

    if int(os.getenv("EMAIL_SSL")) == 1:
        smtp = smtplib.SMTP_SSL
    else:
        smtp = smtplib.SMTP

    with smtp(os.getenv("EMAIL_SMTP_SERVER"), int(os.getenv("EMAIL_PORT"))) as server:
        if int(os.getenv("EMAIL_AUTHENTICATION")):
            server.login(from_mail, password)
        server.sendmail(from_mail, to_mail, msg.as_string())
        server.quit()
