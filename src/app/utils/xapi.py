import requests
import os
import json
import platform
import re
from datetime import datetime
from flask_security import current_user
from flask import request, current_app
from app.models import User
from VERSION import VERSION
from dotenv import load_dotenv
load_dotenv()


def lrs_push(verb, object, creation, route, rotation, tourid, mediapageid):
    if not int(os.getenv("XAPI_TRACKING")):
        return

    XAPI_ENDPOINT = os.getenv("XAPI_ENDPOINT")
    if creation:
        XAPI_AUTH = os.getenv("XAPI_AUTH_CREATION")
    else:
        XAPI_AUTH = os.getenv("XAPI_AUTH_EXPERIENCE")

    if current_user.is_authenticated:
        user = User.query.filter_by(username=current_user.username).first()
    else:
        user = False

    if user and user.la_opt_out:
        actor = {
            "name": "Opt-Out User",
            "mbox": "mailto:optout@rwth-aachen.de"
        }
    elif current_user.is_authenticated:
        actor = {
            "name": user.username,
            "mbox": 'mailto:' + (user.email or 'emptyemail@rwth-aachen.de')
        }
    else:
        actor = {
            "name": "Unregistered User",
            "mbox": "mailto:unregistered@rwth-aachen.de"
        }

    context = {
        "platform": "N/A",
        "language": "en-GB",
        "extensions": {
            "https://xapi.elearn.rwth-aachen.de/definitions/generic/extensions/context/version": VERSION,
            "https://xapi.elearn.rwth-aachen.de/definitions/gitanalysis/extensions/context/projectUrl": request.url
        }
    }

    user_platform = request.headers.get('User-Agent')
    if user_platform and len(user_platform) > 3:
        context["platform"] = user_platform

    if not creation:
        context["extensions"]["https://xapi.elearn.rwth-aachen.de/definitions/virtualReality/extensions/result/rotation"] = rotation
        context["extensions"]["https://xapi.elearn.rwth-aachen.de/definitions/virtualReality/extensions/result/position"] = mediapageid

    if tourid != 0:
        context["extensions"]["https://xapi.elearn.rwth-aachen.de/definitions/360tours/extensions/context/tourId"] = tourid

    stmt = {
        "actor": actor,
        "verb": verb,
        "object": object,
        "context": context
    }

    lrs_header = {
        "Authorization": XAPI_AUTH,
        "X-Experience-API-Version": "1.0.3",
        "Content-Type": "application/json; charset=utf-8"
    }

    res = requests.post(XAPI_ENDPOINT + '/statements',
                        data=json.dumps(stmt), headers=lrs_header)

    if res.status_code != 200:
        failed_statements_file = os.path.join(
            "app", "utils", "unsent_xapi_statements.json")
        with open(failed_statements_file, "a", encoding="utf-8") as f:
            f.write(json.dumps(stmt) + "\n")

    print(
        f'{request.remote_addr} - - [{datetime.now().strftime("%d/%b/%Y %H:%M:%S")}] "xAPI {route} HTTP/1.1" {res.status_code} -')


def lrs_event(route, rotation=0, tourid=0, mediapageid=0):
    registry_path = os.path.join(
        "app", "utils", "xapi_vocabulary.json")
    with open(registry_path, "r", encoding="utf-8") as file:
        registry = json.load(file)

    event = registry.get(route)

    if (tourid == 0):
        tourid = re.search(
            r"id=(\d+)", request.url).group(1) if re.search(r"id=(\d+)", request.url) else 0

    if event:
        lrs_push(event["verb"], event["object"],
                 event["creation"], route, rotation, tourid, mediapageid)
    else:
        return
