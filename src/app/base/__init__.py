from flask import Blueprint

# Define the blueprint: 'base', set its url prefix: app.url/base
base_bp = Blueprint(
    'base',
    __name__,
    url_prefix='/'
)
