from flask import send_from_directory, abort
import os
import json
import time
import requests
import io
from datetime import datetime
from VERSION import VERSION
from flask import render_template, redirect, request, url_for, abort, Blueprint, flash, send_file, jsonify, current_app, g
from flask_babel import get_locale
from flask_security import Security, SQLAlchemySessionUserDatastore, roles_accepted, login_required, current_user, permissions_required, user_authenticated
from app import uploaded_images
from sqlalchemy import desc, and_, not_, func
from app.models import User, Hotspot, Mediapage, Tour, SharedTour, SharedGroupTour, Asset, Group, GroupUsers, Role, Dependencies, EscapeRoomAnswers, HotspotVisited
from app.base import base_bp
from app import home, db, app, user_datastore
from werkzeug.utils import secure_filename
from app.utils.xapi import lrs_event
from alembic.config import Config
from alembic import command

path_to_static = os.path.join(os.path.join(os.getcwd(), "app"), "static")
path_to_node_modules = os.path.join(os.getcwd(), "node_modules")


@base_bp.before_request
def require_login():
    g.locale = str(get_locale())
    g.locales = os.getenv('LANGUAGES').split(",")
    """Ensure the user is logged in"""
    if request.path.startswith('/home') and not current_user.is_authenticated:
        return redirect(url_for('auth.login', email=request.args.get('email'), token=request.args.get('token')))


@base_bp.route('/', endpoint='index')
def index():
    """
    this function shows the index page depending on the login of a user
    """
    user_id = current_user.id if current_user.is_authenticated else None
    lrs_event('/')
    if (user_id):
        return redirect(url_for('home.home_page', email=request.args.get('email'), token=request.args.get('token')))
    else:
        return render_template('index.html', currentYear=datetime.now().year, VERSION=VERSION)


@base_bp.route('/node_modules/<path:filename>', endpoint='node_modules')
def node_modules(filename):
    return send_from_directory(path_to_node_modules, filename)


@base_bp.route('/uploaded_files/<filename>', methods=['GET', 'POST'], endpoint='serve_uploaded_file')
def serve_uploaded_file(filename):
    """
    this function serves files from /app/Uploaded_Media to the user
    """
    asset_url = f"http://{app.config['ASSET_URL']}/{filename}"

    # start_time = time.monotonic()
    response = requests.get(asset_url, stream=True)
    # end_time = time.monotonic()
    # elapsed_time = end_time - start_time
    # print(f"Time taken for request: {elapsed_time:.2f} seconds")

    lrs_event('/uploaded_files/<filename>')
    return send_file(
        io.BytesIO(response.content),
        mimetype=response.headers.get('content-type'),
        as_attachment=False,
        download_name=filename
    )


@base_bp.route('/uploaded_files/thumbnails/<filename>', methods=['GET', 'POST'], endpoint='serve_uploaded_thumbnails')
def serve_uploaded_thumbnails(filename):
    """
    this function serves files like serve_uploaded_file() but from the thumbnails folder
    """
    return serve_uploaded_file(f"thumbnails/{filename}")


@base_bp.route('/get_hotspot', methods=['POST'], endpoint='get_hotspot')
def get_hotspot():
    """
    this function returns all Hotspots which belongs to a Mediapage but also requires the tour_id
    """
    hotspot = Hotspot.query.get(request.get_json())
    lrs_event('/get_hotspot')
    return jsonify(hotspot.to_dict())


@base_bp.route('/get_hotspots', methods=['POST'], endpoint='get_hotspots')
def get_hotspots():
    """
    this function returns Hotspot which belongs to a Mediapage but also requires the tour_id
    """
    data = request.get_json()
    tour_id = data.get('tour_id')
    mediapage_id = data.get('mediapage_id')

    if not tour_id or not mediapage_id:
        return render_template("error.html", status_code=400, status_message="Tour or Mediapage not found")

    hotspots = Hotspot.query.filter_by(media_page_id=mediapage_id).all()
    hotspots_list = [hotspot.to_dict() for hotspot in hotspots]

    lrs_event('/get_hotspots')
    return jsonify(hotspots_list)


@base_bp.route('/get_mediapages', methods=['POST'], endpoint='get_mediapages')
def get_mediapages():
    """
    this function returns all Mediapages which belongs to a Tour
    """
    data = request.get_json()
    tour_id = data.get('tour_id')

    if not tour_id:
        return render_template("error.html", status_code=400, status_message="Tour not found")

    mediapages = Mediapage.query.filter_by(tour_id=tour_id).all()
    mediapages_list = [mediapage.to_dict() for mediapage in mediapages]

    lrs_event('/get_Mediapages')
    return jsonify(mediapages_list)


@base_bp.route('/view_tour/<int:id>', endpoint='view_tour')
def view_tour(id, share_link=False, full_preload=False):
    """
    this function calls get_hotspots() but from a different route
    """
    if share_link:
        # Retrieve tour by share string
        tour = Tour.query.filter_by(share_string=id).first()
    else:
        if not current_user.is_authenticated:
            return redirect(url_for('auth.login', email=request.args.get('email'), token=request.args.get('token'), view_tour=id))

        # Retrieve tour by id and creator_id
        tour = Tour.query.filter_by(id=id, creator_id=current_user.id).first()
        if current_user.has_permission('user_management'):
            tour = Tour.query.filter_by(id=id).first()

        # Check if the tour is shared with the current user or one of the user's groups
        if not tour:
            shared_tour = SharedTour.query.filter_by(
                tour_id=id, collaborator_id=current_user.id).first()
            shared_group_tour = SharedGroupTour.query.join(GroupUsers, GroupUsers.group_id == SharedGroupTour.group_id).filter(
                SharedGroupTour.tour_id == id, GroupUsers.user_id == current_user.id).first()
            if shared_tour or shared_group_tour:
                tour = Tour.query.filter_by(id=id).first()
            else:
                if current_user.is_authenticated and Tour.query.filter_by(id=id).first():
                    return render_template("error.html", status_code=403, status_message="Access denied")
                else:
                    return render_template("error.html", status_code=404, status_message="Tour not found")

    # Retrieve all mediapages and hotspots associated with the tour
    mediapages = Mediapage.query.filter_by(tour_id=tour.id).all()
    # hotspots = Hotspot.query.join(Mediapage, and_(
    #    Hotspot.media_page_id == Mediapage.id, Mediapage.tour_id == tour.id)).all()
    mediapages_dicts = [mediapage.to_dict() for mediapage in mediapages]
    """ hotspots = Hotspot.query.add_column(func.group_concat(Dependencies.dependsOn).label('dependencies'))
    hotspots = hotspots.join(Mediapage, and_(Hotspot.media_page_id == Mediapage.id, Mediapage.tour_id == tour.id))
    hotspots = hotspots.join(Dependencies, Hotspot.id == Dependencies.hotspot_id, isouter=True).group_by(Hotspot.id).all()
    
    hotspots_dicts = [{ **hotspot[0].to_dict(), "dependencies": [int(x) for x in hotspot[1].split(',')] if hotspot[1] else []} for hotspot in hotspots] """

    # [ { hotspot_id: 2, dependencies: [3, 4, 5] }, .... ]
    # currentSession = Sessions.query.filter_by(user_id=current_user.id , tour_id = tour.id).all()
    # add HotpotVisited to mediapages_dicts
    if current_user.is_authenticated:
        for mediapage in mediapages_dicts:
            for hotspot in mediapage["hotspots"]:
                hotspot["visited"] = HotspotVisited.query.filter_by(
                    user_id=current_user.id, hotspot_id=hotspot["id"]).first() is not None

    lrs_event('/view_tour/<int:id>')
    return render_template('view_tour.html', tour_id=tour.id, mediapages=mediapages_dicts,
                           mediatypes='mediatypes_dict', currentYear=datetime.now().year, full_preload=full_preload, VERSION=VERSION)


@base_bp.route('/view/<string:id>', endpoint='view_tour_by_sharestring')
def view_tour_by_sharestring(id):
    """
    this function does the same as view_tour but no login is required because the share-string was used
    """
    return view_tour(id, share_link=True)


@base_bp.route('/view_offline/<string:id>', endpoint='view_tour_fully_preloaded')
def view_tour_fully_preloaded(id):
    """
    this function does the same as view_tour but all assets get preloaded in the beginning
    """
    return view_tour(id, full_preload=True)


@base_bp.route('/setHotspotReset', methods=['POST'])
def setHotspotReset():
    data = request.get_json()
    tour_id = data.get('tour_id')
    hotspotVisitedArray = HotspotVisited.query.filter_by(
        user_id=current_user.id, tour_id=tour_id).all()
    for hotVis in hotspotVisitedArray:
        db.session.delete(hotVis)
        db.session.commit()
    return jsonify({'message': 'Hotspot reset successfully'})


@base_bp.route('/visitedHotspot', methods=['POST'])
def visitedHotspot():
    data = request.get_json()
    id = data.get('id')
    tour_id = data.get('_tour_id')
    print(id)
    print(tour_id)

    existingHotspotVisited = HotspotVisited.query.filter_by(
        hotspot_id=id, user_id=current_user.id, tour_id=tour_id).first()
    if not existingHotspotVisited:
        newHotspotVisited = HotspotVisited(
            user_id=current_user.id, hotspot_id=id, tour_id=tour_id)
        db.session.add(newHotspotVisited)
        db.session.commit()

    ready_hotspots = []

    # dependent hotspots
    possHotspots = Dependencies.query.filter(
        Dependencies.dependsOn == id).all()

    for hot in possHotspots:
        possible = True
        depends = Dependencies.query.filter(
            Dependencies.hotspot_id == hot.hotspot_id).all()
        for dep in depends:

            dephot = HotspotVisited.query.filter_by(
                hotspot_id=dep.dependsOn, user_id=current_user.id, tour_id=tour_id).first()
            if not dephot:
                possible = False
        # Wenn alle abhängigen Hotspots visited sind dann wird der Hotspot in die Liste gepackt
        if (possible):
            ready_hotspots.append(hot.hotspot_id)

    return jsonify(ready_hotspots)


@base_bp.route('/setVisibleIfLocked', methods=['POST'])
def setVisibleIfLocked():
    data = request.get_json()
    hotspotid = data.get('hotspotid')
    state = data.get('state')
    hotspot = Hotspot.query.get(hotspotid)
    hotspot.visibleIfLocked = state
    db.session.commit()
    return jsonify({'message': 'VisibleIfLocked toggled successfully'})


@base_bp.route('/setDependency', methods=['POST'], endpoint='setDependency')
def setDependency():
    data = request.get_json()
    print(data)
    hotspotid = data.get('hotspot_id')
    dependsOnid = data.get('dependsOnid')
    media_page_id = data.get('media_page_id')
    tour_id = db.session.query(Mediapage).filter(
        Mediapage.id == media_page_id).first().tour_id

    existing_dependency = Dependencies.query.filter_by(
        hotspot_id=hotspotid, dependsOn=dependsOnid).first()

    if existing_dependency:
        db.session.delete(existing_dependency)
        db.session.commit()
        print('deleted successful')
        return jsonify({'message': 'Dependency removed', 'db_Id': existing_dependency.id})

    else:
        deadlock_dependency = db.session.query(Dependencies).join(Hotspot, Hotspot.id == Dependencies.hotspot_id).join(
            Mediapage, Mediapage.id == Hotspot.media_page_id).filter(Mediapage.tour_id == tour_id).all()
        if sanityCheck(deadlock_dependency, hotspotid, dependsOnid):
            return jsonify({'message': 'Deadlock detected'})
        else:
            new_dependency = Dependencies(
                hotspot_id=hotspotid, dependsOn=dependsOnid)
            db.session.add(new_dependency)
            db.session.commit()
            return jsonify({'message': 'Dependency added', 'db_Id': new_dependency.id})


def sanityCheck(dependencies, hotspot_id, dependsOn):
    cycle = {}
    callStack = set()
    finished = set()
    cycle[hotspot_id] = []
    cycle[hotspot_id].append(dependsOn)

    for dep in dependencies:
        if dep.hotspot_id not in cycle:
            cycle[dep.hotspot_id] = []
        cycle[dep.hotspot_id].append(dep.dependsOn)

    def depthSearch(key):
        if key in finished:
            return False
        if key in callStack:
            return True
        callStack.add(key)

        for node in cycle.get(key, []):
            if depthSearch(node):
                return True

        callStack.remove(key)
        finished.add(key)
        return False

    for key in cycle:
        if depthSearch(key):
            return True
    return False

    db.session.commit()

    lrs_event('/setDependency')
    return jsonify({"message": "Dependencies updated successfully"}), 200


@base_bp.route('/getDependency', methods=['POST'])
def getDependency():
    data = request.get_json()
    id = data.get('id')
    dependencies = Dependencies.query.filter_by(hotspot_id=id).all()
    depends_on_list = [dep.dependsOn for dep in dependencies]
    return jsonify(depends_on_list)


@base_bp.route('/setQuizQuestion', methods=['POST'])
def setQuizQuestion():

    data = request.get_json()
    hotspotid = data.get('hotspotid')
    answer = data.get('question')

    print(hotspotid)
    print(answer)

    hotspot = Hotspot.query.get(hotspotid)
    hotspot.question = answer
    db.session.commit()
    return jsonify({'message': 'Question added successfully'})


@base_bp.route('/setQuizAnswer', methods=['POST'])
def setQuizAnswer():

    data = request.get_json()
    answerId = data.get('inputId')
    answerText = data.get('answerText')

    answer = EscapeRoomAnswers.query.get(answerId)
    answer.answer = answerText
    db.session.commit()
    return jsonify({'message': 'Answer updated successfully'})


@base_bp.route('/addQuizAnswer', methods=['POST'])
def addQuizAnswer():

    data = request.get_json()
    hotspotid = data.get('hotspotid')

    try:
        answer = EscapeRoomAnswers(
            hotspot_id=hotspotid, answer="", correct=False)
        db.session.add(answer)
        db.session.commit()
        return jsonify({'id': answer.id})
    except:
        return jsonify({'message': 'Error while adding Answer'})


@base_bp.route('/deleteQuizAnswer', methods=['POST'])
def deleteQuizAnswer():

    data = request.get_json()
    answerId = data.get('answerid')

    existing_answer = EscapeRoomAnswers.query.filter_by(id=answerId).first()

    if existing_answer:
        db.session.delete(existing_answer)
        db.session.commit()
        return jsonify({'message': 'Answer removed successfully'})
    else:
        return jsonify({'message': 'Error while deleting Answer'})


@base_bp.route('/toggleQuizAnswer', methods=['POST'])
def toggleQuizAnswer():

    data = request.get_json()
    answerId = data.get('inputId')
    answerState = data.get('answerState')

    existing_answer = EscapeRoomAnswers.query.filter_by(id=answerId).first()
    existing_answer.correct = answerState
    db.session.commit()
    return jsonify({'message': 'Answer toggled successfully'})


@base_bp.route('/lrs_event_vr', methods=['POST'], endpoint='lrs_event_vr')
def lrs_event_vr():
    data = request.get_json()
    lrs_event(data.get('event'), rotation=data.get('rotation'),
              tourid=data.get('tourid'), mediapageid=data.get('mediapageid'))
    return jsonify({"message": "xAPI-Event triggered successfully"}), 201


@base_bp.route('/garbage_collector', methods=['GET'], endpoint='garbage_collector')
@login_required
def garbage_collector():
    # 1. Get all references from the database
    asset_filenames = db.session.query(Asset.filename_id).all()

    mediapage_filenames = (
        db.session.query(Mediapage.asset_id)
        .filter(Mediapage.tour_id.isnot(None))
        .all()
    )

    hotspot_filenames = (
        db.session.query(Hotspot.presented_media_id)
        .join(Mediapage, Hotspot.media_page_id == Mediapage.id)
        .filter(Mediapage.tour_id.isnot(None))
        .all()
    )

    referenced_filenames = {
        filename[0]
        for filenames in (asset_filenames, mediapage_filenames, hotspot_filenames)
        for filename in filenames
    }

    # 2. Get all files in the Uploaded_Media directory EXCLUDING the "thumbnails" subdirectory
    media_dir = current_app.config['UPLOADED_IMAGES_DEST']
    all_filenames = set(
        filename
        for filename in os.listdir(media_dir)
        if not filename.startswith("mll")  # Exclude "mll" demo files
        # Exclude subdirectories
        and os.path.isfile(os.path.join(media_dir, filename))
    )

    # 3. Find unreferenced files to delete
    files_to_delete = all_filenames - referenced_filenames

    # 4. Delete unreferenced files
    total_size_freed = 0
    for filename in files_to_delete:
        file_path = os.path.join(media_dir, filename)
        total_size_freed += os.path.getsize(file_path)
        os.remove(file_path)

    lrs_event('/garbage_collector')
    return jsonify({"message": f"Deleted {len(files_to_delete)} unreferenced file(s), making {round(total_size_freed / (1024 * 1024), 2)} MB free."}), 200


@base_bp.route('/impressum')
def impressum():
    return render_template('impressum.html', currentYear=datetime.now().year, VERSION=VERSION)


@base_bp.route('/datenschutz', endpoint='datenschutz')
def datenschutz():
    return render_template('datenschutz.html', currentYear=datetime.now().year, VERSION=VERSION)


@base_bp.route('/migration_autogenerate', endpoint='migration_autogenerate')
@login_required
def migration_autogenerate():
    alembic_cfg = Config("alembic.ini")
    command.revision(alembic_cfg, autogenerate=True)

    return jsonify({"message": "Alemic revision created successfully"}), 200
