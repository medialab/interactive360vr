from flask import render_template, flash, redirect, url_for, request, session, jsonify, current_app, g
from flask_babel import get_locale, gettext as _
from flask_security import Security, SQLAlchemySessionUserDatastore, roles_accepted, login_required, current_user, login_user, logout_user
from sqlalchemy import or_
from datetime import datetime
from app.models import User, Role, GroupUsers
from app import db, user_datastore
from . import auth_bp
from VERSION import VERSION
from app.utils.xapi import lrs_event
from app.utils.mail import send_email
import random
import os
import re
from authlib.integrations.flask_client import OAuth
from dotenv import load_dotenv
load_dotenv()


@auth_bp.before_request
def before_request():
    g.locale = str(get_locale())
    g.locales = os.getenv('LANGUAGES').split(",")


@auth_bp.route('/login', methods=['GET', 'POST'], endpoint='login')
def login():
    """
    this function logins a user and set the session cookie
    """
    if current_user.is_authenticated:
        return redirect(url_for('base.index', email=request.form.get('email', ''), token=request.args.get('token')))

    if request.method == 'GET':
        username_or_email = request.args.get('username')
        password = request.args.get('password')
        remember_me = request.form.get('remember_me', False)
        la = request.form.get('la', False)

    if request.method == 'POST':
        username_or_email = request.form.get('username_or_email')
        password = request.form['password']
        remember_me = request.form.get('remember_me', False)
        la = request.form.get('la', False)

    if username_or_email and password:
        user = User.query.filter(
            or_(User.username == username_or_email, User.email == username_or_email)).first()
        if user:
            if user.password_hash:
                if user.check_password(password):
                    user.la_opt_out = (la == "on")
                    user.last_login = datetime.now()
                    db.session.commit()
                    login_user(user, remember=remember_me)
                    lrs_event('/login')
                    print(dict(session))
                    if request.args.get('view_tour'):
                        return redirect(url_for('base.view_tour', id=request.args.get('view_tour')))
                    return redirect(url_for('base.index', email=request.args.get('email'), token=request.args.get('token')))
            else:
                flash(
                    _('Your password is not set. Maybe you used Google, GitLab or GitHub for registration.'), 'danger')
                return redirect(url_for('auth.login'))
        flash(_('Incorrect credentials. Please try again.'), 'danger')
        return redirect(url_for('auth.login'))

    return render_template('auth/login.html', currentYear=datetime.now().year, VERSION=VERSION)


@auth_bp.route('/logout', endpoint='logout')
@login_required
def logout():
    """
    this function logs out a user (so much wow)
    """
    lrs_event('/logout')
    logout_user()
    flash(_('You have been logged out.'), 'info')
    return redirect(url_for('auth.login'))


@auth_bp.route('/register', methods=['GET', 'POST'])
def register():
    """
    this function creates a new user in the database
    """
    if current_user.is_authenticated:
        return redirect(url_for('base.index'))

    if request.method == 'POST':
        if os.getenv('ALLOW_NEW_USERS') and not int(os.getenv('ALLOW_NEW_USERS')):
            flash(
                'Registration for new users has been disabled by the administrator.', 'danger')
            return redirect(url_for('auth.register'))

        username = request.form['username']
        password = request.form['password']
        if request.form['email'] == "":
            email = None
            if os.getenv('ALLOW_EMPTY_MAIL') and not int(os.getenv('ALLOW_EMPTY_MAIL')):
                flash(
                    'Registration without e-mail address has been disabled by the administrator.', 'danger')
                return redirect(url_for('auth.register'))
        else:
            email = request.form['email']

        if len(password) < 6:
            flash(_('Your password needs to be at least 6 characters long.'), 'danger')
            return redirect(url_for('auth.register', email=request.form.get('email', ''), token=request.args.get('token')))

        existing_username = User.query.filter_by(username=username).first()
        if existing_username:
            flash(_('Username already exists.'), 'danger')
            return redirect(url_for('auth.register', email=request.form.get('email', ''), token=request.args.get('token')))

        if email:
            existing_email = User.query.filter_by(email=email).first()
            if existing_email:
                flash(_('Email already exists.'), 'danger')
                return redirect(url_for('auth.register', email=request.form.get('email', ''), token=request.args.get('token')))

        user = User(username=username, email=email)
        user.set_password(password)
        user.language = str(get_locale())

        # initial user role
        roles = Role.query.all()
        for role in roles:
            if role.assign_to_new_user:
                user_datastore.add_role_to_user(user, role)

        db.session.add(user)
        db.session.commit()

        # check if email has been invited to a group
        if email:
            group_invites = GroupUsers.query.filter_by(
                invite_email=email).all()
            for group_invite in group_invites:
                group_invite.user_id = user.id
                db.session.commit()

        lrs_event('/register')
        send_email("registration", email, username)
        flash(
            _('Account created successfully! Please login with your credentials.'), 'success')
        return redirect(url_for('auth.login', email=request.form.get('email', ''), username=request.form.get('username', ''), token=request.args.get('token')))

    return render_template('auth/register.html', currentYear=datetime.now().year, VERSION=VERSION)


@auth_bp.route('/forgotten_password', methods=['POST'], endpoint='forgotten_password')
def forgotten_password():
    """
    this function sends password forgotten mails
    """
    data = request.get_json()
    username_or_email = data.get("username_or_email")
    user = User.query.filter(
        or_(User.username == username_or_email, User.email == username_or_email)).first()

    if not user:
        flash(_('User not found. Please try again.'), 'danger')
        return jsonify({"message": "User not found"}), 404

    secret = ''.join(random.choice(
        "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789") for _ in range(16))
    user.reset_secret = secret
    db.session.commit()

    lrs_event('/forgotten_password')
    send_email("password", user.email, user.username, secret=secret)
    flash(_('You should receive an email shortly with your username and instructions for resetting your password.'), 'success')
    return jsonify({"message": "Mail sent successfully"}), 200


@auth_bp.route('/reset', methods=['GET'], endpoint='reset')
def reset():
    """
    this function allows to change user data
    """
    secret = request.args.get('secret') or 0
    user = User.query.filter(User.reset_secret == secret).first()

    if not secret or not user:
        return render_template("error.html", status_code=401, status_message="Reset link is not valid")

    return render_template('auth/reset.html', currentYear=datetime.now().year, VERSION=VERSION)


@auth_bp.route('/set_password', methods=['POST'], endpoint='set_password')
def set_password():
    """
    this function sets the new password
    """
    data = request.get_json()
    new_password = data.get("new_password")
    secret = data.get("secret")

    user = User.query.filter(User.reset_secret == secret).first()
    user.set_password(new_password)
    user.reset_secret = None
    db.session.commit()

    lrs_event('/forgotten_password')
    return jsonify({"message": "Password reset successful"}), 200


oauth = OAuth(current_app)

oauth.register(
    name='google',
    client_id=os.getenv('GOOGLE_CLIENT_ID'),
    client_secret=os.getenv('GOOGLE_CLIENT_SECRET'),
    server_metadata_url='https://accounts.google.com/.well-known/openid-configuration',
    client_kwargs={
        'scope': 'openid email profile'
    }
)


@auth_bp.route('/login/google', endpoint='login_google')
def login_google():
    redirect_uri = url_for('auth.authorize_google', _external=True)
    return oauth.google.authorize_redirect(redirect_uri)


@auth_bp.route('/authorize/google', endpoint='authorize_google')
def authorize_google():
    token = oauth.google.authorize_access_token()
    resp = oauth.google.get('https://www.googleapis.com/oauth2/v3/userinfo')
    user_info = resp.json()

    if 'email' not in user_info or not user_info['email']:
        flash('Google did not provide an email address for authorization.', 'danger')
        return redirect(url_for('auth.register'))

    user = User.query.filter_by(email=user_info['email']).first()
    if not user:
        username = re.sub(r'[^a-zA-Z]', '', user_info['name'])
        new_user = User.query.filter_by(username=username).first()
        while new_user:
            username = username + "1"
            new_user = User.query.filter_by(username=username).first()

        user = User(username=username, email=user_info['email'])
        user.language = str(get_locale())
        # initial user role
        roles = Role.query.all()
        for role in roles:
            if role.assign_to_new_user:
                user_datastore.add_role_to_user(user, role)

        db.session.add(user)
        db.session.commit()

        # check if email has been invited to a group
        group_invites = GroupUsers.query.filter_by(
            invite_email=user_info['email']).all()
        for group_invite in group_invites:
            group_invite.user_id = user.id
            db.session.commit()

        lrs_event('/register')
        send_email("registration", user.email, username)

    user.last_login = datetime.now()
    db.session.commit()
    login_user(user)
    lrs_event('/login')
    return redirect(url_for('base.index'))


oauth.register(
    name='gitlab',
    client_id=os.getenv('GITLAB_CLIENT_ID'),
    client_secret=os.getenv('GITLAB_CLIENT_SECRET'),
    access_token_url='https://gitlab.com/oauth/token',
    authorize_url='https://gitlab.com/oauth/authorize',
    api_base_url='https://gitlab.com/api/v4/',
    jwks_uri='https://gitlab.com/oauth/discovery/keys',
    client_kwargs={
        'scope': 'openid email profile read_user'
    }
)


@auth_bp.route('/login/gitlab', endpoint='login_gitlab')
def login_gitlab():
    redirect_uri = url_for('auth.authorize_gitlab', _external=True)
    return oauth.gitlab.authorize_redirect(redirect_uri)


@auth_bp.route('/authorize/gitlab', endpoint='authorize_gitlab')
def authorize_gitlab():
    token = oauth.gitlab.authorize_access_token()
    resp = oauth.gitlab.get('user')
    user_info = resp.json()

    if 'email' not in user_info or not user_info['email']:
        flash('GitLab did not provide an email address for authorization.', 'danger')
        return redirect(url_for('auth.register'))

    user = User.query.filter_by(email=user_info['email']).first()
    if not user:
        username = re.sub(r'[^a-zA-Z]', '', user_info['username'])
        new_user = User.query.filter_by(username=username).first()
        while new_user:
            username = username + "1"
            new_user = User.query.filter_by(username=username).first()

        user = User(username=username, email=user_info['email'])
        user.language = str(get_locale())
        # initial user role
        roles = Role.query.all()
        for role in roles:
            if role.assign_to_new_user:
                user_datastore.add_role_to_user(user, role)

        db.session.add(user)
        db.session.commit()

        # check if email has been invited to a group
        group_invites = GroupUsers.query.filter_by(
            invite_email=user_info['email']).all()
        for group_invite in group_invites:
            group_invite.user_id = user.id
            db.session.commit()

        lrs_event('/register')
        send_email("registration", user.email, username)

    user.last_login = datetime.now()
    db.session.commit()
    login_user(user)
    lrs_event('/login')
    return redirect(url_for('base.index'))


oauth.register(
    name='github',
    client_id=os.getenv('GITHUB_CLIENT_ID'),
    client_secret=os.getenv('GITHUB_CLIENT_SECRET'),
    access_token_url='https://github.com/login/oauth/access_token',
    authorize_url='https://github.com/login/oauth/authorize',
    api_base_url='https://api.github.com/',
)


@auth_bp.route('/login/github', endpoint='login_github')
def login_github():
    redirect_uri = url_for('auth.authorize_github', _external=True)
    return oauth.github.authorize_redirect(redirect_uri)


@auth_bp.route('/authorize/github', endpoint='authorize_github')
def authorize_github():
    token = oauth.github.authorize_access_token()
    resp = oauth.github.get('user')
    user_info = resp.json()

    if 'email' not in user_info or not user_info['email']:
        flash('GitHub did not provide an email address for authorization.', 'danger')
        return redirect(url_for('auth.register'))

    user = User.query.filter_by(email=user_info['email']).first()
    if not user:
        username = re.sub(r'[^a-zA-Z]', '', user_info['username'])
        new_user = User.query.filter_by(username=username).first()
        while new_user:
            username = username + "1"
            new_user = User.query.filter_by(username=username).first()

        user = User(username=username, email=user_info['email'])
        user.language = str(get_locale())
        # initial user role
        roles = Role.query.all()
        for role in roles:
            if role.assign_to_new_user:
                user_datastore.add_role_to_user(user, role)

        db.session.add(user)
        db.session.commit()

        # check if email has been invited to a group
        group_invites = GroupUsers.query.filter_by(
            invite_email=user_info['email']).all()
        for group_invite in group_invites:
            group_invite.user_id = user.id
            db.session.commit()

        lrs_event('/register')
        send_email("registration", user.email, username)

    user.last_login = datetime.now()
    db.session.commit()
    login_user(user)
    lrs_event('/login')
    return redirect(url_for('base.index'))
