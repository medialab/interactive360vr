from flask import Blueprint

# Define the blueprint: 'base', set its url prefix: app.url/base
auth_bp = Blueprint(
    'auth',
    __name__,
    url_prefix='/'
)
