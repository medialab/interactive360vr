import * as CookieConsent from "../../node_modules/vanilla-cookieconsent/dist/cookieconsent.esm.js";

CookieConsent.run({
  guiOptions: {
    consentModal: {
      layout: "cloud",
      position: "bottom left",
      equalWeightButtons: false,
      flipButtons: false,
    },
    preferencesModal: {
      layout: "box",
      position: "right",
      equalWeightButtons: false,
      flipButtons: false,
    },
  },
  categories: {
    necessary: {
      readOnly: true,
    },
  },
  language: {
    default: "de",
    autoDetect: "browser",
    translations: {
      de: {
        consentModal: {
          title: "Süße Nachrichten: Wir verwenden Cookies",
          description:
            "Um den Betrieb unserer Webseite zu ermöglichen und bestimmte Funktionen sicherzustellen, verwenden wir technisch notwendige Cookies. Diese Cookies sind für die Funktionalität der Webseite unerlässlich und erfordern keine explizite Einwilligung.",
          acceptAllBtn: "Akzeptieren",
          showPreferencesBtn: "Einstellungen verwalten",
          footer:
            '<a href="/impressum">Impressum</a>\n<a href="/datenschutz">Datenschutzerklärung</a>',
        },
        preferencesModal: {
          title: "Präferenzen für die Zustimmung",
          acceptAllBtn: "Akzeptieren",
          closeIconLabel: "Modal schließen",
          serviceCounterLabel: "Dienstleistungen",
          sections: [
            {
              title: "Verwendung von Cookies",
              description:
                "Diese Website verwendet Cookies, um Ihr Nutzererlebnis zu verbessern und bestimmte Funktionen bereitzustellen. Einige Cookies sind technisch notwendig, während andere uns helfen, die Website zu analysieren und Ihnen personalisierte Inhalte anzuzeigen. Sie können Ihre Cookie-Einstellungen jederzeit anpassen.",
            },
            {
              title:
                'Streng Notwendige Cookies <span class="pm__badge">Immer Aktiviert</span>',
              description:
                "Diese Cookies sind für den Betrieb der Website unerlässlich und ermöglichen grundlegende Funktionen wie die Seitennavigation und den Zugriff auf sichere Bereiche. Sie speichern keine personenbezogenen Daten und erfordern keine explizite Einwilligung.",
              linkedCategory: "necessary",
            },
          ],
        },
      },
      en: {
        consentModal: {
          title: "Sweet News: We Use Cookies",
          description:
            "To enable the operation of our website and ensure certain functions, we use technically necessary cookies. These cookies are essential for the functionality of the website and do not require explicit consent.",
          acceptAllBtn: "Accept",
          showPreferencesBtn: "Manage preferences",
          footer:
            '<a href="/impressum">Legal Notice</a>\n<a href="/datenschutz">Privacy Policy</a>',
        },
        preferencesModal: {
          title: "Consent Preferences Center",
          acceptAllBtn: "Accept",
          closeIconLabel: "Close modal",
          serviceCounterLabel: "Service|Services",
          sections: [
            {
              title: "Use of Cookies",
              description:
                "This website uses cookies to enhance your user experience and provide certain features. Some cookies are technically necessary, while others help us analyze the website and display personalized content to you. You can adjust your cookie settings at any time.",
            },
            {
              title:
                'Strictly Necessary Cookies <span class="pm__badge">Always Enabled</span>',
              description:
                "These cookies are essential for the operation of the website and enable basic functions like page navigation and access to secure areas. They do not store any personal data and do not require explicit consent.",
              linkedCategory: "necessary",
            },
          ],
        },
      },
    },
  },
  disablePageInteraction: true,
});
