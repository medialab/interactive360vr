# Translations template for PROJECT.
# Copyright (C) 2025 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2025.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2025-01-08 14:42+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.15.0\n"

#: __init__.py:22
msgid "create_tour"
msgstr ""

#: __init__.py:22
msgid "Allows creating a new tour"
msgstr ""

#: __init__.py:23
msgid "edit_tour"
msgstr ""

#: __init__.py:23
msgid "Allows editing an existing tour"
msgstr ""

#: __init__.py:24
msgid "rename_tour"
msgstr ""

#: __init__.py:24
msgid "Allows renaming a tour"
msgstr ""

#: __init__.py:25
msgid "copy_tour"
msgstr ""

#: __init__.py:25
msgid "Allows duplicating a tour"
msgstr ""

#: __init__.py:26
msgid "delete_tour"
msgstr ""

#: __init__.py:26
msgid "Allows deleting a tour"
msgstr ""

#: __init__.py:27
msgid "share_tour"
msgstr ""

#: __init__.py:27
msgid "Allows sharing a tour via sharelink with view-only"
msgstr ""

#: __init__.py:28
msgid "export_tour"
msgstr ""

#: __init__.py:28
msgid "Allows exporting a tour to an external format"
msgstr ""

#: __init__.py:29
msgid "import_tour"
msgstr ""

#: __init__.py:29
msgid "Allows importing a tour from an external format"
msgstr ""

#: __init__.py:30
msgid "add_demo_tour"
msgstr ""

#: __init__.py:30
msgid "Allows adding a copy of the demo tour to the own tours"
msgstr ""

#: __init__.py:31
msgid "edit_collaborator"
msgstr ""

#: __init__.py:31
msgid "Allows editing collaborators on a project"
msgstr ""

#: __init__.py:32
msgid "create_group"
msgstr ""

#: __init__.py:32
msgid "Allows creating new groups"
msgstr ""

#: __init__.py:33
msgid "edit_group"
msgstr ""

#: __init__.py:33
msgid "Allows modifying group details"
msgstr ""

#: __init__.py:34
msgid "delete_group"
msgstr ""

#: __init__.py:34
msgid "Allows deleting groups"
msgstr ""

#: __init__.py:35
msgid "edit_roles"
msgstr ""

#: __init__.py:35
msgid "Allows modifying roles of an user"
msgstr ""

#: __init__.py:36
msgid "set_demotour"
msgstr ""

#: __init__.py:36
msgid "Allows to set a tour as the demo tour on the public start page"
msgstr ""

#: __init__.py:37
msgid "user_management"
msgstr ""

#: __init__.py:37
msgid "Allows managing user accounts"
msgstr ""

#: __init__.py:38
msgid "role_management"
msgstr ""

#: __init__.py:38
msgid "Allows managing roles and permissions"
msgstr ""

#: __init__.py:39
msgid "delete_user"
msgstr ""

#: __init__.py:39
msgid "Allows delete his own account"
msgstr ""

#: auth/routes.py:63
msgid ""
"Your password is not set. Maybe you used Google, GitLab or GitHub to "
"registration."
msgstr ""

#: auth/routes.py:65
msgid "Incorrect credentials. Please try again."
msgstr ""

#: auth/routes.py:79
msgid "You have been logged out."
msgstr ""

#: auth/routes.py:100
msgid "Your password needs to be at least 6 characters long."
msgstr ""

#: auth/routes.py:105
msgid "Username already exists."
msgstr ""

#: auth/routes.py:111
msgid "Email already exists."
msgstr ""

#: auth/routes.py:138
msgid "Account created successfully! Please login with your credentials."
msgstr ""

#: auth/routes.py:155
msgid "User not found. Please try again."
msgstr ""

#: auth/routes.py:165
msgid ""
"You should receive an email shortly with your username and instructions "
"for resetting your password."
msgstr ""

#: home/routes.py:1118
msgid "Token already used by another account!"
msgstr ""

#: home/routes.py:1122
msgid "You are already in this group!"
msgstr ""

#: home/routes.py:1132 home/routes.py:1149
msgid "The token was invalid!"
msgstr ""

#: home/routes.py:1135
msgid "Group successfully joined."
msgstr ""

#: home/routes.py:1152
msgid "Group invention denied."
msgstr ""

#: home/routes.py:1285
msgid "Groupname already exists"
msgstr ""

#: templates/base.html:18
msgid "19squared - Just more than 360"
msgstr ""

#: templates/base.html:60
msgid "Staging Environment"
msgstr ""

#: templates/base.html:65
msgid "Local Development"
msgstr ""

#: templates/base.html:88 templates/base.html:92
msgid "Home"
msgstr ""

#: templates/base.html:106 templates/base.html:113 templates/groups.html:456
#: templates/profil.html:195 templates/user_management.html:62
msgid "Groups"
msgstr ""

#: templates/base.html:127 templates/base.html:133
#: templates/user_management.html:44
msgid "User-Management"
msgstr ""

#: templates/base.html:153
msgid "Logout"
msgstr ""

#: templates/auth/login.html:195 templates/base.html:162
#: templates/base.html:166
msgid "Login"
msgstr ""

#: templates/auth/register.html:10 templates/auth/register.html:198
#: templates/base.html:175 templates/base.html:181
msgid "Register"
msgstr ""

#: templates/base.html:204
msgid "Impressum"
msgstr ""

#: templates/base.html:207
msgid "Privacy"
msgstr ""

#: templates/groups.html:38
msgid "Create new Group"
msgstr ""

#: templates/groups.html:42 templates/groups.html:86
msgid "Group Name"
msgstr ""

#: templates/groups.html:47 templates/groups.html:840 templates/profil.html:214
msgid "Role"
msgstr ""

#: templates/groups.html:49
msgid "(Users joining the group will receive the role)"
msgstr ""

#: templates/create/assets.js:391 templates/create/create_tour.html:60
#: templates/create/create_tour.html:84 templates/create/media_card.html:22
#: templates/create/mediapage.js:1488 templates/groups.html:66
#: templates/groups.html:98 templates/groups.html:131 templates/groups.html:246
#: templates/groups.html:439 templates/lib.html:37 templates/lib.html:66
#: templates/lib.html:97 templates/lib.html:125 templates/lib.html:158
#: templates/lib.html:183 templates/lib.html:344 templates/profil.html:24
#: templates/profil.html:56 templates/role_management.html:24
#: templates/role_management.html:58 templates/user_management.html:24
msgid "Cancel"
msgstr ""

#: templates/groups.html:69 templates/groups.html:254 templates/lib.html:69
#: templates/role_management.html:66
msgid "Create"
msgstr ""

#: templates/groups.html:81
msgid "Rename Group"
msgstr ""

#: templates/groups.html:106 templates/groups.html:864 templates/lib.html:100
#: templates/lib.html:566
msgid "Rename"
msgstr ""

#: templates/groups.html:117
msgid "Delete Group"
msgstr ""

#: templates/groups.html:120
msgid "Are you sure you want to delete this group?"
msgstr ""

#: templates/create/assets.js:392 templates/create/media_card.html:121
#: templates/create/mediapage.js:1489 templates/create/pic_card.html:96
#: templates/groups.html:139 templates/groups.html:874 templates/lib.html:133
#: templates/lib.html:608 templates/profil.html:32
#: templates/role_management.html:32 templates/user_management.html:32
msgid "Delete"
msgstr ""

#: templates/groups.html:152 templates/groups.html:887
msgid "Create User"
msgstr ""

#: templates/groups.html:160
msgid "<b>Choose</b> usernames yourself<br /> or <b>generate</b> them"
msgstr ""

#: templates/groups.html:179
msgid "Generate<br />Usernames"
msgstr ""

#: templates/groups.html:185 templates/groups.html:354
msgid "Usernames"
msgstr ""

#: templates/groups.html:186 templates/groups.html:355
msgctxt "Usernames"
msgid "one per line"
msgstr ""

#: templates/groups.html:197
msgid "Already existing users"
msgstr ""

#: templates/groups.html:212 templates/groups.html:404
msgid "Print"
msgstr ""

#: templates/groups.html:212 templates/groups.html:404
msgid "all tokens"
msgstr ""

#: templates/groups.html:228
msgid "as "
msgstr ""

#: templates/groups.html:228 templates/groups.html:420
#: templates/groups.html:508
msgid "QR"
msgstr ""

#: templates/groups.html:228 templates/groups.html:420
msgid "-Codes"
msgstr ""

#: templates/groups.html:267 templates/groups.html:893
msgid "Invite User"
msgstr ""

#: templates/groups.html:273
msgid "Invite Users via"
msgstr ""

#: templates/groups.html:285 templates/groups.html:334
#: templates/user_management.html:60
msgid "Email"
msgstr ""

#: templates/groups.html:290
msgid "send invite emails to join the group"
msgstr ""

#: templates/auth/register.html:143 templates/groups.html:303
#: templates/groups.html:507 templates/groups.html:668 templates/profil.html:85
msgid "Username"
msgstr ""

#: templates/groups.html:308
msgid "send invite to users to join the group"
msgstr ""

#: templates/groups.html:321
msgid "Invite Token"
msgstr ""

#: templates/groups.html:326
msgid "generates token which can users redeem"
msgstr ""

#: templates/groups.html:335
msgctxt "Email"
msgid "one per line"
msgstr ""

#: templates/groups.html:348
msgid "Invalid emails"
msgstr ""

#: templates/groups.html:368
msgid "Users not found"
msgstr ""

#: templates/groups.html:375
msgid "Users already in group"
msgstr ""

#: templates/groups.html:381
msgid "Number of Invite-Tokens"
msgstr ""

#: templates/groups.html:420
msgid "as"
msgstr ""

#: templates/groups.html:447 templates/groups.html:1006
#: templates/groups.html:1012
msgid "Invite"
msgstr ""

#: templates/groups.html:463
msgid "Create Group"
msgstr ""

#: templates/groups.html:467
msgid "This page displays an overview of the groups you are managing."
msgstr ""

#: templates/groups.html:509
msgid "Email / Invite-Token / Key"
msgstr ""

#: templates/groups.html:538
msgid "created by yourself"
msgstr ""

#: templates/groups.html:544
msgid "joined"
msgstr ""

#: templates/groups.html:550
msgid "invite declined"
msgstr ""

#: templates/groups.html:556
msgid "not joined yet"
msgstr ""

#: templates/groups.html:604
msgid "initial password of the account"
msgstr ""

#: templates/groups.html:659 templates/groups.html:801
msgid "Scan to login"
msgstr ""

#: templates/auth/login.html:155 templates/auth/register.html:168
#: templates/groups.html:673 templates/profil.html:111
msgid "Password"
msgstr ""

#: templates/groups.html:676 templates/groups.html:815
msgid "Scan to join"
msgstr ""

#: templates/groups.html:682 templates/lib.html:27
msgid "Token"
msgstr ""

#: templates/groups.html:832 templates/user_management.html:276
msgid "Search..."
msgstr ""

#: templates/groups.html:833
msgid "No users added yet"
msgstr ""

#: templates/groups.html:900
msgid "Print QR-Codes for easy sharing:"
msgstr ""

#: templates/groups.html:1018
msgid "Generate"
msgstr ""

#: templates/groups.html:1075
msgid "Number of usernames must be between 1 and 50"
msgstr ""

#: templates/create/share.js:380 templates/groups.html:1190
msgid "Copied!"
msgstr ""

#: templates/index.html:28
msgid "Just more than 360"
msgstr ""

#: templates/index.html:31
msgid ""
"A web application for creating and experiencing interactive 360° tours in"
" extended reality environments."
msgstr ""

#: templates/index.html:35
msgid "View Demo Tour"
msgstr ""

#: templates/index.html:41
msgid "You are currently not logged in. Please"
msgstr ""

#: templates/index.html:42
msgid "register"
msgstr ""

#: templates/index.html:43
msgid "or"
msgstr ""

#: templates/index.html:44
msgid "login"
msgstr ""

#: templates/index.html:45
msgid "with your existing"
msgstr ""

#: templates/index.html:46
msgid "credentials."
msgstr ""

#: templates/lib.html:14 templates/lib.html:377
msgid "Join Group"
msgstr ""

#: templates/lib.html:18
msgid "You should receive the token from your teacher,"
msgstr ""

#: templates/lib.html:19
msgid "otherwise please contact"
msgstr ""

#: templates/lib.html:23
msgid "the admin"
msgstr ""

#: templates/lib.html:40
msgid "Join"
msgstr ""

#: templates/lib.html:52 templates/lib.html:393
msgid "Create New Tour"
msgstr ""

#: templates/lib.html:56 templates/lib.html:86
msgid "Project Name"
msgstr ""

#: templates/lib.html:82
msgid "Rename Tour"
msgstr ""

#: templates/lib.html:113
msgid "Delete Project"
msgstr ""

#: templates/lib.html:116
msgid "Are you sure you want to delete this tour?"
msgstr ""

#: templates/lib.html:146 templates/lib.html:630
msgid "Set as Demo"
msgstr ""

#: templates/lib.html:149
msgid ""
"Are you sure you want to change the public demo tour of the home page to "
"this tour?"
msgstr ""

#: templates/create/create_tour.html:91 templates/lib.html:161
#: templates/lib.html:190
msgid "Confirm"
msgstr ""

#: templates/create/create_tour.html:109 templates/lib.html:209
msgid "Share Tour"
msgstr ""

#: templates/create/create_tour.html:120 templates/lib.html:220
msgid "Scan QR-Code or share link to view tour."
msgstr ""

#: templates/create/create_tour.html:138 templates/create/share.js:388
#: templates/lib.html:238
msgid "Copy to clipboard"
msgstr ""

#: templates/lib.html:252
msgid "Download Project"
msgstr ""

#: templates/lib.html:262
msgid "<li>Click on <i>Open Full Tour</i></li>"
msgstr ""

#: templates/lib.html:264
msgid "Download Tour via Browser"
msgstr ""

#: templates/lib.html:267
msgid "Right-click anywhere on the page or press"
msgstr ""

#: templates/lib.html:270
msgid "<li>Select <i>Save (Page) As...</i> from the context menu</li>"
msgstr ""

#: templates/lib.html:271
msgid "<li>Choose a destination folder and filename</li>"
msgstr ""

#: templates/lib.html:272
msgid "<li>Click <i>Save</i></li>"
msgstr ""

#: templates/lib.html:276
msgid "Add"
msgstr ""

#: templates/lib.html:278
msgid "directly after"
msgstr ""

#: templates/lib.html:280
msgid "in the saved HTML file."
msgstr ""

#: templates/lib.html:283
msgid "Deploy Downloaded Tour"
msgstr ""

#: templates/lib.html:287
msgid ""
"Install Node.js and run the following command in the same folder as the "
"HTML is stored"
msgstr ""

#: templates/lib.html:296
msgid ""
"Install Python and run the following command in the same folder as the "
"HTML is stored"
msgstr ""

#: templates/lib.html:303
msgid "Server: "
msgstr ""

#: templates/lib.html:304
msgid ""
"Deploy the HTML file and the asset folder on your server and allow users "
"to access the HTML file."
msgstr ""

#: templates/lib.html:309
msgid "Do not open the file directly in a browser using the"
msgstr ""

#: templates/lib.html:311
msgid "protocol since it is not supported due to security issues."
msgstr ""

#: templates/lib.html:319
msgid "Open Full Tour"
msgstr ""

#: templates/lib.html:332
msgid "Remove Tour"
msgstr ""

#: templates/lib.html:335
msgid "Are you sure you want to remove this shared tour?"
msgstr ""

#: templates/lib.html:351 templates/lib.html:594 templates/profil.html:64
msgid "Remove"
msgstr ""

#: templates/lib.html:361
msgid "My Projects"
msgstr ""

#: templates/lib.html:368
msgid "Manage Groups"
msgstr ""

#: templates/lib.html:382
msgid "This page displays an overview of your projects"
msgstr ""

#: templates/lib.html:398
msgid "Import Tour"
msgstr ""

#: templates/lib.html:403
msgid "Add Demo Tour"
msgstr ""

#: templates/lib.html:411
msgid "You were invited to the group"
msgstr ""

#: templates/lib.html:421
msgid "Accept"
msgstr ""

#: templates/lib.html:430
msgid "Deny"
msgstr ""

#: templates/create/share.html:580 templates/lib.html:528
msgid "Share view"
msgstr ""

#: templates/lib.html:541
msgid "Export"
msgstr ""

#: templates/lib.html:552
msgid "Download"
msgstr ""

#: templates/lib.html:579
msgid "Duplicate"
msgstr ""

#: templates/lib.html:619
msgid "Defined as<br>Public Demo"
msgstr ""

#: templates/lib.html:671 templates/lib.html:692
msgid "View"
msgstr ""

#: templates/lib.html:678
msgid "Edit"
msgstr ""

#: templates/lib.html:703
msgid "Your groups"
msgstr ""

#: templates/lib.html:841
msgid ""
"WARNING: Your assets are being exported but you have to make sure to "
"upload them with the same file name after importing the tour."
msgstr ""

#: templates/profil.html:7 templates/profil.html:275
msgid "Delete Account"
msgstr ""

#: templates/profil.html:10
msgid "Are you sure you want to delete your account?"
msgstr ""

#: templates/profil.html:12
msgid "All tours and all groups that you has created will also be deleted!"
msgstr ""

#: templates/profil.html:42 templates/profil.html:221
msgid "Leave Group"
msgstr ""

#: templates/profil.html:45
msgid "Are you sure you want to leave this group?"
msgstr ""

#: templates/profil.html:73
msgid "Profil"
msgstr ""

#: templates/auth/register.html:156 templates/profil.html:99
msgid "E-Mail"
msgstr ""

#: templates/profil.html:126
msgid "Reset"
msgstr ""

#: templates/profil.html:133
msgid "Language"
msgstr ""

#: templates/profil.html:141
msgid "English"
msgstr ""

#: templates/profil.html:141
msgid "German"
msgstr ""

#: templates/profil.html:150
msgid "Roles (Permissions)"
msgstr ""

#: templates/profil.html:242
msgid "Creator"
msgstr ""

#: templates/profil.html:255 templates/role_management.html:228
#: templates/user_management.html:64
msgid "Quota"
msgstr ""

#: templates/profil.html:261
msgid ""
"Your quota has been customized to deviate from the standard quota "
"assigned to your role."
msgstr ""

#: templates/profil.html:268
msgid "Account"
msgstr ""

#: templates/role_management.html:7
msgid "Delete Role"
msgstr ""

#: templates/role_management.html:10
msgid "Are you sure you want to delete this role?"
msgstr ""

#: templates/role_management.html:12
msgid "Many users could lose their rights!"
msgstr ""

#: templates/role_management.html:43
msgid "Create New Role"
msgstr ""

#: templates/role_management.html:47
msgid "Role Name"
msgstr ""

#: templates/role_management.html:77 templates/user_management.html:50
msgid "Role-Management"
msgstr ""

#: templates/role_management.html:84
msgid "Create Role"
msgstr ""

#: templates/role_management.html:92
msgid "Permission / Role"
msgstr ""

#: templates/role_management.html:152
msgid "Allow to add role to a group"
msgstr ""

#: templates/role_management.html:156
msgid ""
"Attention! Any user with the permission to create a group can assign "
"these roles!"
msgstr ""

#: templates/role_management.html:190
msgid "Assign to new user"
msgstr ""

#: templates/role_management.html:194
msgid "Every newly registered user gets these roles!"
msgstr ""

#: templates/role_management.html:232
msgid "Users receive the highest quota assigned to any of their roles"
msgstr ""

#: templates/user_management.html:7
msgid "Delete User"
msgstr ""

#: templates/user_management.html:10
msgid "Are you sure you want to delete this user?"
msgstr ""

#: templates/user_management.html:12
msgid "All tours and all groups that this user has created will also be deleted!"
msgstr ""

#: templates/user_management.html:58
msgid "ID"
msgstr ""

#: templates/create/mediapage.js:769 templates/create/mediapage.js:776
#: templates/user_management.html:59
msgid "Name"
msgstr ""

#: templates/user_management.html:61
msgid "Roles"
msgstr ""

#: templates/user_management.html:63
msgid "Tours"
msgstr ""

#: templates/user_management.html:65
msgid "Last Login"
msgstr ""

#: templates/user_management.html:177
msgid ""
"The user quota has been updated successfully. Please note that if the "
"user's current asset usage exceeds their new quota, no existing assets "
"will be deleted. However, the user will be restricted from uploading new "
"assets until their usage falls below the quota. The user's quota can be "
"reset to the default value for their role by clearing the input field."
msgstr ""

#: templates/user_management.html:347
msgid "Adjust column count to screen width"
msgstr ""

#: templates/auth/login.html:55 templates/auth/login.html:57
#: templates/auth/register.html:55 templates/auth/register.html:57
msgid "Continue with Google"
msgstr ""

#: templates/auth/login.html:107 templates/auth/login.html:109
#: templates/auth/register.html:107 templates/auth/register.html:109
msgid "Continue with GitLab"
msgstr ""

#: templates/auth/login.html:134 templates/auth/login.html:136
#: templates/auth/register.html:134 templates/auth/register.html:136
msgid "Continue with GitHub"
msgstr ""

#: templates/auth/login.html:143
msgid "Username or Email"
msgstr ""

#: templates/auth/login.html:167
msgid "Need help accessing your account?"
msgstr ""

#: templates/auth/login.html:178
msgid "Remember Me"
msgstr ""

#: templates/auth/login.html:184
msgid "Anonymise Learning Analytics Data"
msgstr ""

#: templates/auth/login.html:189
msgid ""
"We use Learning Analytics for research purposes. Check this box if you "
"prefer your data to be collected anonymously, rather than being linked to"
" your username and email."
msgstr ""

#: templates/auth/register.html:156
msgid "(optional)"
msgstr ""

#: templates/auth/register.html:179
msgid "Confirm Password"
msgstr ""

#: templates/auth/register.html:190
msgid "We recommend the usage of"
msgstr ""

#: templates/auth/register.html:194
msgid "secure passwords"
msgstr ""

#: templates/auth/register.html:201
msgid "Registration Requirements"
msgstr ""

#: templates/auth/register.html:205
msgid "The username is at least four characters long."
msgstr ""

#: templates/auth/register.html:210
msgid "The email address is valid or empty."
msgstr ""

#: templates/auth/register.html:215
msgid "The password is at least six characters long and matches the confirmation."
msgstr ""

#: templates/auth/reset.html:4
msgid "Reset Password"
msgstr ""

#: templates/auth/reset.html:8
msgid "New Password"
msgstr ""

#: templates/auth/reset.html:13
msgid "Change Password"
msgstr ""

#: templates/auth/reset.html:19
msgid ""
"Your password has been reset successfully. Please login to your account "
"using your new password."
msgstr ""

#: templates/create/assets.js:46
msgid "Drop here to upload and add as hotspot"
msgstr ""

#: templates/create/assets.js:56
msgid "Drop here to add as hotspot"
msgstr ""

#: templates/create/assets.js:69
msgid "Drop here to upload and add as new scene"
msgstr ""

#: templates/create/assets.js:74
msgid "Drop here to add as new scene"
msgstr ""

#: templates/create/assets.js:124 templates/create/assets.js:187
msgid "successfully uploaded!"
msgstr ""

#: templates/create/assets.js:320 templates/create/assets.js:455
#: templates/create/media_card.html:10 templates/create/media_card.html:78
#: templates/create/mediapage.js:86 templates/create/mediapage.js:1383
#: templates/create/pic_card.html:64
msgid "Notes"
msgstr ""

#: templates/create/assets.js:384 templates/create/mediapage.js:1481
msgid "Confirm Delete"
msgstr ""

#: templates/create/assets.js:388
msgid "Are you sure you want to delete this file?"
msgstr ""

#: templates/create/create_aframe.js:93
msgid "Please select a media page first."
msgstr ""

#: templates/create/create_tour.html:37
msgid "Edit Name"
msgstr ""

#: templates/create/create_tour.html:63 templates/create/media_card.html:25
msgid "Save"
msgstr ""

#: templates/create/create_tour.html:161
msgid "Uploading..."
msgstr ""

#: templates/create/create_tour.html:174 templates/create/create_tour.html:262
#: templates/create/create_tour.html:281 templates/create/create_tour.html:355
msgid "Drop here to upload"
msgstr ""

#: templates/create/create_tour.html:199
msgid "Upload or Drag&Drop"
msgstr ""

#: templates/create/create_tour.html:207
msgid "Record Audio"
msgstr ""

#: templates/create/create_tour.html:211
msgid "Recording.."
msgstr ""

#: templates/create/create_tour.html:289
msgid "Begin by uploading your media on the left"
msgstr ""

#: templates/create/create_tour.html:295
msgid "Add your uploaded file as scene by clicking on the +"
msgstr ""

#: templates/create/create_tour.html:380
msgid "Add a hotspot by drag&drop from the bottom"
msgstr ""

#: templates/create/create_tour.html:386
msgid "Select hotspot to edit properties"
msgstr ""

#: templates/create/create_tour.html:395
msgid "Show Tour"
msgstr ""

#: templates/create/hotspots.html:17
msgid "Text"
msgstr ""

#: templates/create/hotspots.html:36
msgid "Image"
msgstr ""

#: templates/create/hotspots.html:60
msgid "Move"
msgstr ""

#: templates/create/hotspots.html:79
msgid "Audio"
msgstr ""

#: templates/create/hotspots.html:98
msgid "Video"
msgstr ""

#: templates/create/hotspots.html:117
msgid "Quiz"
msgstr ""

#: templates/create/hotspots.html:137
msgid "Combination"
msgstr ""

#: templates/create/hotspots.html:164 templates/create/hotspots.html:173
msgid "there is a note!"
msgstr ""

#: templates/create/hotspots.html:184
msgid "Tour-Notes"
msgstr ""

#: templates/create/hotspots.html:209
msgid "users viewing"
msgstr ""

#: templates/create/hotspots.html:217
msgid "users editing"
msgstr ""

#: templates/create/hotspots.html:226
msgid "Tour-Collaborator"
msgstr ""

#: templates/create/hotspots.html:254
msgid "new messages"
msgstr ""

#: templates/create/hotspots.html:263
msgid "Chat"
msgstr ""

#: templates/create/hotspots.html:275
msgid "Enter your message here"
msgstr ""

#: templates/create/media_card.html:68
msgid "Card title"
msgstr ""

#: templates/create/media_card.html:89
msgid "This is setted as Start Scene"
msgstr ""

#: templates/create/media_card.html:99
msgid "Set as Start Scene"
msgstr ""

#: templates/create/media_card.html:111 templates/create/pic_card.html:86
msgid "Edit name"
msgstr ""

#: templates/create/mediapage.js:113
msgid "This is set as Start Scene"
msgstr ""

#: templates/create/mediapage.js:134
msgid "to"
msgstr ""

#: templates/create/mediapage.js:136
msgid "Linked Scene is Empty"
msgstr ""

#: templates/create/mediapage.js:140
msgid "Hotspot is Empty"
msgstr ""

#: templates/create/mediapage.js:155
msgid "${hotspot.media_type_id}"
msgstr ""

#: templates/create/mediapage.js:155
msgid "is Empty"
msgstr ""

#: templates/create/mediapage.js:169
msgid "Question is Empty"
msgstr ""

#: templates/create/mediapage.js:794
msgid "Empty"
msgstr ""

#: templates/create/mediapage.js:798
msgid "Linked Scene"
msgstr ""

#: templates/create/mediapage.js:802
msgid "Set the scene to move to"
msgstr ""

#: templates/create/mediapage.js:843
msgid "Set Image"
msgstr ""

#: templates/create/mediapage.js:860
msgid "Hotspot Text"
msgstr ""

#: templates/create/mediapage.js:876
msgid "Set Video"
msgstr ""

#: templates/create/mediapage.js:890
msgid "Set Audio"
msgstr ""

#: templates/create/mediapage.js:897 templates/create/mediapage.js:907
msgid "Question"
msgstr ""

#: templates/create/mediapage.js:918
msgid "Add Answer"
msgstr ""

#: templates/create/mediapage.js:926
msgid "Answer"
msgstr ""

#: templates/create/mediapage.js:992
msgid "Set Quiz"
msgstr ""

#: templates/create/mediapage.js:997 templates/create/mediapage.js:1007
msgid "Instruction"
msgstr ""

#: templates/create/mediapage.js:1079
msgid "Set Combination"
msgstr ""

#: templates/create/mediapage.js:1084
msgid "Rotation"
msgstr ""

#: templates/create/mediapage.js:1119 templates/create/mediapage.js:1203
msgid "Dependencies"
msgstr ""

#: templates/create/mediapage.js:1122 templates/create/mediapage.js:1207
msgid ""
"The hotspot is only shown when already interacted with the selected "
"dependencies"
msgstr ""

#: templates/create/mediapage.js:1130
msgid "Search for Hotspot"
msgstr ""

#: templates/create/mediapage.js:1193
msgid "Locked Hotspot Visible"
msgstr ""

#: templates/create/mediapage.js:1241
msgid "Set Icon"
msgstr ""

#: templates/create/mediapage.js:1272
msgid "Default"
msgstr ""

#: templates/create/mediapage.js:1485
msgid "Are you sure you want to delete this scene?"
msgstr ""

#: templates/create/pic_card.html:75
msgid "Download Asset"
msgstr ""

#: templates/create/share.html:7 templates/create/share.html:52
msgid "owner"
msgstr ""

#: templates/create/share.html:19 templates/create/share.html:63
#: templates/create/share.html:118 templates/create/share.html:186
#: templates/create/share.html:279 templates/create/share.html:378
#: templates/create/share.html:430
msgid "viewing"
msgstr ""

#: templates/create/share.html:29 templates/create/share.html:72
#: templates/create/share.html:127 templates/create/share.html:195
#: templates/create/share.html:288 templates/create/share.html:387
msgid "editing"
msgstr ""

#: templates/create/share.html:92 templates/create/share.html:228
#: templates/create/share.html:470 templates/create/share.html:523
#: templates/create/share.js:8 templates/create/share.js:161
msgid "change to<br />view permission"
msgstr ""

#: templates/create/share.html:104 templates/create/share.html:240
#: templates/create/share.html:339
msgid "edit permission"
msgstr ""

#: templates/create/share.html:139 templates/create/share.html:208
msgid "remove<br />collaborator"
msgstr ""

#: templates/create/share.html:160 templates/create/share.html:327
#: templates/create/share.js:9 templates/create/share.js:162
msgid "change to<br />edit permission"
msgstr ""

#: templates/create/share.html:172
msgid "view permission"
msgstr ""

#: templates/create/share.html:258 templates/create/share.html:357
msgid "This is a group"
msgstr ""

#: templates/create/share.html:262 templates/create/share.html:361
msgid "toggle members"
msgstr ""

#: templates/create/share.html:305 templates/create/share.html:404
msgid "remove<br />group"
msgstr ""

#: templates/create/share.html:458
msgid "Share with"
msgstr ""

#: templates/create/share.html:484
msgid "user"
msgstr ""

#: templates/create/share.html:503 templates/create/share.js:240
msgid "enter a username first"
msgstr ""

#: templates/create/share.html:537
msgid "group"
msgstr ""

#: templates/create/share.html:557 templates/create/share.js:268
msgid "enter one of your groups first"
msgstr ""

#: templates/create/share.js:22 templates/create/share.js:175
msgid "Collaborator permission"
msgstr ""

#: templates/create/share.js:25 templates/create/share.js:178
msgid "Change permission to"
msgstr ""

#: templates/create/share.js:26 templates/create/share.js:179
msgid "edit"
msgstr ""

#: templates/create/share.js:26 templates/create/share.js:179
msgid "view"
msgstr ""

#: templates/create/share.js:70
msgid "Add Collaborator"
msgstr ""

#: templates/create/share.js:73 templates/create/share.js:279
msgid "Add "
msgstr ""

#: templates/create/share.js:75 templates/create/share.js:281
msgid "with"
msgstr ""

#: templates/create/share.js:77 templates/create/share.js:283
msgid "permission to<br /><b>edit</b>"
msgstr ""

#: templates/create/share.js:78 templates/create/share.js:284
msgid "permission to<br />(only) <b>view</b>"
msgstr ""

#: templates/create/share.js:79 templates/create/share.js:285
msgid "the tour?"
msgstr ""

#: templates/create/share.js:99
msgid "User already added as collaborator."
msgstr ""

#: templates/create/share.js:101
msgid "User not found."
msgstr ""

#: templates/create/share.js:103
msgid "An unexpected error occurred."
msgstr ""

#: templates/create/share.js:120 templates/create/share.js:323
msgid "Remove Collaborator"
msgstr ""

#: templates/create/share.js:123 templates/create/share.js:326
msgid "remove "
msgstr ""

#: templates/create/share.js:125 templates/create/share.js:328
msgid "from this project?"
msgstr ""

#: templates/create/share.js:226 templates/create/share.js:253
msgid "add group"
msgstr ""

#: templates/create/share.js:277
msgid "Add Group"
msgstr ""

#: templates/create/share.js:306
msgid "Group not found or already added as collaborator."
msgstr ""

#: templates/create/share.js:423
msgid "Guest"
msgstr ""

#: templates/create/share.js:423
msgid "Guests"
msgstr ""

