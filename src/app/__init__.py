# Import flask and template operators
from app.models import User, Role, Asset, Mediapage, Tour, Hotspot, db
import os
import time
import json
from flask import Flask, render_template, current_app, url_for, redirect, request
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_security import Security, SQLAlchemyUserDatastore, RoleMixin, current_user
from flask_babel import Babel, gettext as _
from flask_uploads import UploadSet, IMAGES, configure_uploads
from flask_socketio import SocketIO, join_room, leave_room, close_room, send, emit
from sqlalchemy.exc import OperationalError, InterfaceError, PendingRollbackError
from datetime import datetime
from werkzeug.exceptions import HTTPException
from VERSION import VERSION

from dotenv import load_dotenv
load_dotenv()

allPermissions = {
    _("create_tour"): _("Allows creating a new tour"),
    _("edit_tour"): _("Allows editing an existing tour"),
    _("rename_tour"): _("Allows renaming a tour"),
    _("copy_tour"): _("Allows duplicating a tour"),
    _("delete_tour"): _("Allows deleting a tour"),
    _("share_tour"): _("Allows sharing a tour via sharelink with view-only"),
    _("export_tour"): _("Allows exporting a tour to an external format"),
    _("import_tour"): _("Allows importing a tour from an external format"),
    _("add_demo_tour"): _("Allows adding a copy of the demo tour to the own tours"),
    _("edit_collaborator"): _("Allows editing collaborators on a project"),
    _("create_group"): _("Allows creating new groups"),
    _("edit_group"): _("Allows modifying group details"),
    _("delete_group"): _("Allows deleting groups"),
    _("edit_roles"): _("Allows modifying roles of an user"),
    _("set_demotour"): _("Allows to set a tour as the demo tour on the public start page"),
    _("user_management"): _("Allows managing user accounts"),
    _("role_management"): _("Allows managing roles and permissions"),
    _("delete_user"): _("Allows delete his own account")
}

# Define the WSGI application object
app = Flask(__name__)
# Talisman(app)

# Configurations
app.config.from_object("config.Config")
app.config.from_object(f'config.{app.config["ENVIRONMENT"]}Config')
app.secret_key = os.getenv('APP_RANDOM_KEY')
# Initialize the database object with the app
db.init_app(app)

# Initialize the Babel object with the app


def get_locale():
    if not request.accept_languages.best_match(app.config["LANGUAGES"]):
        return "en"
    if current_user.is_authenticated:
        return current_user.language
    cookie_language = request.cookies.get('language')
    if cookie_language:
        return cookie_language
    else:
        return request.accept_languages.best_match(app.config["LANGUAGES"])


babel = Babel(app)
babel.init_app(
    app, default_locale=app.config["DEFAULT_LANGUAGE"], locale_selector=get_locale)

# Initialize the socketio object with the app
if os.getenv('APP_TEST') or (app.config["ENVIRONMENT"] == "Development" and not (os.getenv('APP_DOCKER'))):
    socketio = SocketIO(app, async_mode="threading",
                        cors_allowed_origins="*")
else:
    socketio = SocketIO(app, async_mode="gevent_uwsgi",
                        cors_allowed_origins="*")

print(f"SocketIO async mode is: {socketio.async_mode}")
socketio.init_app(app)

# Initialize Flask-Migrate with the app and the database
migrate = Migrate()
migrate.init_app(app, db)

# create the database tablesimport_tour()
with app.app_context():
    db.create_all()

# Configure uploads
uploaded_images = UploadSet('images', app.config['ALLOWED_EXTENSIONS'])
app.config['UPLOADED_IMAGES_DEST'] = os.path.join(
    app.root_path, 'Uploaded_Media')
app.config['UPLOADED_THUMBNAIL_DEST'] = os.path.join(
    app.config['UPLOADED_IMAGES_DEST'], 'thumbnails')
configure_uploads(app, uploaded_images)


# lib

users = {}
usersView = {}


@socketio.on('joinLib')
def on_joinLib(data):
    join_room("lib")
    join_room("user-"+str(data['user_id']))
    for tour in data['tours']:
        join_room("lib-"+str(tour))
        users.setdefault(str(tour), [])
        usersView.setdefault(str(tour), [])
        emit('updateOnlineUsers', {'tour_id': str(tour), 'users': users[str(
            tour)], 'usersView': usersView[str(tour)]}, room="lib-"+str(tour))
    for group in data['groups']:
        join_room("libGroup-"+str(group))


@socketio.on('createTour')
def on_createTour(data):
    join_room("lib-"+str(data['tour_id']))
    emit('createTour', data, room="user-"+str(data['user_id']))


@socketio.on('renameTour')
def on_renameTour(data):
    emit('renameTour', data, room="lib-"+str(data['tour_id']))


@socketio.on('deleteTour')
def on_deleteTour(data):
    emit('deleteTour', data, room="lib-"+str(data['tour_id']))
    close_room("lib-"+str(data['tour_id']))
    close_room(str(data['tour_id']))


@socketio.on('removeTour')
def on_removeTour(data):
    emit('removeTour', data, room="lib-"+str(data['tour_id']))
    leave_room("lib-"+str(data['tour_id']))


@socketio.on('addCollaborator')
def on_addCollaborator(data):
    # emit to new user
    emit('addCollaborator', data, room="user-"+str(data['collaborator_id']))
    emit('addCollaborator', data, room="lib-"+str(data['tour_id']))


@socketio.on('removeCollaborator')
def on_removeCollaborator(data):
    emit('removeCollaborator', data, room="lib-"+str(data['tour_id']))


@socketio.on('addGroup')
def on_addGroup(data):
    # emit to new group
    emit('addGroup', data, room="libGroup-"+str(data['group_id']))
    emit('addGroup', data, room="lib-"+str(data['tour_id']))


@socketio.on('removeGroup')
def on_removeGroup(data):
    emit('removeGroup', data, room="lib-"+str(data['tour_id']))
    leave_room("lib-"+str(data['tour_id']))


@socketio.on('editGroupRights')
def on_editGroupRights(data):
    emit('editGroupRights', data, room="lib-"+str(data['tour_id']))


@socketio.on('editCollaboratorRights')
def on_editCollaboratorRights(data):
    emit('editCollaboratorRights', data, room="lib-"+str(data['tour_id']))


# create tour


@socketio.on('joinCreateTour')
def on_joinCreateTour(data):
    join_room(str(data['tour_id']))
    users.setdefault(str(data['tour_id']), []).append(
        {'username': data['username'], 'sid': request.sid})
    usersView.setdefault(str(data['tour_id']), [])
    emit('updateOnlineUsers', {'tour_id': str(data['tour_id']), 'users': users[str(data['tour_id'])], 'usersView': usersView[str(
        data['tour_id'])]}, room=str(data['tour_id']))
    emit('updateOnlineUsers', {'tour_id': str(data['tour_id']), 'users': users[str(data['tour_id'])], 'usersView': usersView[str(
        data['tour_id'])]}, room="lib-"+str(data['tour_id']))


@socketio.on('disconnect')
def on_disconnect():
    for tour, user_list in users.items():
        for user in user_list:
            if user['sid'] == request.sid:
                user_list.remove(user)
                emit('updateOnlineUsers', {'tour_id': str(tour),
                     'users': users[str(tour)], 'usersView': usersView[str(tour)]}, room=str(tour))
                emit('updateOnlineUsers', {'tour_id': str(tour),
                     'users': users[str(tour)], 'usersView': usersView[str(tour)]}, room="lib-"+str(tour))

    for tour, user_list in usersView.items():
        for user in user_list:
            if user['sid'] == request.sid:
                user_list.remove(user)
                emit('updateOnlineUsers', {'tour_id': str(tour),
                     'users': users[str(tour)], 'usersView': usersView[str(tour)]}, room=str(tour))
                emit('updateOnlineUsers', {'tour_id': str(tour),
                     'users': users[str(tour)], 'usersView': usersView[str(tour)]}, room="lib-"+str(tour))


@socketio.on('hotspot')
def on_hotspot(data):
    emit('hotspot', data['hotspot_id'], room=data['tour'], include_self=False)
    emit('hotspot', data['hotspot_id'], room="view-"+data['tour'])


@socketio.on('removeHotspot')
def on_removeHotspot(data):
    emit('removeHotspot', data['hotspot_id'],
         room=data['tour'])
    emit('removeHotspot', data['hotspot_id'],
         room="view-"+data['tour'])


@socketio.on('updateMediaPages')
def on_updateMediaPages(data):
    emit('updateMediaPages', room=data['tour'], include_self=False)
    emit('updateMediaPages', room="view-"+data['tour'])


@socketio.on('updateFileList')
def on_updateFileList(data):
    emit('updateFileList', room=data['tour'])


@socketio.on('removeAsset')
def on_removeAsset(data):
    emit('removeAsset', data['id'], room=data['tour'], include_self=False)


@socketio.on('joinViewTour')
def on_joinViewTour(data):
    join_room("view-" + str(data['tour_id']))
    users.setdefault(str(data['tour_id']), [])
    usersView.setdefault(str(data['tour_id']), []).append(
        {'username': data['username'], 'sid': request.sid})
    emit('updateOnlineUsers', {'tour_id': str(data['tour_id']), 'users': users[str(data['tour_id'])], 'usersView': usersView[str(
        data['tour_id'])]}, room=str(data['tour_id']))
    emit('updateOnlineUsers', {'tour_id': str(data['tour_id']), 'users': users[str(data['tour_id'])], 'usersView': usersView[str(
        data['tour_id'])]}, room="lib-"+str(data['tour_id']))


@socketio.on('updateTourAnnotation')
def on_updateTourAnnotation(data):
    emit('updateTourAnnotation', data['annotation'],
         room=data['tour_id'], include_self=False)


@socketio.on('text')
def text(data):
    new_chat = Chat(user_id=data['user_id'],
                    tour_id=data['tour_id'], message=data['msg'])
    db.session.add(new_chat)
    db.session.commit()

    user = User.query.get(data['user_id'])
    username = user.username if user else "Unknown"

    emit('message', {
        'msg': "[" + str(new_chat.created) + "] " + username + ":\n" + data['msg'],
    }, to=data['tour_id'])


# error handling
MAX_RETRIES = 3
RETRY_DELAY = 2


@app.errorhandler(Exception)
def handle_error(e):
    print(e)

    if isinstance(e, (OperationalError, InterfaceError, PendingRollbackError)):
        for attempt in range(MAX_RETRIES):
            try:
                db.session.rollback()
                db.session.execute(db.text("SELECT 1"))
                return redirect(url_for('auth.login'))
            except (OperationalError, InterfaceError, PendingRollbackError) as retry_exc:
                if attempt < MAX_RETRIES - 1:
                    print(
                        f"MariaDB connection error: {retry_exc}. Retrying in {RETRY_DELAY} seconds...")
                    time.sleep(RETRY_DELAY)
                else:
                    code = 503
                    message = "Database connection failed. Please reload the page."
    elif isinstance(e, HTTPException):
        code = e.code
        message = e.description
    else:
        code = 500
        message = str(e) or e.__class__.__name__ or "Internal Server Error"

    return (
        render_template(
            "error.html",
            status_code=code,
            status_message=message,
            currentYear=datetime.now().year,
            VERSION=VERSION,
        ),
        code,
    )


@app.teardown_appcontext
def shutdown_session(exception=None):
    db.session.remove()


# load users, roles for a session
user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore, register_blueprint=False)

# autopep8: off
# Import a module / component using its blueprint handler variable (mod_auth)
from app.base.routes import base_bp as base_module
from app.home.routes import home_bp as home_module
from app.auth.routes import auth_bp as auth_module
from app.models import Chat
# autopep8: on

# DEBUG


@app.context_processor
def utility_functions():
    def print_in_console(message):
        print(str(message))

    return dict(mdebug=print_in_console)


# Register blueprint(s)
app.register_blueprint(base_module)
app.register_blueprint(home_module, url_prefix='/home')
app.register_blueprint(auth_module)
# app.register_blueprint(xyz_module)


def extract_data(data_string, key):
    start = data_string.find(key) + len(key) + 3
    end = data_string[start:].find(",") + start

    if start > end:
        end = len(data_string) - 1
    else:
        n = 1
        if data_string[start-len(key)-4:start-5] in ["_", "V"] or data_string[start-len(key)-10:start-17] in ["linked"]:
            if data_string[0:1] in ["_", "V"]:
                return extract_data(data_string[2:], key)
            elif data_string[0:1] in ["l"]:
                return extract_data(data_string[22:], key)
            else:
                return extract_data(data_string[1:], key)
        while data_string[end:end+3] != ", '":
            end = data_string[start+n:].find(",") + start + n
            n += 1

    try:
        return (int(data_string[start:end]))
    except:
        if data_string[start:end].lower() == 'true' or data_string[start:end].lower() == 'false':
            return (bool(data_string[start:end]))
        elif data_string[start:end].lower() == 'none':
            return None
        else:
            try:
                return (float(data_string[start:end]))
            except:
                return (data_string[start+1:end-1])


with app.app_context():

    # Define Roles
    roles = {
        "admin": ["create_tour", "edit_tour", "rename_tour", "copy_tour", "delete_tour", "share_tour", "export_tour", "import_tour", "add_demo_tour", "edit_collaborator", "create_group", "edit_group", "delete_group", "edit_roles", "set_demotour", "user_management", "role_management", "delete_user"],
        "manager": ["create_tour", "edit_tour", "rename_tour", "copy_tour", "delete_tour", "share_tour", "export_tour", "import_tour", "add_demo_tour", "edit_collaborator", "create_group", "edit_group", "delete_group", "delete_user"],
        "user": ["create_tour", "edit_tour", "rename_tour", "copy_tour", "delete_tour", "share_tour", "export_tour", "import_tour", "add_demo_tour", "edit_collaborator", "delete_user"]
    }

    # Define Default Quotas in GiB
    quotas = {
        "admin": 100,
        "manager": 10,
        "user": 3
    }

    for role in roles:
        if not user_datastore.find_role(role):
            new_role = user_datastore.create_role(name=role)
            new_role.set_permissions(roles[role])
            new_role.default_quota_gb = quotas[role]
            if role == "user":
                new_role.allow_as_role_in_group = True
                new_role.assign_to_new_user = True
        else:
            existing_role = user_datastore.find_role(role)
            existing_role.set_permissions(roles[role])
            existing_role.default_quota_gb = quotas[role]
    db.session.commit()

    # Assign 'user' role to all existing users
    for user in User.query.all():
        user_datastore.add_role_to_user(user, 'user')
    db.session.commit()

    # Generate admin user and demo tour
    existingAdmin = User.query.filter_by(
        username=os.getenv('ADMIN_USERNAME')).first()
    if not existingAdmin:
        user = User(username=os.getenv('ADMIN_USERNAME'),
                    email=os.getenv('ADMIN_EMAIL'))
        password = os.getenv('ADMIN_PASSWORD')
        if user and password:
            user.set_password(password)
            user_datastore.add_role_to_user(user, 'admin')
            db.session.add(user)
            db.session.commit()
            if not Tour.query.filter_by(share_string="demo").first():
                json_file_path = os.path.join(os.path.join(
                    os.getcwd(), "app"), "static") + '/demo.360xr'
                with open(json_file_path, 'r', encoding='utf-8') as file:
                    data = json.load(file)

                tour = Tour(creator_id=1, name=data["tour_name"])
                tour.set_share_string("demo")
                db.session.add(tour)
                db.session.commit()

                mediapage_mapping = {}

                for asset in data["assets"]:
                    if asset["filename_id"]:
                        # Check if the file exists
                        if os.path.exists(os.path.join(current_app.config['UPLOADED_IMAGES_DEST'], asset["filename_id"])):
                            newAsset = Asset(filename_id=asset["filename_id"], name=asset["name"], format=asset["format"], created=datetime.utcnow(
                            ), tour_id=tour.id, media_type=asset["media_type"])
                            db.session.add(newAsset)
                        else:
                            print("File not found. Skipping asset..")

                for mediapage in data["mediapages"]:
                    newMediapage = Mediapage(asset_id=extract_data(mediapage, 'asset_id'), tour_id=tour.id, is_start_page=int(extract_data(
                        mediapage, 'is_start_page')), name=extract_data(mediapage, 'name'), media_type=extract_data(mediapage, 'media_type'))
                    db.session.add(newMediapage)

                    searchMediapage = Mediapage.query.filter_by(asset_id=extract_data(
                        mediapage, 'asset_id'), tour_id=tour.id).first()
                    mediapage_mapping[extract_data(
                        mediapage, 'id')] = searchMediapage.id
                    mediapage_mapping[None] = None

                for hotspot in data["hotspots"]:
                    try:
                        newHotspot = Hotspot(media_page_id=mediapage_mapping[extract_data(hotspot, 'media_page_id')], linked_media_page_id=mediapage_mapping[extract_data(hotspot, 'linked_media_page_id')], media_type_id=extract_data(hotspot, 'media_type_id'), presented_media_id=extract_data(hotspot, 'presented_media_id'), name=extract_data(hotspot, 'name'), desc=extract_data(
                            hotspot, 'desc'), position=extract_data(hotspot, 'position'), rotation=extract_data(hotspot, 'rotation'), size=extract_data(hotspot, 'size'), custom_display_icon_id=extract_data(hotspot, 'custom_display_icon_id'), starttime=extract_data(hotspot, 'starttime'), endtime=-1)
                        db.session.add(newHotspot)
                    except Exception:
                        print("Invalid hotspot data. Skipping hotspot..")

                db.session.commit()
    else:
        existingAdmin.set_password(os.getenv('ADMIN_PASSWORD'))
        user_datastore.add_role_to_user(existingAdmin, 'admin')
        db.session.commit()
