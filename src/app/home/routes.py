
import os
import time
import json
import math
from datetime import datetime
from VERSION import VERSION
from flask import request, redirect, url_for, render_template, flash, jsonify, current_app, send_file, g
from flask_babel import get_locale, gettext as _, pgettext as __
from flask_security import current_user, login_required, roles_required, permissions_required
from app import db, user_datastore

from werkzeug.utils import secure_filename
from moviepy.editor import VideoFileClip
from PIL import Image
from . import home_bp
from app.models import Asset, User, Tour, Mediapage, Hotspot, SharedTour, Group, GroupUsers, SharedGroupTour, Role, Chat, ChatUnread, TourAnnotationUnread, Dependencies, EscapeRoomAnswers, HotspotVisited
from app import db, uploaded_images, allPermissions
from sqlalchemy import and_, or_
from app.utils.xapi import lrs_event
from app.utils.mail import send_email

"""
Very Important: All routes from this file are sub-routes from /home
"""


@home_bp.before_request
def require_login():
    g.locale = str(get_locale())
    g.locales = os.getenv('LANGUAGES').split(",")
    """Ensure the user is logged in"""
    if request.path.startswith('/home') and not current_user.is_authenticated:
        return redirect(url_for('auth.login', username=request.args.get('username'), email=request.args.get('email',), password=request.args.get('password'), token=request.args.get('token')))


@home_bp.route('/', methods=['GET'], endpoint='home_page')
@login_required
def home_page():
    """
    Shows the User the library and his created Tours
    """
    user_tours = Tour.query.filter_by(creator_id=current_user.id).all()
    for tour in user_tours:
        tour.owner = User.query.get(tour.creator_id)
        tour.viewer = {}
        tour.editor = {}
        tour.viewerGroupMembers = {}
        tour.editorGroupMembers = {}
        # needed for own sharedTours
        if SharedTour.query.filter_by(tour_id=tour.id).first():
            tour.shared = True
            tour.sharedTour_id = tour.id
            tour.editor = {editor.id: User.query.filter_by(id=editor.collaborator_id).first().username
                           for editor in SharedTour.query.filter_by(tour_id=tour.id, edit="1").all()}
            tour.viewer = {viewer.id: User.query.filter_by(id=viewer.collaborator_id).first().username
                           for viewer in SharedTour.query.filter_by(tour_id=tour.id, edit="0").all()}

    # add shared tours to user tours
    shared_tours = SharedTour.query.filter_by(
        collaborator_id=current_user.id).all()
    for shared_tour in shared_tours:
        tour = Tour.query.get(shared_tour.tour_id)
        tour.shared = True
        tour.sharedTour_id = shared_tour.id
        tour.owner = User.query.get(tour.creator_id)
        tour.editor = {editor.id: User.query.filter_by(id=editor.collaborator_id).first().username
                       for editor in SharedTour.query.filter_by(tour_id=tour.id, edit="1").all()}
        tour.viewer = {viewer.id: User.query.filter_by(id=viewer.collaborator_id).first().username
                       for viewer in SharedTour.query.filter_by(tour_id=tour.id, edit="0").all()}
        tour.viewerGroup = {}
        tour.editorGroup = {}
        tour.viewerGroupMembers = {}
        tour.editorGroupMembers = {}
        if current_user.username in tour.editor.values():
            tour.currentUser_isEditor = True
        user_tours.append(tour)

    # Get all groups that the current user is associated with
    groups = {group.id: group.name for group in Group.query.join(
        GroupUsers, GroupUsers.group_id == Group.id).filter(GroupUsers.user_id == current_user.id, GroupUsers.accepted == True).all()}

    # Get all pending groups for the current user
    pending_groups = {
        group.id: {
            "name": group.name,
            "token": group_user.invite_token
        }
        for group, group_user in db.session.query(Group, GroupUsers)
        .filter(
            Group.id == GroupUsers.group_id,
            (GroupUsers.user_id == current_user.id) | (
                GroupUsers.invite_email == current_user.email if current_user.email else False),
            GroupUsers.accepted.is_(None)
        ).all()
    }

    # add shared group tours to user tours
    shared_group_tours = SharedGroupTour.query.join(
        GroupUsers, GroupUsers.group_id == SharedGroupTour.group_id
    ).filter(GroupUsers.user_id == current_user.id, GroupUsers.accepted == True).all()
    for shared_group_tour in shared_group_tours:
        tour = Tour.query.get(shared_group_tour.tour_id)
        tour.shared = True
        tour.sharedGroup = True
        tour.sharedTour_id = shared_group_tour.id
        tour.owner = User.query.get(tour.creator_id)
        tour.editorGroup = {editorGroup.id: Group.query.filter_by(id=editorGroup.group_id).first().name
                            for editorGroup in SharedGroupTour.query.filter_by(tour_id=tour.id, edit="1").all()}
        tour.viewerGroup = {viewerGroup.id: Group.query.filter_by(id=viewerGroup.group_id).first().name
                            for viewerGroup in SharedGroupTour.query.filter_by(tour_id=tour.id, edit="0").all()}
        tour.viewerGroupMembers = {viewerGroup.id: [user.username for user in User.query.join(GroupUsers, GroupUsers.user_id == User.id).filter(GroupUsers.group_id == viewerGroup.group_id, GroupUsers.accepted == True).all()]
                                   for viewerGroup in SharedGroupTour.query.filter_by(tour_id=tour.id, edit="0").all()}
        tour.editorGroupMembers = {editorGroup.id: [user.username for user in User.query.join(GroupUsers, GroupUsers.user_id == User.id).filter(GroupUsers.group_id == editorGroup.group_id, GroupUsers.accepted == True).all()]
                                   for editorGroup in SharedGroupTour.query.filter_by(tour_id=tour.id, edit="1").all()}
        for editorGroup in SharedGroupTour.query.filter_by(tour_id=tour.id, edit="1").all():
            if editorGroup.group_id in groups:
                tour.currentUser_isEditor = True
        # Merge shared group tours with existing user tours if they have the same tour ID
        existing_tour = next(
            (extour for extour in user_tours if extour.id == tour.id), None)
        if existing_tour:
            existing_tour.editorGroup = tour.editorGroup
            existing_tour.viewerGroup = tour.viewerGroup
        else:
            tour.editor = {editor.id: User.query.filter_by(id=editor.collaborator_id).first().username
                           for editor in SharedTour.query.filter_by(tour_id=tour.id, edit="1").all()}
            tour.viewer = {viewer.id: User.query.filter_by(id=viewer.collaborator_id).first().username
                           for viewer in SharedTour.query.filter_by(tour_id=tour.id, edit="0").all()}
            user_tours.append(tour)

    # Create a dictionary of tour IDs and their start pages
    tour_dict = {tour.id: tour.get_start_page()
                 for tour in user_tours if tour.get_start_page() is not None}

    # Get all users that the current user is associated with (same group)
    users = {user.id: user.username
             for user in User.query.join(GroupUsers, GroupUsers.user_id == User.id)
             .filter(GroupUsers.group_id.in_(
                 db.session.query(GroupUsers.group_id)
                 .filter_by(user_id=current_user.id)
             )).all()}

    # Get all users
    all_users = {user.id: user.username
                 for user in User.query.join(GroupUsers, GroupUsers.user_id == User.id).all()}

    contactEmail = os.getenv('CONTACT_EMAIL') if os.getenv(
        'CONTACT_EMAIL') else None
    lrs_event('/')
    return render_template("lib.html", tours=user_tours, groups=groups, pending_groups=pending_groups, tour_dict=tour_dict, users=users, all_user=all_users, contactEmail=contactEmail, currentYear=datetime.now().year, VERSION=VERSION)


@home_bp.route('/create', methods=['GET'], endpoint='create_page')
@login_required
def create_page():
    """
    This route returns the complexest HTML Template in this project
    It collects all required Data from the Database to generate the Create Page
    Please note the variables file_js and media_js this are variables which are passed into the html to create html code with JS variables in it
    """
    tour_id = request.args.get('id')
    if not tour_id:
        return redirect(url_for('home.home_page'))
    tour = Tour.query.filter_by(id=tour_id, creator_id=current_user.id).first()
    if current_user.has_permission('user_management'):
        tour = Tour.query.filter_by(id=tour_id).first()

    # Check if the tour is shared with the current user or one of the user's groups
    if not tour:
        shared_tour = SharedTour.query.filter_by(
            tour_id=tour_id, collaborator_id=current_user.id, edit="1").first()
        shared_group_tour = SharedGroupTour.query.join(GroupUsers, GroupUsers.group_id == SharedGroupTour.group_id).filter(
            SharedGroupTour.tour_id == tour_id, GroupUsers.user_id == current_user.id, GroupUsers.accepted == True, SharedGroupTour.edit == "1").first()
        if shared_tour or shared_group_tour:
            tour = Tour.query.filter_by(id=tour_id).first()
        else:
            if current_user.is_authenticated and Tour.query.filter_by(id=tour_id).first():
                return render_template("error.html", status_code=403, status_message="Access denied")
            else:
                return render_template("error.html", status_code=404, status_message="Tour not found")

    tour.owner = User.query.get(tour.creator_id)
    tour.editor = {editor.id: User.query.filter_by(id=editor.collaborator_id).first().username
                   for editor in SharedTour.query.filter_by(tour_id=tour.id, edit="1").all()}
    tour.viewer = {viewer.id: User.query.filter_by(id=viewer.collaborator_id).first().username
                   for viewer in SharedTour.query.filter_by(tour_id=tour.id, edit="0").all()}
    tour.editorGroup = {editorGroup.id: Group.query.filter_by(id=editorGroup.group_id).first().name
                        for editorGroup in SharedGroupTour.query.filter_by(tour_id=tour.id, edit="1").all()}
    tour.viewerGroup = {viewerGroup.id: Group.query.filter_by(id=viewerGroup.group_id).first().name
                        for viewerGroup in SharedGroupTour.query.filter_by(tour_id=tour.id, edit="0").all()}
    tour.viewerGroupMembers = {viewerGroup.id: [user.username for user in User.query.join(GroupUsers, GroupUsers.user_id == User.id).filter(GroupUsers.group_id == viewerGroup.group_id, GroupUsers.accepted == True).all()]
                               for viewerGroup in SharedGroupTour.query.filter_by(tour_id=tour.id, edit="0").all()}
    tour.editorGroupMembers = {editorGroup.id: [user.username for user in User.query.join(GroupUsers, GroupUsers.user_id == User.id).filter(GroupUsers.group_id == editorGroup.group_id, GroupUsers.accepted == True).all()]
                               for editorGroup in SharedGroupTour.query.filter_by(tour_id=tour.id, edit="1").all()}

    tour_annotation = tour.annotation if tour.annotation is not None else ""
    tour_annotation_unread = TourAnnotationUnread.query.filter_by(
        tour_id=tour_id, user_id=current_user.id).first()
    last_tour_annotation = tour_annotation_unread.last_annotation if tour_annotation_unread is not None else ""

    tour_uploaded_files = Asset.query.filter_by(tour_id=tour_id).all()

    file_list = []
    for file in tour_uploaded_files:
        _, ext = os.path.splitext(file.filename_id)
        ext = ext.lower()

        # Get the file type from the map, default to 'Other' if the extension isn't in the map
        file_type = current_app.config['FILE_TYPES'].get(ext, 'Other')

        file_list.append({
            "filename_id": file.filename_id,
            "name": file.name,
            "format": file.format,
            "media_type": file.media_type,
            "annotation": file.annotation,
            "equirectangular": file.equirectangular
        })

    # Fetch all mediapages of the tour, and their associated hotspots
    mediapages = Mediapage.query.filter_by(tour_id=tour_id).all()

    # Load hotspots for each mediapage
    for mediapage in mediapages:
        mediapage.hotspots = Hotspot.query.filter_by(
            media_page_id=mediapage.id).all()

    # Create dummy for the JS function
    file_js = type('obj', (object,), {'filename_id': '${file.filename}',
                                      'name': '${file.name}',
                                      'filetype': '${file.filetype}',
                                      'media_type': '${file.media_type}',
                                      })
    media_js = type('obj', (object,), {'asset_id': '${file.asset_id}',
                                       'id': '${file.id}',
                                       'name': '${file.name}',
                                       'annotation': '${file.annotation}',
                                       'media_type': '${file.media_type}',
                                       'thumbnail': '${file.thumbnail}'
                                       })

    # load chat messages
    chat = Chat.query.filter_by(tour_id=tour_id).all()
    formatted_chat = "".join(
        f"[{message.created}] {User.query.get(message.user_id).username}:\n{message.message}\n"
        for message in chat
    )
    # load unread chat messages
    chat_unread_count = len(chat)
    chat_unread = ChatUnread.query.filter_by(
        tour_id=tour_id, user_id=current_user.id).first()
    if chat_unread:
        chat_unread_count = Chat.query.filter(
            Chat.tour_id == tour_id, Chat.id > chat_unread.last_chat_id).count()

    tour_creator = User.query.get(tour.creator_id).username

    # Get all groups that the current user is associated with
    groups = {group.id: group.name for group in Group.query.join(
        GroupUsers, GroupUsers.group_id == Group.id).filter(GroupUsers.user_id == current_user.id, GroupUsers.accepted == True).all()}

    lrs_event('/create')
    return render_template('create/create_tour.html', uploaded_files=file_list, file_js=file_js,
                           tour=tour, tour_id=tour_id, tour_creator=tour_creator, tour_annotation=tour_annotation, last_tour_annotation=last_tour_annotation, mediapages=mediapages, media_js=media_js, chat=formatted_chat, chat_unread_count=chat_unread_count, groups=groups, currentYear=datetime.now().year, VERSION=VERSION)


@home_bp.route('/upload', methods=['POST'], endpoint='upload_route')
@login_required
def upload_route():
    """
    This is a simple Route for uploading files.
    The files are uploaded to the Uploaded_Media folder according to the configuration. In addition, small previews are created in the form of thumbnails.
    """
    tour_id = request.values.get('tour_id')
    if 'image' not in request.files:
        return jsonify({"message": "Image not found"}), 400
    if not tour_id:
        return jsonify({"message": "Tour not found"}), 400

    file = request.files['image']

    if file.filename == '' or not file:
        return jsonify({"message": "Filename not found"}), 400

    if len(Tour.query.filter_by(id=tour_id, creator_id=current_user.id).all()) < 1:
        return jsonify({"message": "Tour not found"}), 400

    if not uploaded_images.file_allowed(file, file.filename.lower()):
        return jsonify({"message": "Type not allowed"}), 405

    # User Quota Check
    file_size_gb = int(request.headers.get('Content-Length', 0)) / (1024 ** 3)
    user = User.query.filter_by(id=current_user.id).first()
    newQuota = user.used_quota_gb + file_size_gb

    if user.individual_quota_gb:
        givenQuota = user.individual_quota_gb
    else:
        givenQuota = 0
        for role in user.roles:
            if role.default_quota_gb > givenQuota:
                givenQuota = role.default_quota_gb

    if newQuota > givenQuota:
        return jsonify({"message": "User Quota exceeded"}), 426

    user.used_quota_gb = newQuota
    db.session.commit()

    # Interpolate the filename by deciding whether there already exists a tour that references to the uploaded asset
    imp_mpage = Mediapage.query.filter_by(
        tour_id=tour_id, asset_id=file.filename).first()

    if imp_mpage:
        imp_asset = Asset.query.filter_by(
            filename_id=imp_mpage.asset_id).first()
        if not imp_asset:
            filename = imp_mpage.asset_id
        else:
            filename = "copy_" + imp_mpage.asset_id
    else:
        filename = str(int(time.time() * 10000000)) + \
            "." + file.filename.split(".")[-1]
        filename = filename.lower()

    name = filename.split('.')[0]
    name = secure_filename(file.filename.split(".")[0])[:17]
    uploaded_images.save(file, name=filename)

    _, ext = os.path.splitext(file.filename)
    ext = ext.lower()

    asset = Asset(
        filename_id=filename,
        name=name,
        format=file.content_type,
        created=datetime.utcnow(),
        tour_id=tour_id,
        media_type=current_app.config['FILE_TYPES'].get(ext, 'Other')
    )

    db.session.add(asset)
    # db.session.commit()

    # Check if the uploaded file is an image or a video
    media_type = current_app.config['FILE_TYPES'].get(ext, 'Other')

    # Create thumbnail
    thumb_path = os.path.join(
        current_app.config['UPLOADED_THUMBNAIL_DEST'], filename + '.jpg')
    file_path = os.path.join(
        current_app.config['UPLOADED_IMAGES_DEST'], filename)
    if media_type == 'Image':
        img = Image.open(file)
        width, height = img.size
        if width and height:
            asset.equirectangular = (width == 2 * height)
        img = img.resize((512, 512))
        img = img.convert("RGB")
        img.save(thumb_path, "JPEG")
    elif media_type == 'Video':
        clip = VideoFileClip(file_path)
        duration = clip.duration
        mid_time = duration / 2
        thumbnail = clip.save_frame(thumb_path, t=mid_time)

    db.session.commit()

    lrs_event('/upload')
    return jsonify({"message": "File uploaded", "filename": filename, "name": name, "media_type": media_type}), 200


@home_bp.route('/uploaded_files', methods=['GET'], endpoint='get_uploaded_files')
@login_required
def get_uploaded_files():
    """
    This function gets all Assets which are uploaded to a given tour
    """
    tour_id = request.args.get("tour_id")
    if not tour_id:
        return render_template("error.html", status_code=400, status_message="Tour not found")

    user_uploaded_files = Asset.query.filter_by(tour_id=tour_id).all()

    file_list = []

    for file in user_uploaded_files:
        _, ext = os.path.splitext(file.filename_id)
        ext = ext.lower()

        # Get the file type from the map, default to 'Other' if the extension isn't in the map
        file_type = current_app.config['FILE_TYPES'].get(ext, 'Other')

        file_list.append({
            "filename": file.filename_id,
            "name": file.name,
            "media_type": file.media_type,
            "annotation": file.annotation,
            "equirectangular": file.equirectangular
        })
    lrs_event('/uploaded_files')
    return jsonify(file_list)


@home_bp.route("/edit_asset_name", methods=["POST"], endpoint='edit_asset_name')
@login_required
def edit_asset_name():
    """
    This Function allows to edit the Name of an Asset. Note that the Name is a field in the Database and not the filename
    """
    data = request.get_json()
    # Secure the User-Input
    name = data['name']
    # Check for empty name
    if not name or name == "":
        return render_template("error.html", status_code=400, status_message="Filename too short")
    if len(name) > 12:
        return render_template("error.html", status_code=400, status_message="Filename too long")
    file = Asset.query.filter_by(
        filename_id=data['filename_id'], tour_id=data['tour_id']).first()
    if not file:
        return render_template("error.html", status_code=400, status_message="File not found")

    file.name = name
    db.session.commit()

    lrs_event('/edit_asset_name')
    return jsonify({"message": "File name updated"}), 200


@home_bp.route("/delete_file", methods=["POST"], endpoint='delete_file')
@login_required
def delete_file():
    """
    This function deletes an asset
    """
    data = request.get_json()

    file = Asset.query.filter_by(
        filename_id=data['filename'], tour_id=data['tour_id']).first()
    if not file:
        return render_template("error.html", status_code=400, status_message="File not found")

    db.session.delete(file)
    db.session.commit()

    lrs_event('/delete_file')
    return jsonify({"message": "File deleted"}), 200


@home_bp.route('/new_tour', methods=['GET'], endpoint='create_tour')
@login_required
@permissions_required('create_tour')
def create_tour():
    """
    In this function a new empty tour is created
    """
    # Get project name from request data
    project_name = request.args.get('project_name')

    # Create new tour with project name
    tour = Tour(name=project_name, creator_id=current_user.id)
    db.session.add(tour)
    db.session.commit()

    # Return success response to client
    lrs_event('/new_tour')
    return redirect(url_for('home.create_page') + "?id=" + str(tour.id))


@home_bp.route('/create_mediapage', methods=['POST'], endpoint='create_mediapage')
@login_required
def create_mediapage():
    """
    This function is called if the user clicks the green "+"-Button to create a new Mediapage from an Asset
    """
    tour_id = request.form.get('tour_id')
    if not tour_id:
        return render_template("error.html", status_code=400, status_message="Tour not found")

    presented_media_id = request.form.get('asset_id')
    if not presented_media_id:
        return render_template("error.html", status_code=400, status_message="Media not found")

    media_name = request.form.get('name').strip()
    if not media_name:
        return render_template("error.html", status_code=400, status_message="Media not found")

    asset = Asset.query.filter_by(filename_id=presented_media_id,
                                  tour_id=tour_id).first()

    # Check if any media pages with the specified tour_id exist
    existing_mediapages = Mediapage.query.filter_by(tour_id=tour_id).all()
    is_start_page = False
    if not existing_mediapages:
        is_start_page = True

    _, ext = os.path.splitext(presented_media_id)
    ext = ext.lower()

    # Get the file type from the map, default to 'Other' if the extension isn't in the map
    file_type = current_app.config['FILE_TYPES'].get(ext, 'Other')

    new_mediapage = Mediapage(tour_id=tour_id, asset_id=presented_media_id,
                              name=media_name, is_start_page=is_start_page, media_type=file_type)

    db.session.add(new_mediapage)
    db.session.commit()

    lrs_event('/create_mediapage')
    return jsonify({"message": "Mediapage created", "mediapage_id": new_mediapage.id, "equirectangular": asset.equirectangular}), 201


@home_bp.route('/save_hotspot', methods=['POST'], endpoint='save_hotspot')
@login_required
def save_hotspot():
    """
    This Function creates a new Hotspot. Therefore a lot of parameters are necessary.
    """
    name = request.form.get("name")
    position = request.form.get("position")
    rotation = request.form.get("rotation")
    mediapage = request.form.get("media_page_id")
    media_type = request.form.get("media_type")
    presented_media_id = request.form.get("presented_media_id")
    custom_display_icon_id = request.form.get('custom_display_icon_id')
    starttime = request.form.get('starttime')
    endtime = request.form.get('endtime')

    if request.form.get('endtime') == "NaN":
        endtime = -1

    if not position or not rotation:
        return render_template("error.html", status_code=400, status_message="Position or Rotation values not found")

    new_hotspot = Hotspot(name=name, position=position, rotation=rotation, media_page_id=mediapage, media_type_id=media_type,
                          presented_media_id=presented_media_id, custom_display_icon_id=custom_display_icon_id, starttime=starttime, endtime=endtime)
    db.session.add(new_hotspot)
    db.session.commit()

    lrs_event('/save_hotspot')
    return jsonify({"message": "Hotspot saved", 'hotspot_id': new_hotspot.id}), 200


@home_bp.route('/media_pages', methods=['GET'], endpoint='get_media_pages')
@login_required
def get_media_pages():
    """
    This function returns a list of all mediapages from a Tour.
    """
    tour_id = request.args.get("tour_id")
    if not tour_id:
        return render_template("error.html", status_code=400, status_message="Tour not found")

    tour_media = Mediapage.query.filter_by(tour_id=tour_id).all()
    tour_media_list = [tour.to_dict() for tour in tour_media]

    lrs_event('/media_pages')
    return jsonify(tour_media_list)


@home_bp.route('/remove-hotspot', methods=['POST'], endpoint='remove_hotspot')
@login_required
def remove_hotspot():
    """
    As Simply as the function name is. this function removes a hotspot from a Tour
    """
    hotspotid = request.form.get('hotspot_id')

    if not hotspotid:
        return render_template("error.html", status_code=400, status_message="Hotspot not found")

    hotspot = Hotspot.query.get(hotspotid)
    if not hotspot:
        raise ValueError(f"Hotspot with ID {hotspotid} not found")

    # Delete corresponding Dependencies
    dependencies_to_delete = Dependencies.query.filter(
        or_(Dependencies.hotspot_id == hotspotid, Dependencies.dependsOn == hotspotid)).all()
    if dependencies_to_delete:
        for dependency in dependencies_to_delete:
            db.session.delete(dependency)
            db.session.commit()
    # Delete corresponding Quiz Answers if the Hotspot is a Quiz
    quiz_answers_to_delete = EscapeRoomAnswers.query.filter(
        EscapeRoomAnswers.hotspot_id == hotspotid).all()
    if quiz_answers_to_delete:
        for quiz_answer in quiz_answers_to_delete:
            print(quiz_answer)
            db.session.delete(quiz_answer)
            db.session.commit()

    # Delete corresponding HotspotVisited
    hotspot_visited_to_delete = HotspotVisited.query.filter(
        HotspotVisited.hotspot_id == hotspotid).all()
    for currenthotspot in hotspot_visited_to_delete:
        db.session.delete(currenthotspot)
        db.session.commit()

    # Delete the hotspot from the database
    db.session.delete(hotspot)
    # Commit the changes
    db.session.commit()

    lrs_event('/remove-hotspot')
    return jsonify({"message": f"Hotspot with ID {hotspotid} removed successfully."})


@home_bp.route('/update-mediapage', methods=['POST'], endpoint='update_mediapage')
@login_required
def update_mediapage():
    """
    This method is used to change mediapages propertys in the database.
    """
    data = request.json
    mediapage_id = data.get('mediaId')
    tour_id = data.get('tour_id')
    mediapage = Mediapage.query.get(mediapage_id)
    if mediapage is None:
        return render_template("error.html", status_code=400, status_message="Mediapage not found")
    if 'tour_id' in data:
        mediapage.tour_id = data['tour_id']
    if 'is_start_page' in data and 'tour_id' in data:
        db.session.query(Mediapage).filter(Mediapage.tour_id == tour_id).update(
            {Mediapage.is_start_page: 0}, synchronize_session='fetch')
        mediapage.is_start_page = data['is_start_page']
    if 'orientation' in data:
        mediapage.orientation = data['orientation']
    if 'name' in data:
        mediapage.name = data['name'].strip()
    if 'annotation' in data:
        mediapage.annotation = data['annotation']

    db.session.commit()

    lrs_event('/update-mediapage')
    return jsonify(mediapage.to_dict()), 200


@home_bp.route('/delete-mediapage', methods=['POST'], endpoint='delete_mediapage')
@login_required
def delete_mediapage():
    """
    As Simply as the function name is. this function deletes a Mediapage. Note that also all Hotspot on it will be removed by SQLalchemy.
    """
    data = request.get_json()
    mediapage_id = data.get('mediapage_id')

    mediapage = Mediapage.query.filter_by(id=mediapage_id).first()
    if mediapage:
        # Find and delete associated hotspots
        hotspots = Hotspot.query.filter_by(media_page_id=mediapage_id).all()
        for hotspot in hotspots:
            # Delete corresponding Dependencies
            dependencies_to_delete = Dependencies.query.filter(
                or_(Dependencies.hotspot_id == hotspot.id, Dependencies.dependsOn == hotspot.id)).all()
            if dependencies_to_delete:
                for dependency in dependencies_to_delete:
                    db.session.delete(dependency)
                    db.session.commit()
            # Delete corresponding Quiz Answers if the Hotspot is a Quiz
            quiz_answers_to_delete = EscapeRoomAnswers.query.filter(
                EscapeRoomAnswers.hotspot_id == hotspot.id).all()
            if quiz_answers_to_delete:
                for quiz_answer in quiz_answers_to_delete:
                    print(quiz_answer)
                    db.session.delete(quiz_answer)
                    db.session.commit()

            # Delete corresponding HotspotVisited
            hotspot_visited_to_delete = HotspotVisited.query.filter(
                HotspotVisited.hotspot_id == hotspot.id).all()
            for currenthotspot in hotspot_visited_to_delete:
                db.session.delete(currenthotspot)
                db.session.commit()

                db.session.delete(hotspot)

        # if currently is startpage define a new one
        if (mediapage.is_start_page):
            newStartPage = Mediapage.query.filter_by(
                tour_id=mediapage.tour_id).first()
            newStartPage.is_start_page = 1

        # Delete the media page
        db.session.delete(mediapage)
        db.session.commit()

        lrs_event('/delete-mediapage')
        return jsonify(success=True), 200
    else:
        return jsonify(success=False, error="MediaPage not found"), 404


@login_required
@home_bp.route("/hotspot/<string:hotspot_id>", methods=["PUT"], endpoint='update_hotspot')
def update_hotspot(hotspot_id):
    """
    This function can change Properties of a Hotspot
    """
    hotspot = Hotspot.query.get(hotspot_id)
    if hotspot is None:
        return render_template("error.html", status_code=400, status_message="Hotspot not found")

    data = request.form
    if "name" in data:
        hotspot.name = data["name"]
    if "position" in data:
        hotspot.position = data["position"]
    if "rotation" in data:
        hotspot.rotation = data["rotation"]
    if "z-rotation" in data:
        prevRotation = hotspot.rotation
        hotspot.rotation = prevRotation.rsplit(
            ' ', 1)[0] + ' ' + data["z-rotation"]
    if "desc" in data:
        hotspot.desc = data["desc"]
    if "size" in data:
        hotspot.size = data["size"]
    if "linked_media_page_id" in data:
        hotspot.linked_media_page_id = data["linked_media_page_id"]
    if "presented_media_id" in data:
        hotspot.presented_media_id = data["presented_media_id"]
    if "custom_display_icon_id" in data:
        hotspot.custom_display_icon_id = data["custom_display_icon_id"]
    if "starttime" in data:
        hotspot.starttime = data["starttime"]
    if "endtime" in data:
        hotspot.endtime = data["endtime"]

    db.session.commit()
    lrs_event('/hotspot/<string:hotspot_id>')
    return jsonify(hotspot.to_dict())


@login_required
@home_bp.route('/delete_tour/<string:tour_id>', methods=['DELETE'], endpoint='delete_tour')
@permissions_required('delete_tour')
def delete_tour(tour_id):
    # Query the database for the Tour with the given tour_id
    tour = Tour.query.filter_by(id=tour_id).first()
    # If no tour with this id is found, return an error message
    if tour is None:
        return render_template("error.html", status_code=404, status_message="Tour not found")

    # Delete all hotspots associated with the mediapages of the tour
    mediapages = Mediapage.query.filter_by(tour_id=tour.id).all()
    for mediapage in mediapages:
        Hotspot.query.filter_by(media_page_id=mediapage.id).delete()

    # Delete all mediapages associated with the tour
    Mediapage.query.filter_by(tour_id=tour.id).delete()

    # Delete all assets associated with the tour
    Asset.query.filter_by(tour_id=tour.id).delete()

    # Delete all shared group tours associated with the tour
    SharedGroupTour.query.filter_by(tour_id=tour.id).delete()

    # Delete all chat unread entries associated with the tour
    ChatUnread.query.filter_by(tour_id=tour.id).delete()

    # Delete all chat entries associated with the tour
    Chat.query.filter_by(tour_id=tour.id).delete()

    # Delete all shared tours associated with the tour
    SharedTour.query.filter_by(tour_id=tour.id).delete()

    db.session.delete(tour)
    db.session.commit()

    lrs_event('/delete_tour/<string:tour_id>')
    return jsonify({"message": "Tour deleted successfully"}), 200


@login_required
@home_bp.route('/accept_group_invite', methods=['PUT'], endpoint='accept_group_invite')
def accept_group_invite():
    group_id = request.form.get('group_id')
    group_user = GroupUsers.query.filter_by(
        group_id=group_id, user_id=current_user.id).first()
    if group_user is None:
        return jsonify({"message": "Group not found"}), 404
    group_user.accepted = True
    db.session.commit()
    lrs_event('/accept_group_invite')
    return jsonify({"message": "Group invite accepted successfully"}), 200


@home_bp.route("/remove_from_group", methods=["DELETE"], endpoint='remove_from_group')
@login_required
def remove_from_group():
    data = request.get_json()

    groupUsers = GroupUsers.query.filter_by(
        group_id=data['group_id'], user_id=current_user.id).first()

    db.session.delete(groupUsers)
    db.session.commit()

    lrs_event('/remove_from_group')
    return jsonify({"message": "Removed from group"}), 200


@login_required
@home_bp.route('/edit_collaborator_rights/<string:sharedTour_id>', methods=['PUT'], endpoint='edit_collaborator_rights')
@permissions_required('edit_collaborator')
def edit_collaborator_rights(sharedTour_id):
    sharedTour = SharedTour.query.filter_by(id=sharedTour_id).first()
    # If no tour with this id is found, return an error message
    if sharedTour is None:
        return jsonify({"message": "SharedTour not found"}), 404

    sharedTour.edit = request.form.get('edit')
    db.session.commit()

    lrs_event('/edit_collaborator_rights/<string:sharedTour_id>')
    return jsonify({"message": "Collaborator rights edited successfully", "tour_id": sharedTour.tour_id}), 200


@login_required
@home_bp.route('/add_collaborator/<string:tour_id>', methods=['POST'], endpoint='add_collaborator')
@permissions_required('edit_collaborator')
def add_collaborator(tour_id):
    collaborator = request.form.get('collaborator')
    user = User.query.filter_by(
        username=collaborator).first()
    if user is None:
        return jsonify({"message": "User not found"}), 404
    edit = request.form.get('edit')
    if SharedTour.query.filter_by(tour_id=tour_id, collaborator_id=user.id).first():
        return jsonify({"message": "Collaborator already exists"}), 400
    sharedTour = SharedTour(
        tour_id=tour_id, collaborator_id=user.id, edit=edit)
    db.session.add(sharedTour)
    db.session.commit()

    send_email("share", user.email,
               user.username, tour_id=sharedTour.tour_id)
    lrs_event('/add_collaborator/<string:tour_id>')
    return jsonify({"message": "Collaborator added successfully", "collaborator_id": user.id}), 200


@login_required
@home_bp.route('/remove_collaborator/<string:sharedTour_id>', methods=['DELETE'], endpoint='remove_collaborator')
@permissions_required('edit_collaborator')
def remove_collaborator(sharedTour_id):
    # Query the database for the Tour with the given tour_id
    sharedTour = SharedTour.query.get(sharedTour_id)
    # If no tour with this id is found, return an error message
    if sharedTour is None:
        return jsonify({"message": "SharedTour not found"}), 404

    # If the tour is found, delete it from the database
    db.session.delete(sharedTour)

    # Commit the changes to the database
    db.session.commit()
    lrs_event('/remove_collaborator/<string:sharedTour_id>')
    return jsonify({"message": "Collaborator removed successfully", "tour_id": sharedTour.tour_id, "collaborator_id": sharedTour.collaborator_id}), 200


@login_required
@home_bp.route('/edit_group_rights/<string:sharedGroupTour_id>', methods=['PUT'], endpoint='edit_group_rights')
@permissions_required('edit_collaborator')
def edit_group_rights(sharedGroupTour_id):
    sharedGroupTour = SharedGroupTour.query.filter_by(
        id=sharedGroupTour_id).first()
    # If no tour with this id is found, return an error message
    if sharedGroupTour is None:
        return jsonify({"message": "SharedGroupTour not found"}), 404

    sharedGroupTour.edit = request.form.get('edit')
    db.session.commit()

    lrs_event('/edit_group_rights/<string:sharedGroupTour_id>')
    return jsonify({"message": "Collaborator rights edited successfully", "tour_id": sharedGroupTour.tour_id}), 200


@login_required
@home_bp.route('/add_group/<string:tour_id>', methods=['POST'], endpoint='add_group')
@permissions_required('edit_collaborator')
def add_group(tour_id):
    group = request.form.get('group')
    group_db = Group.query.filter_by(
        name=group).first()
    edit = request.form.get('edit')
    if SharedGroupTour.query.filter_by(tour_id=tour_id, group_id=group_db.id).first():
        return jsonify({"message": "Collaborator already exists"}), 400
    sharedGroupTour = SharedGroupTour(
        tour_id=tour_id, group_id=group_db.id, edit=edit)
    db.session.add(sharedGroupTour)
    db.session.commit()

    # send Mail to all Users in the Group
    group_users = GroupUsers.query.filter_by(group_id=group_db.id).all()
    for group_user in group_users:
        user = User.query.get(group_user.user_id)
        if user:
            send_email("share", user.email, user.username,
                       tour_id=sharedGroupTour.tour_id)

    lrs_event('/add_group/<string:tour_id>')
    return jsonify({"message": "Group added successfully", "group_id": group_db.id}), 200


@login_required
@home_bp.route('/remove_group/<string:sharedGroupTour_id>', methods=['DELETE'], endpoint='remove_group')
@permissions_required('edit_collaborator')
def remove_group(sharedGroupTour_id):
    # Query the database for the Tour with the given tour_id
    sharedGroupTour = SharedGroupTour.query.filter_by(
        id=sharedGroupTour_id).first()
    # If no tour with this id is found, return an error message
    if sharedGroupTour is None:
        return jsonify({"message": "SharedGroupTour not found"}), 404

    # If the tour is found, delete it from the database
    db.session.delete(sharedGroupTour)

    # Commit the changes to the database
    db.session.commit()
    lrs_event('/remove_group/<string:sharedGroupTour_id>')
    return jsonify({"message": "Group removed successfully", "tour_id": sharedGroupTour.tour_id, "group_id": sharedGroupTour.group_id}), 200


@login_required
@home_bp.route('/copy_tour/<string:tour_id>', methods=['POST'], endpoint='copy_tour')
@permissions_required('copy_tour')
def copy_tour(tour_id):
    # Get project name from request data
    project_name = Tour.query.filter_by(id=tour_id).first().name[:45] + "-copy"

    """
    # Check if tour name is already taken
    while True:
        tour = Tour.query.filter_by(name=project_name).first()
        if tour:
            project_name = project_name + "-copy"
        else:
            break
    """

    # Retrieve all mediapages and hotspots associated with the tour
    mediapages = Mediapage.query.filter_by(tour_id=tour_id).all()
    # assets = Asset.query.filter_by(tour_id=tour_id).all()
    hotspots = Hotspot.query.join(Mediapage, and_(
        Hotspot.media_page_id == Mediapage.id, Mediapage.tour_id == tour_id)).all()

    # Copy all belonging mediapages and hotspots
    tour = Tour(name=project_name, creator_id=current_user.id)
    db.session.add(tour)

    new_tour = Tour.query.filter_by(name=project_name).first()

    for mediapage in mediapages:
        mediapage_copy = Mediapage(asset_id=mediapage.asset_id, tour_id=new_tour.id,
                                   is_start_page=mediapage.is_start_page, name=mediapage.name, media_type=mediapage.media_type)
        db.session.add(mediapage_copy)

    for hotspot in hotspots:
        oldMediapage = Mediapage.query.filter_by(
            id=hotspot.media_page_id).first()
        newMediapage = Mediapage.query.filter_by(
            asset_id=oldMediapage.asset_id, tour_id=new_tour.id).first()
        hotspot_copy = Hotspot(media_page_id=newMediapage.id, linked_media_page_id=hotspot.linked_media_page_id, media_type_id=hotspot.media_type_id, presented_media_id=hotspot.presented_media_id, name=hotspot.name,
                               desc=hotspot.desc, position=hotspot.position, rotation=hotspot.rotation, size=hotspot.size, custom_display_icon_id=hotspot.custom_display_icon_id, starttime=hotspot.starttime, endtime=hotspot.endtime)
        db.session.add(hotspot_copy)
    db.session.commit()

    lrs_event('/copy_tour/<string:tour_id>')
    return jsonify({"message": "Tour copied"}), 200


@login_required
@home_bp.route('/export_tour/<string:tour_id>', methods=['GET'], endpoint='export_tour')
@permissions_required('export_tour')
def export_tour(tour_id):
    # Retrieve all mediapages and hotspots associated with the tour
    tour = Tour.query.filter_by(id=tour_id).first()
    mediapages = Mediapage.query.filter_by(tour_id=tour_id).all()
    assets = Asset.query.filter_by(tour_id=tour_id).all()
    hotspots = Hotspot.query.join(Mediapage, and_(
        Hotspot.media_page_id == Mediapage.id, Mediapage.tour_id == tour_id)).all()

    exportData = {
        "tour_name": str(tour.name),
        "tour": str(clean_dict(tour.__dict__)),
        "mediapages": [],
        "assets": [],
        "hotspots": []
    }

    for mediapage in mediapages:
        exportData["mediapages"].append(str(clean_dict(mediapage.__dict__)))

    for asset in assets:
        exportData["assets"].append(clean_dict(asset.__dict__))

    for hotspot in hotspots:
        exportData["hotspots"].append(str(clean_dict(hotspot.__dict__)))

    lrs_event('/export_tour/<string:tour_id>')
    return jsonify(exportData), 201

# Removes '_sa_instance_state" entries from the export file


def clean_dict(data):
    if isinstance(data, dict):
        return {key: clean_dict(value) for key, value in data.items() if key != "_sa_instance_state"}
    else:
        return data


@login_required
@home_bp.route('/import_tour', methods=['POST'], endpoint='import_tour')
@permissions_required('import_tour')
def import_tour():
    data = request.get_json()
    project_name = data["tour_name"][:45] + "-import"

    """
    # Check if tour name is already taken
    n = 2
    while True:
        tour = Tour.query.filter_by(name=project_name).first()
        if tour:
            project_name = data["tour_name"] + "-import" + "-" + str(n)
            n += 1
        else:
            break
    """

    tour = Tour(creator_id=current_user.id, name=project_name)
    db.session.add(tour)
    db.session.commit()

    mediapage_mapping = {}

    for asset in data["assets"]:
        if asset["filename_id"]:
            # Check if the file exists
            if os.path.exists(os.path.join(current_app.config['UPLOADED_IMAGES_DEST'], asset["filename_id"])):
                newAsset = Asset(filename_id=asset["filename_id"], name=asset["name"], format=asset["format"], created=datetime.utcnow(
                ), tour_id=tour.id, media_type=asset["media_type"])
                db.session.add(newAsset)
            else:
                print("File not found. Skipping asset..")

    for mediapage in data["mediapages"]:
        newMediapage = Mediapage(asset_id=extract_data(mediapage, 'asset_id'), tour_id=tour.id, is_start_page=int(extract_data(
            mediapage, 'is_start_page')), name=extract_data(mediapage, 'name'), media_type=extract_data(mediapage, 'media_type'))
        db.session.add(newMediapage)

        searchMediapage = Mediapage.query.filter_by(asset_id=extract_data(
            mediapage, 'asset_id'), tour_id=tour.id).first()
        mediapage_mapping[extract_data(mediapage, 'id')] = searchMediapage.id
        mediapage_mapping[None] = None

    for hotspot in data["hotspots"]:
        try:
            newHotspot = Hotspot(media_page_id=mediapage_mapping[extract_data(hotspot, 'media_page_id')], linked_media_page_id=mediapage_mapping[extract_data(hotspot, 'linked_media_page_id')], media_type_id=extract_data(hotspot, 'media_type_id'), presented_media_id=extract_data(hotspot, 'presented_media_id'), name=extract_data(hotspot, 'name'), desc=extract_data(
                hotspot, 'desc'), position=extract_data(hotspot, 'position'), rotation=extract_data(hotspot, 'rotation'), size=extract_data(hotspot, 'size'), custom_display_icon_id=extract_data(hotspot, 'custom_display_icon_id'), starttime=extract_data(hotspot, 'starttime'), endtime=-1)
            db.session.add(newHotspot)
        except Exception:
            print("Invalid hotspot data. Skipping hotspot..")

    db.session.commit()

    lrs_event('/import_tour')
    return jsonify({"message": "Tour imported", "tour_id": tour.id}), 201

# Extracts specified data field from pseudo-JSON-string


def extract_data(data_string, key):
    start = data_string.find(key) + len(key) + 3
    end = data_string[start:].find(",") + start

    if start > end:
        end = len(data_string) - 1
    else:
        n = 1
        if data_string[start-len(key)-4:start-5] in ["_", "V"] or data_string[start-len(key)-10:start-17] in ["linked"]:
            if data_string[0:1] in ["_", "V"]:
                return extract_data(data_string[2:], key)
            elif data_string[0:1] in ["l"]:
                return extract_data(data_string[22:], key)
            else:
                return extract_data(data_string[1:], key)
        while data_string[end:end+3] != ", '":
            end = data_string[start+n:].find(",") + start + n
            n += 1

    try:
        return (int(data_string[start:end]))
    except:
        if data_string[start:end].lower() == 'true' or data_string[start:end].lower() == 'false':
            return (bool(data_string[start:end]))
        elif data_string[start:end].lower() == 'none':
            return None
        else:
            try:
                return (float(data_string[start:end]))
            except:
                return (data_string[start+1:end-1])


@login_required
@home_bp.route('/rename_tour', methods=['PUT'], endpoint='rename_tour')
@permissions_required('rename_tour')
def rename_tour():
    data = request.get_json()
    tour = Tour.query.get(data['tour_id'])
    tour.name = data['newName']
    db.session.commit()

    lrs_event('/rename_tour')
    return jsonify({"message": "Tour renamed"}), 200


@login_required
@home_bp.route('/join_group', methods=['PUT'], endpoint='join_group')
def join_group():
    data = request.get_json()

    group_user = GroupUsers.query.filter_by(invite_token=data['token']).first()
    if group_user:
        if group_user.user_id is not None and group_user.user_id != current_user.id:
            return jsonify({"message": _("Token already used by another account!")}), 400
        already_member = GroupUsers.query.filter_by(
            group_id=group_user.group_id, user_id=current_user.id, accepted=True).first()
        if already_member:
            return jsonify({"message": _("You are already in this group!")}), 400
        group_user.user_id = current_user.id
        group_user.accepted = True
        group = Group.query.get(group_user.group_id)
        for role in group.roles.split(', '):
            role = Role.query.filter_by(name=role).first()
            if role:
                user_datastore.add_role_to_user(current_user, role)
        db.session.commit()
    else:
        return jsonify({"message": _("The token was invalid!")}), 400

    lrs_event('/join_group')
    return jsonify({"message": _("Group successfully joined.")}), 200


@login_required
@home_bp.route('/deny_group', methods=['PUT'], endpoint='deny_group')
def deny_group():
    data = request.get_json()

    group_user = GroupUsers.query.filter_by(invite_token=data['token']).first()
    if group_user:
        group_user.user_id = current_user.id
        group_user.accepted = False
        db.session.commit()
    else:
        return jsonify({"message": _("The token was invalid!")}), 400

    lrs_event('/deny_group')
    return jsonify({"message": _("Group invention denied.")}), 200


@login_required
@home_bp.route('/set_demotour', methods=['PUT'], endpoint='set_demotour')
@permissions_required('set_demotour')
def set_demotour():
    oldDemo = Tour.query.filter_by(share_string='demo').first()
    if oldDemo:
        oldDemo.generate_unique_share_string()
    data = request.get_json()
    tour = Tour.query.get(data['tour_id'])
    tour.share_string = "demo"
    db.session.commit()

    lrs_event('/set_demotour')
    return jsonify({"message": "Demotour changed"}), 200


@login_required
@home_bp.route('/delete_user/<string:user_id>', methods=['DELETE'], endpoint='delete_user')
@permissions_required('delete_user')
def delete_user(user_id):
    # Query the database for the Tour with the given user_id
    user = User.query.get(user_id)

    # If no tour with this id is found, return an error message
    if user is None:
        return render_template("error.html", status_code=404, status_message="User not found")

    # Remove user from all groups
    GroupUsers.query.filter_by(user_id=user.id).delete()

    # Delete all groups where the user is the creator
    groups = Group.query.filter_by(creator_id=user.id).all()
    for group in groups:
        delete_group(group.id)

    # Delete all chats
    ChatUnread.query.filter_by(user_id=user.id).delete()
    Chat.query.filter_by(user_id=user.id).delete()
    TourAnnotationUnread.query.filter_by(user_id=user.id).delete()

    # Delete all tours where the user is the creator
    user_tours = Tour.query.filter_by(creator_id=user.id).all()
    for tour in user_tours:
        delete_tour(tour.id)

    # Delete all shared tours where the user is the collaborator
    shared_tours = SharedTour.query.filter_by(collaborator_id=user.id).all()
    for shared_tour in shared_tours:
        db.session.delete(shared_tour)

    # If the user is found, delete it from the database
    db.session.delete(user)

    # Commit the changes to the database
    db.session.commit()

    lrs_event('/delete_user/<string:user_id>')
    return jsonify({"message": "User deleted successfully"}), 200


@login_required
@home_bp.route('/create_role', methods=['POST'], endpoint='create_role')
@permissions_required('role_management')
def create_role():
    """
    In this function a new empty role is created
    """
    role_name = request.form.get('role_name')

    # Check if role name is already taken
    role = Role.query.filter_by(name=role_name).first()
    if role:
        return render_template("error.html", status_code=409, status_message="Rolename already exists")

    # Create new role with role name
    role = Role(name=role_name)
    db.session.add(role)
    db.session.commit()

    # Return success response to client
    lrs_event('/create_role')
    return jsonify({"message": "Role created"}), 200


@login_required
@home_bp.route('/update_roles', methods=['PUT'], endpoint='update_roles')
@permissions_required('edit_roles')
def update_roles():
    data = request.get_json()
    user = User.query.get(data['user_id'])
    if not user:
        return render_template("error.html", status_code=404, status_message="User not found")
    user.roles.clear()
    for role in data['roles']:
        user_datastore.add_role_to_user(user, role)
    db.session.commit()

    lrs_event('/update_roles/<string:user_id>')
    return jsonify({"message": "User roles updated successfully"}), 200


@login_required
@home_bp.route('/delete_role/<string:role_id>', methods=['DELETE'], endpoint='delete_role')
@permissions_required('role_management')
def delete_role(role_id):
    role = Role.query.get(role_id)
    if role is None:
        return render_template("error.html", status_code=404, status_message="Role not found")

    db.session.commit()
    db.session.delete(role)
    db.session.commit()

    lrs_event('/delete_role/<string:role_id>')
    return jsonify({"message": "Role deleted successfully"}), 200


@login_required
@home_bp.route('/create_group', methods=['POST'], endpoint='create_group')
@permissions_required('create_group')
def create_group():
    """
    In this function a new empty group is created
    """
    group_name = request.form.get('group_name')
    group_roles = request.form.get('group_roles')

    # Check if group name is already taken
    group = Group.query.filter_by(name=group_name).first()
    if group:
        flash(_("Groupname already exists"), 'danger')
        return jsonify({"message": "User not found"}), 404

    # Create new group with group name
    group = Group(creator_id=current_user.id,
                  name=group_name, roles=group_roles)
    db.session.add(group)
    db.session.commit()

    # Add the creator to the group
    group_user = GroupUsers(
        group_id=group.id, user_id=current_user.id, accepted=True)
    db.session.add(group_user)
    # Add the creator to the group roles
    for role in group.roles.split(', '):
        role = Role.query.filter_by(name=role).first()
        if role:
            user_datastore.add_role_to_user(current_user, role)

    db.session.commit()

    # Return success response to client
    lrs_event('/create_group')
    return jsonify({"message": "Group created"}), 200


@login_required
@home_bp.route('/rename_group', methods=['PUT'], endpoint='rename_group')
@permissions_required('edit_group')
def rename_tour():
    data = request.get_json()
    group = Group.query.get(data['group_id'])
    group.name = data['newName']
    db.session.commit()

    lrs_event('/rename_group')
    return jsonify({"message": "Group renamed"}), 200


@login_required
@home_bp.route('/delete_group', methods=['DELETE'], endpoint='delete_group')
@permissions_required('delete_group')
def delete_group():
    data = request.get_json()
    group = Group.query.get(data['group_id'])

    if group is None:
        return render_template("error.html", status_code=404, status_message="Group not found")

    # Delete all shared group tours associated with the group
    for sharedGroupTour in SharedGroupTour.query.filter_by(group_id=data['group_id']).all():
        db.session.delete(sharedGroupTour)

    # Delete all group users associated with the group
    for group_user in GroupUsers.query.filter_by(group_id=data['group_id']).all():
        db.session.delete(group_user)

    db.session.commit()
    db.session.delete(group)
    db.session.commit()

    lrs_event('/delete_group')
    return jsonify({"message": "Group deleted successfully"}), 200


@login_required
@home_bp.route('/generate_usernames', methods=['POST'], endpoint='generate_usernames')
@permissions_required('edit_group')
def generate_usernames():
    data = request.get_json()
    number = data['number']
    counter = 1
    usernames = []
    while number > 0:
        username = data['group_name'] + str(counter)
        user = User.query.filter_by(username=username).first()
        counter = counter + 1
        if user is None:
            usernames.append(username)
            number = number - 1

    lrs_event('/generate_usernames')
    return jsonify({"usernames": usernames}), 200


@login_required
@home_bp.route('/generate_invites', methods=['POST'], endpoint='generate_invites')
@permissions_required('edit_group')
def generate_invites():
    data = request.get_json()
    accepted = None if os.getenv('GROUP_INVITATION_CONFIRMATION') == '1' else 1
    for _ in range(data['group_invites']):
        group_user = GroupUsers(group_id=data['group_id'], accepted=accepted)
        db.session.add(group_user)
        db.session.commit()

    lrs_event('/generate_invites')
    return jsonify({"message": "Invites generated"}), 200


@login_required
@home_bp.route('/create_users', methods=['POST'], endpoint='create_users')
@permissions_required('edit_group')
def create_users():
    data = request.get_json()
    existing_users = []
    for username in data['usernames']:
        user = User.query.filter_by(username=username).first()
        if user:
            existing_users.append(username)

    if existing_users:
        return jsonify({"message": "Users not found", "existing_users": existing_users}), 404

    for username in data['usernames']:
        user = User(username=username, created_by=current_user.id)
        db.session.add(user)
        db.session.commit()

        # Add the user to the group
        group_user = GroupUsers(
            group_id=data['group_id'], user_id=user.id, accepted=True)
        db.session.add(group_user)
        db.session.commit()
        user.set_password(group_user.invite_token)

        # initial user role
        roles = Role.query.all()
        for role in roles:
            if role.assign_to_new_user:
                user_datastore.add_role_to_user(user, role)

        # Add the user to the group roles
        group = Group.query.get(group_user.group_id)
        for role in group.roles.split(', '):
            role = Role.query.filter_by(name=role).first()
            if role:
                user_datastore.add_role_to_user(current_user, role)

    db.session.commit()
    lrs_event('/create_users')
    return jsonify({"message": "New users created"}), 200


@login_required
@home_bp.route('/generate_username_invites', methods=['POST'], endpoint='generate_username_invites')
@permissions_required('edit_group')
def generate_username_invites():
    data = request.get_json()
    accepted = None if os.getenv('GROUP_INVITATION_CONFIRMATION') == '1' else 1
    missing_users = []
    already_in_group = []
    for username in data['usernames']:
        user = User.query.filter_by(username=username).first()
        if user is None:
            missing_users.append(username)
        if user:
            group_user = GroupUsers.query.filter_by(
                group_id=data['group_id'], user_id=user.id).first()
            if group_user:
                already_in_group.append(username)

    if missing_users:
        return jsonify({"message": "Users not found", "missing_users": missing_users}), 404

    if already_in_group:
        return jsonify({"message": "Users already in group", "already_in_group": already_in_group}), 400

    for username in data['usernames']:
        user = User.query.filter_by(username=username).first()
        if user:
            group_user = GroupUsers(
                group_id=data['group_id'], user_id=user.id, accepted=accepted)
            send_email("invite_group", user.email, user.username,
                       invite_token=group_user.invite_token)
            db.session.add(group_user)

    db.session.commit()
    lrs_event('/generate_email_invites')
    return jsonify({"message": "Email Invites generated"}), 200


@login_required
@home_bp.route('/generate_email_invites', methods=['POST'], endpoint='generate_email_invites')
@permissions_required('edit_group')
def generate_email_invites():
    data = request.get_json()
    accepted = None if os.getenv('GROUP_INVITATION_CONFIRMATION') == '1' else 1

    for email in data['emails']:
        user = User.query.filter_by(email=email).first()
        if user:
            group_user = GroupUsers(
                group_id=data['group_id'], invite_email=email, user_id=user.id, accepted=accepted)
            send_email("invite_group", email, user.username,
                       invite_token=group_user.invite_token)
        else:
            group_user = GroupUsers(
                group_id=data['group_id'], invite_email=email, accepted=accepted)
            send_email("invite_group", email,
                       invite_token=group_user.invite_token)

        db.session.add(group_user)
        db.session.commit()
    lrs_event('/generate_email_invites')
    return jsonify({"message": "Email Invites generated"}), 200


@login_required
@home_bp.route('/update_groups', methods=['PUT'], endpoint='update_groups')
@permissions_required('edit_group')
def update_groups():
    data = request.get_json()
    user = User.query.get(data['user_id'])
    if not user:
        return render_template("error.html", status_code=404, status_message="User not found")

    # Get the list of group IDs from the request data
    group_ids = [Group.query.filter_by(
        name=group_name).first().id for group_name in data['groups']]

    # Find all group memberships for the user
    current_memberships = GroupUsers.query.filter_by(
        user_id=user.id, accepted=True).all()

    # Convert current memberships to a set of group IDs
    current_group_ids = {
        membership.group_id for membership in current_memberships}

    # Convert the new group IDs to a set
    new_group_ids = set(group_ids)

    # Find group memberships to remove (those that are in current but not in new)
    memberships_to_remove = current_group_ids - new_group_ids

    # Find group memberships to add (those that are in new but not in current)
    memberships_to_add = new_group_ids - current_group_ids

    # Remove memberships that are no longer needed
    for group_id in memberships_to_remove:
        membership = GroupUsers.query.filter_by(
            user_id=user.id, group_id=group_id).first()
        if membership:
            db.session.delete(membership)

    # Add new memberships
    for group_id in memberships_to_add:
        new_membership = GroupUsers(
            user_id=user.id, group_id=group_id, accepted=True)
        db.session.add(new_membership)

    db.session.commit()

    lrs_event('/update_groups/<string:user_id>')
    return jsonify({"message": "User groups updated successfully"}), 200


@login_required
@home_bp.route('/remove_member', methods=['DELETE'], endpoint='remove_member')
@permissions_required('edit_group')
def remove_member():
    data = request.get_json()
    group_user = GroupUsers.query.filter_by(id=data['id']).first()
    if group_user:
        if group_user.user_id:
            # Remove all shared group tours where the user is the creator of the tour
            shared_group_tours = SharedGroupTour.query.join(
                Tour, SharedGroupTour.tour_id == Tour.id).filter(Tour.creator_id == group_user.user_id).all()
            for shared_group_tour in shared_group_tours:
                db.session.delete(shared_group_tour)
        db.session.delete(group_user)
        db.session.commit()
    else:
        return jsonify({"message": "User not found in group"}), 404

    lrs_event('/remove_member')
    return jsonify({"message": "User removed from group"}), 200


@login_required
@home_bp.route('/home_tour', methods=['PUT'], endpoint='home_tour')
def home_tour():
    data = request.get_json()

    mpage = Mediapage.query.filter_by(id=extract_data(str(data), 'id')).first()
    old_mpage = Mediapage.query.filter_by(
        tour_id=mpage.tour_id, is_start_page=1).first()
    if old_mpage:
        old_mpage.is_start_page = 0
    mpage.is_start_page = 1

    db.session.commit()

    lrs_event('/home_tour')
    return jsonify({"message": "Home set"}), 200


@home_bp.route('/profil', endpoint='profil')
@login_required
def profil():

    groups = [
        {
            "id": group.id,
            "name": group.name,
            "roles": group.roles,
            "creator": User.query.filter_by(id=group.creator_id).first().username if User.query.filter_by(id=group.creator_id).first() else "",
            "members": [
                {
                    "id": group_user.user_id,
                    "username": User.query.filter_by(id=group_user.user_id).first().username if User.query.filter_by(id=group_user.user_id).first() else "",
                    "email": User.query.filter_by(id=group_user.user_id).first().email if User.query.filter_by(id=group_user.user_id).first() else ""
                }
                for group_user in GroupUsers.query.filter_by(group_id=group.id, accepted=True).all()
            ]
        }
        for group in Group.query.join(GroupUsers, Group.id == GroupUsers.group_id)
        .filter(GroupUsers.user_id == current_user.id).all()
    ]

    global allPermissions
    allPermissions = allPermissions

    user = User.query.filter_by(id=current_user.id).first()

    if user.individual_quota_gb:
        givenQuota = user.individual_quota_gb
    else:
        givenQuota = 0
        for role in user.roles:
            if role.default_quota_gb > givenQuota:
                givenQuota = role.default_quota_gb

    return render_template('profil.html', groups=groups, allPermissions=allPermissions, givenQuota=givenQuota, currentYear=datetime.now().year, VERSION=VERSION)


@home_bp.route('/set_language', methods=['PUT'], endpoint='set_language')
@login_required
def set_language():
    user = User.query.filter_by(id=current_user.id).first()
    user.language = request.json.get('language')
    db.session.commit()

    return jsonify({"message": "Language changed successfully"}), 200


@home_bp.route('/groups', endpoint='groups')
@login_required
@permissions_required('edit_group')
def groups():
    groups = [
        {
            "id": group.id,
            "name": group.name,
            "roles": group.roles,
            "members": [
                {
                    "id": group_user.id,
                    "user_id": group_user.user_id,
                    "username": User.query.filter_by(id=group_user.user_id).first().username if User.query.filter_by(id=group_user.user_id).first() else "",
                    "email": User.query.filter_by(id=group_user.user_id).first().email if User.query.filter_by(id=group_user.user_id).first() else "",
                    "created_by": User.query.filter_by(id=group_user.user_id).first().created_by if User.query.filter_by(id=group_user.user_id).first() else "",
                    "accepted": group_user.accepted,
                    "invite_email": group_user.invite_email,
                    "invite_token": group_user.invite_token
                }
                for group_user in GroupUsers.query.filter_by(group_id=group.id).all()
            ]
        }
        for group in Group.query.filter_by(creator_id=current_user.id).all()
    ]
    allowedRoles = [role.name for role in Role.query.filter_by(
        allow_as_role_in_group=True).all()]
    return render_template('groups.html', groups=groups, allowedRoles=allowedRoles, currentYear=datetime.now().year, VERSION=VERSION)


@home_bp.route('/user_management', endpoint='user_management')
@login_required
@permissions_required('user_management')
def user_management():
    users = User.query.all()
    users = [
        {
            "id": user.id,
            "username": user.username,
            "email": user.email,
            "roles": [role.name for role in user.roles],
            "groups": [
                group.name for group in Group.query.join(GroupUsers, Group.id == GroupUsers.group_id)
                .filter(GroupUsers.user_id == user.id, GroupUsers.accepted == True).all()
            ],
            "tours": [
                {"name": tour.name, "id": tour.id} for tour in Tour.query.filter_by(creator_id=user.id).all()
            ],
            "used_quota_gb": user.used_quota_gb,
            "individual_quota_gb": user.individual_quota_gb,
            "givenQuota": None,
            "last_login": user.last_login if user.last_login else "",
        }
        for user in users
    ]
    allRoles = [role.name for role in Role.query.all()]
    allGroups = [group.name for group in Group.query.all()]

    for user in users:
        user["givenQuota"] = user["individual_quota_gb"]
        if not user["givenQuota"]:
            user["givenQuota"] = 0
            for role in user["roles"]:
                roleObj = Role.query.filter_by(name=role).first()
                user["givenQuota"] = max(
                    user["givenQuota"], roleObj.default_quota_gb)

    return render_template('user_management.html', users=users, allRoles=allRoles, allGroups=allGroups, currentYear=datetime.now().year, VERSION=VERSION)


@home_bp.route('/resize_quota', methods=['PUT'])
@login_required
@permissions_required('user_management')
def resize_quota():
    data = request.get_json()
    user = User.query.filter_by(id=data["user_id"]).first()
    if data["newQuota"] == 0:
        user.individual_quota_gb = None
    else:
        user.individual_quota_gb = data["newQuota"]
    db.session.commit()

    return jsonify({"message": "Quota changed successfully"}), 200


@home_bp.route('/resize_role_quota', methods=['PUT'])
@login_required
@permissions_required('role_management')
def resize_role_quota():
    data = request.get_json()
    role = Role.query.filter_by(id=data["role_id"]).first()
    if data["newQuota"] < 0:
        role.default_quota_gb = 0
    else:
        role.default_quota_gb = data["newQuota"]
    db.session.commit()

    return jsonify({"message": "Role quota changed successfully"}), 200


@home_bp.route('/role_management', endpoint='role_management')
@login_required
@permissions_required('role_management')
def role_management():
    roles = Role.query.all()

    global allPermissions
    allPermissions = allPermissions
    return render_template('role_management.html', roles=roles, allPermissions=allPermissions, currentYear=datetime.now().year, VERSION=VERSION)


@home_bp.route('/update_permission_of_role',  methods=['PUT'], endpoint='update_permission_of_role')
@login_required
@permissions_required('role_management')
def update_permission_of_role():
    data = request.get_json()
    role = Role.query.filter_by(id=data['role']).first()
    permissions = role.get_permissions()
    if data['value']:
        role.set_permissions(permissions + [data['permission']])
        user_datastore.commit()
        return jsonify({"message": "Permission was successfully added"}), 200
    else:
        role.set_permissions(
            [perm for perm in permissions if perm != data['permission']])
        user_datastore.commit()
        return jsonify({"message": "Permission was successfully removed"}), 200


@home_bp.route('/update_allow_as_role_in_group',  methods=['PUT'], endpoint='update_allow_as_role_in_group')
@login_required
@permissions_required('role_management')
def update_allow_as_role_in_group():
    data = request.get_json()
    role = Role.query.filter_by(id=data['role']).first()
    role.allow_as_role_in_group = data['value']
    db.session.commit()
    return jsonify({"message": "Role was successfully updated"}), 200


@home_bp.route('/update_assign_to_new_user',  methods=['PUT'], endpoint='update_assign_to_new_user')
@login_required
@permissions_required('role_management')
def update_assign_to_new_user():
    data = request.get_json()
    role = Role.query.filter_by(id=data['role']).first()
    role.assign_to_new_user = data['value']
    db.session.commit()
    return jsonify({"message": "Role was successfully updated"}), 200


@home_bp.route('/update_tour_annotation',  methods=['PUT'], endpoint='update_tour_annotation')
@login_required
def update_tour_annotation():
    data = request.get_json()
    tour = Tour.query.filter_by(id=data['tour_id']).first()
    tour.annotation = data['annotation']

    tour_annotation_unread = TourAnnotationUnread.query.filter_by(
        tour_id=tour.id, user_id=current_user.id).first()
    if tour_annotation_unread:
        tour_annotation_unread.last_annotation = data['annotation']
    else:
        tour_annotation_unread = TourAnnotationUnread(
            tour_id=tour.id, user_id=current_user.id, last_annotation=data['annotation'])
        db.session.add(tour_annotation_unread)

    db.session.commit()

    lrs_event('/update-tour-annotation')
    return jsonify({"message": "Annotation was successfully updated"}), 200


@home_bp.route('/set_tour_annotation_unread', methods=['PUT', 'POST'], endpoint='set_tour_annotation_unread')
@login_required
def set_chat_unread():
    data = request.get_json()

    tour_annotation_unread = TourAnnotationUnread.query.filter_by(
        tour_id=data['tour_id'], user_id=current_user.id).first()
    if tour_annotation_unread:
        tour_annotation_unread.last_annotation = data['annotation']
    else:
        tour_annotation_unread = TourAnnotationUnread(
            tour_id=data['tour_id'], user_id=current_user.id, last_annotation=data['annotation'])
        db.session.add(tour_annotation_unread)

    db.session.commit()
    lrs_event('/set-tour-annotation-unread')
    return jsonify({"message": "Tour Annotation unread status was successfully updated"}), 200


@home_bp.route('/update_asset_annotation', methods=['POST'], endpoint='update_asset_annotation')
@login_required
def update_asset_annotation():
    data = request.get_json()
    asset = Asset.query.filter_by(
        filename_id=data['asset_id'], tour_id=data['tour_id']).first()
    asset.annotation = data['annotation']
    db.session.commit()

    lrs_event('/update-asset-annotation')
    return jsonify({"message": "Annotation was successfully updated"}), 200


@home_bp.route('/set_chat_unread', methods=['POST'], endpoint='set_chat_unread')
@login_required
def set_chat_unread():
    data = request.get_json()

    chat = Chat.query.filter_by(
        tour_id=data['tour_id']).order_by(Chat.id.desc()).first()
    if chat:
        chat_unread = ChatUnread.query.filter_by(
            tour_id=data['tour_id'], user_id=current_user.id).first()

        if chat_unread:
            chat_unread.last_chat_id = chat.id
        else:
            chat_unread = ChatUnread(
                tour_id=data['tour_id'], user_id=current_user.id, last_chat_id=chat.id)
            db.session.add(chat_unread)

        db.session.commit()

    lrs_event('/set-chat-unread')
    return jsonify({"message": "Chat unread status was successfully updated"}), 200


@home_bp.route('/setDependency', methods=['POST'], endpoint='setDependency')
def setDependency():

    data = request.get_json()

    # Delete existing dependencies for the hotspot
    old_dependencies = Dependencies.query.filter_by(
        hotspot_id=data['hotspot_id']).all()
    for dependency in old_dependencies:
        db.session.delete(dependency)

    # Add new dependencies
    for dependency_id in data['dependencies']:
        new_dependency = Dependencies(
            hotspot_id=data['hotspot_id'], dependsOn=dependency_id)
        db.session.add(new_dependency)

    db.session.commit()

    lrs_event('/setDependency')
    return jsonify({"message": "Dependencies updated successfully"}), 200
