import os
from app import app, db
from app.models import Asset
from datetime import datetime


def test_register():
    """
    register a new user
    """
    with app.test_client() as client:
        resp = client.post(
            "/register",
            data={"username": "terminator",
                  "email": "first@example.org", "password": "leifieee"},
            follow_redirects=True
        )
        assert resp.status_code == 200
        assert b"Account created successfully!" in resp.data


def test_register_login_logout():
    """
    register, login, logout the same user
    """
    with app.test_client() as client:
        resp = client.post(
            "/register",
            data={"username": "terminator2",
                  "email": "second@example.org", "password": "leifieee"},
            follow_redirects=True
        )
        assert resp.status_code == 200
        assert b"Account created successfully!" in resp.data

        resp = client.post(
            "/login",
            data={"username_or_email": "terminator2", "password": "leifieee"},
            follow_redirects=True
        )
        assert resp.status_code == 200
        assert b"My Projects" in resp.data
        assert "/home" in resp.request.path

        resp = client.get("/logout", follow_redirects=True)
        assert resp.status_code == 200
        assert b"You have been logged out." in resp.data
        assert "/login" in resp.request.path


def test_login_create_view():
    """
    login an existing user, then create and view a new empty tour
    """
    with app.test_client() as client:
        resp = client.post(
            "/login",
            data={"username_or_email": "terminator2", "password": "leifieee"},
            follow_redirects=True
        )
        assert resp.status_code == 200
        assert b"My Projects" in resp.data
        assert "/home" in resp.request.path

        resp = client.get(
            "/home/new_tour?project_name=test77",
            follow_redirects=True
        )
        assert resp.status_code == 200
        assert b"Show Tour" in resp.data
        assert "/home/create" in resp.request.path

        resp = client.get(
            "/view_tour/2",
            follow_redirects=True
        )
        assert resp.status_code == 200
        assert "/view_tour" in resp.request.path


def test_export():
    """
    export a tour
    """
    with app.test_client() as client:
        resp = client.post(
            "/login",
            data={"username_or_email": "terminator2", "password": "leifieee"},
            follow_redirects=True
        )
        assert resp.status_code == 200
        assert b"My Projects" in resp.data
        assert "/home" in resp.request.path

        resp = client.get(
            "/home/export_tour/1",
            follow_redirects=True
        )
        assert resp.status_code == 201
        assert b"tour_name" in resp.data
        assert "/home" in resp.request.path


def test_share_view():
    """
    share a tour with a user, then the other users views it
    """
    with app.test_client() as client:
        resp = client.post(
            "/login",
            data={"username_or_email": "terminator2", "password": "leifieee"},
            follow_redirects=True
        )
        assert resp.status_code == 200
        assert b"My Projects" in resp.data
        assert "/home" in resp.request.path

        resp = client.post(
            "/home/add_collaborator/1",
            data={"collaborator": "terminator", "edit": 1},
            follow_redirects=True
        )
        assert resp.status_code == 200
        assert b"Collaborator added successfully" in resp.data
        assert "/home" in resp.request.path

        resp = client.get("/logout", follow_redirects=True)
        assert resp.status_code == 200
        assert b"You have been logged out." in resp.data
        assert "/login" in resp.request.path

        resp = client.post(
            "/login",
            data={"username_or_email": "terminator", "password": "leifieee"},
            follow_redirects=True
        )
        assert resp.status_code == 200
        assert b"My Projects" in resp.data
        assert "/home" in resp.request.path

        resp = client.get(
            "/view_tour/1",
            follow_redirects=True
        )
        assert resp.status_code == 200
        assert b"<title>19squared | View Tour</title>" in resp.data
        assert "/view_tour" in resp.request.path


def test_asset_mediapage_hotspot():
    """
    add an asset to an empty shared tour, then add it as mediapage to the tour and create a hotspot
    """
    with app.test_client() as client:
        resp = client.post(
            "/login",
            data={"username_or_email": "terminator", "password": "leifieee"},
            follow_redirects=True
        )
        assert resp.status_code == 200
        assert b"My Projects" in resp.data
        assert "/home" in resp.request.path

        resp = client.get(
            "/view_tour/1",
            follow_redirects=True
        )
        assert resp.status_code == 200
        assert b"<title>19squared | View Tour</title>" in resp.data
        assert "/view_tour" in resp.request.path

        asset = Asset(
            filename_id="test.png",
            name="test.png",
            format="image/png",
            created=datetime(2023, 1, 1, 0, 0, 0),
            tour_id=1,
            media_type="Image"
        )

        db.session.add(asset)
        db.session.commit()

        resp = client.post(
            "/home/create_mediapage",
            data={
                'tour_id': 1,
                'asset_id': 'test.png',
                'name': 'Test Mediapage'
            },
            follow_redirects=True
        )
        assert resp.status_code == 201
        assert b"Mediapage created" in resp.data
        assert "/home" in resp.request.path

        resp = client.post(
            "/home/save_hotspot",
            data={
                "name": "My Hotspot",
                "position": "0 1.6 -3",
                "rotation": "0 45 0",
                "media_page_id": 1,
                "media_type": 1,
                "presented_media_id": 1,
                'custom_display_icon_id': None,
                'starttime': 0,
                'endtime': -1
            },
            follow_redirects=True
        )
        assert resp.status_code == 200
        assert b"Hotspot saved" in resp.data
        assert "/home" in resp.request.path


def test_user_management():
    """
    login as admin and view user management
    """
    with app.test_client() as client:
        resp = client.post(
            "/login",
            data={"username_or_email": os.getenv(
                'ADMIN_USERNAME'), "password": os.getenv('ADMIN_PASSWORD')},
            follow_redirects=True
        )
        assert resp.status_code == 200
        assert b"My Projects" in resp.data
        assert "/home" in resp.request.path

        resp = client.get(
            "/home/user_management",
            follow_redirects=True
        )
        assert resp.status_code == 200
        assert b"User-Management" in resp.data


def test_role_management():
    """
    login as admin and view role management
    """
    with app.test_client() as client:
        resp = client.post(
            "/login",
            data={"username_or_email": os.getenv(
                'ADMIN_USERNAME'), "password": os.getenv('ADMIN_PASSWORD')},
            follow_redirects=True
        )
        assert resp.status_code == 200
        assert b"My Projects" in resp.data
        assert "/home" in resp.request.path

        resp = client.get(
            "/home/role_management",
            follow_redirects=True
        )
        assert resp.status_code == 200
        assert b"Role-Management" in resp.data

        resp = client.post(
            "/home/create_role",
            data={"role_name": "test"},
            follow_redirects=True
        )
        assert resp.status_code == 200
        assert b"Role created" in resp.data


def test_groups():
    """
    login as admin, create a group, add a role to the group
    """
    with app.test_client() as client:
        resp = client.post(
            "/login",
            data={"username_or_email": os.getenv(
                'ADMIN_USERNAME'), "password": os.getenv('ADMIN_PASSWORD')},
            follow_redirects=True
        )
        assert resp.status_code == 200
        assert b"My Projects" in resp.data
        assert "/home" in resp.request.path

        resp = client.get(
            "/home/groups",
            follow_redirects=True
        )
        assert resp.status_code == 200
        assert b"Groups" in resp.data

        resp = client.post(
            "/home/create_group",
            data={"group_name": "testgroup",
                  "group_roles": ["test"],
                  "group_invites": int(10)},
            follow_redirects=True
        )
        assert resp.status_code == 200
        assert b"Group created" in resp.data
