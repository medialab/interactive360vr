import os
import mimetypes
from app import app
from dotenv import load_dotenv
load_dotenv()


class Config(object):
    # Define the application directory
    BASE_DIR = os.path.abspath(os.path.dirname(__file__))

    # Development server settings
    PORT = int(os.getenv('APP_PORT'))

    if int(os.getenv('APP_SSL')):
        SSL = True
    else:
        SSL = False

    if int(os.getenv('APP_DEBUG')):
        ENVIRONMENT = "Development"
    else:
        ENVIRONMENT = "Production"

    # MariaDB docker connection
    if os.getenv('APP_DOCKER') and int(os.getenv('APP_DOCKER')):
        DB_URL = os.getenv('DB_URL_PROD')
        ASSET_URL = os.getenv('ASSETS_URL_PROD')
    elif os.getenv('APP_TEST') and int(os.getenv('APP_TEST')):
        DB_URL = os.getenv('DB_URL_TEST')
        ASSET_URL = os.getenv('ASSETS_URL_TEST')
    else:
        DB_URL = os.getenv('DB_URL_DEV')
        ASSET_URL = os.getenv('ASSETS_URL_DEV')

    SQLALCHEMY_DATABASE_URI = 'mariadb+mariadbconnector://root:' + \
        os.getenv('DB_PASSWORD') + '@' + DB_URL + '/' + os.getenv('DB_NAME')

    # Language settings
    LANGUAGES = os.getenv('LANGUAGES').split(",")
    DEFAULT_LANGUAGE = os.getenv('DEFAULT_LANGUAGE')

    # Auth options
    AUTH_GOOGLE = os.getenv('AUTH_GOOGLE')
    AUTH_GITLAB = os.getenv('AUTH_GITLAB')
    AUTH_GITHUB = os.getenv('AUTH_GITHUB')

    # Further options
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DATABASE_CONNECT_OPTIONS = {}

    # Application threads. A common general assumption is
    # using 2 per available processor cores - to handle
    # incoming requests using one and performing background
    # operations using the other.
    THREADS_PER_PAGE = 2

    # Enable protection against *Cross-site Request Forgery (CSRF)*
    CSRF_ENABLED = True

    # Use a secure, unique and absolutely secret key for
    # signing the data.
    CSRF_SESSION_KEY = os.getenv('APP_RANDOM_KEY')
    SECRET_KEY = os.getenv('APP_RANDOM_KEY')

    # Allowed storage size per user upload.
    # = 1 Gibibyte * APP_CONTENT_LENGTH
    MAX_CONTENT_LENGTH = int(
        os.getenv('APP_CONTENT_LENGTH')) * 1024 * 1024 * 1024

    UPLOAD_EXTENSIONS = ['.jpg', '.jpeg', '.png', '.gif', '.bmp',
                         '.mp4', '.mov', '.webm', '.avi', '.mp3', '.wav', '.flac', '.mkv']
    ALLOWED_EXTENSIONS = [item.replace(".", "") for item in UPLOAD_EXTENSIONS]
    FILE_TYPES = {
        '.jpg': 'Image',
        '.jpeg': 'Image',
        '.png': 'Image',
        '.webp': 'Image',
        '.gif': 'Image',
        '.bmp': 'Image',
        '.mp4': 'Video',
        '.avi': 'Video',
        '.webm': 'Video',
        '.mov': 'Video',
        '.mkv': 'Video',
        '.mp3': 'Audio',
        '.wav': 'Audio',
        '.flac': 'Audio',
    }


class DevelopmentConfig(Config):
    DEBUG = True


class ProductionConfig(Config):
    DEBUG = False
